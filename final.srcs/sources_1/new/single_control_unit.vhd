

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity single_control_unit is
port( clk,reset: in std_logic;
      iobus: in std_logic_vector(3 downto 0);
      iochoice: in std_logic_vector(1 downto 0);
      can_bet: in std_logic;
      bankrupt: in std_logic;
      p_card_rdy,d_card_rdy: in std_logic;
      bet_amount: out std_logic_vector(3 downto 0);
      player_hit,dealer_hit: out std_logic;
      dealer_win,player_win: out std_logic;
      p_score_ext,d_score_ext: out std_logic_vector(4 downto 0);
      state_exit : out std_logic_vector(3 downto 0);
      add_flag_ext: out std_logic;
      p_t1,p_t2,p_t3,p_t4,d_t1,d_t2,d_t3,d_t4: out std_logic;
      p_card,d_card: in std_logic_vector(3 downto 0);
      mem_clear: out std_logic;
      easy_out,medium_out,hard_out: out std_logic;
      tie : out std_logic;
      vga_debug: out std_logic_vector(3 downto 0)
);
end single_control_unit;

architecture Behavioral of single_control_unit is
--- state variable declarations
signal n_state,p_state,buff_state: std_logic_vector(15 downto 0) := "0010000000000000";
constant user_1: std_logic_vector(15 downto 0):=    "0000000000000001";
constant deal_1: std_logic_vector(15 downto 0):=    "0000000000000010";
constant user_2: std_logic_vector(15 downto 0):=    "0000000000000100";
constant deal_2: std_logic_vector(15 downto 0):=    "0000000000001000";
constant user_3: std_logic_vector(15 downto 0):=    "0000000000010000";
constant deal_3: std_logic_vector(15 downto 0):=    "0000000000100000";
constant user_4: std_logic_vector(15 downto 0):=    "0000000001000000";
constant deal_4: std_logic_vector(15 downto 0):=    "0000000010000000";
constant user_win: std_logic_vector(15 downto 0):=  "0000000100000000";
constant deal_win: std_logic_vector(15 downto 0):=  "0000001000000000";
constant user_bet: std_logic_vector(15 downto 0):=  "0000010000000000";
constant bet_conf: std_logic_vector(15 downto 0):=  "0000100000000000";
constant broke: std_logic_vector(15 downto 0):=     "0001000000000000";
constant mode_sel: std_logic_vector(15 downto 0):=  "0010000000000000";
constant score_calc: std_logic_vector(15 downto 0):="0100000000000000";
constant nxt_round: std_logic_vector(15 downto 0):= "1000000000000000";

signal easy, medium,hard : std_logic := '0';
signal p_stand,d_stand: std_logic:='0';
signal p_ace,d_ace: std_logic:='0';
signal p_win,d_win: std_logic:='0';
--signal p_turn,d_turn: std_logic_vector(2 downto 0):="000";
signal bet_am_int : std_logic_vector(3 downto 0):="0000";
signal phit,dhit: std_logic:= '0';
signal ps_t1,ps_t2,ps_t3,ps_t4,ds_t1,ds_t2,ds_t3,ds_t4: std_logic:='0';
signal dec_made,hit_dec,stand_dec : std_logic := '0';

constant N: integer:=26;
signal counter: unsigned(N-1 downto 0):= ( others => '0' );
signal add_flag_int: std_logic:= '0';
--shared variable p_score,d_score: unsigned(4 downto 0):="00000";
signal p_score,d_score: unsigned(4 downto 0):="00000";

begin
    easy_out <= easy;
    medium_out <= medium;
    hard_out <= hard;
    vga_debug <= "000" & add_flag_int;
    p_score_ext <= std_logic_vector(p_score);
    d_score_ext <= std_logic_vector(d_score);
    player_win <= p_win;
    dealer_win <= d_win;
    
    p_t1 <= ps_t1;
    p_t2 <= ps_t2;
    p_t3 <= ps_t3;
    p_t4 <= ps_t4;
    d_t1 <= ds_t1;
    d_t2 <= ds_t2;
    d_t3 <= ds_t3;
    d_t4 <= ds_t4;
    
    
    
    bet_amount <= bet_am_int;
    player_hit <= phit;
    dealer_hit <= dhit;
    state_exit <= "0000" when n_state="0000000000000001" else 
                  "0001" when n_state="0000000000000010" else 
                  "0010" when n_state="0000000000000100" else
                  "0011" when n_state="0000000000001000" else
                  "0100" when n_state="0000000000010000" else
                  "0101" when n_state="0000000000100000" else
                  "0110" when n_state="0000000001000000" else
                  "0111" when n_state="0000000010000000" else
                  "1000" when n_state="0000000100000000" else
                  "1001" when n_state="0000001000000000" else
                  "1010" when n_state="0000010000000000" else
                  "1011" when n_state="0000100000000000" else
                  "1100" when n_state="0001000000000000" else
                  "1101" when n_state="0010000000000000" else
                  "1110" when n_state="0100000000000000" else
                  "1111";
                  
    add_flag_ext <= add_flag_int;
    --- state changing process
    comb: process(clk,p_state,n_state)
    variable add_flag: std_logic;
    variable add_flag_2: std_logic;
    begin
       if (rising_edge(clk)) then                   --DELAY MODULE  
           --p_state <= n_state;
           if(p_state = n_state) then
           
            add_flag := '0';
            add_flag_2 := '0';
          elsif(p_state= user_win OR p_state=deal_win OR p_state = score_calc) then
            add_flag_2 :=  not(counter(N-1));
            if(add_flag_2 = '1') then
                counter <= counter + 1;
            else
                p_state <= n_state;
                counter <= (others => '0');
            end if;
          else
              add_flag := not (counter(N-2));
              if(add_flag = '1') then
                counter <= counter + 1;
              else
                p_state <= n_state;
                counter <= (others => '0');
              end if;
          end if;
          add_flag_int <= add_flag;
                
               
       end if;
     end process comb;
    
    --- state working
    seq: process(reset,clk,p_ace,d_ace,p_win,d_win)
     
    begin
       if(reset='1') then
       n_state <= mode_sel;
       mem_clear <= '1';
       easy <= '0';
       medium <= '0';
       hard <= '0';
       p_win <= '0';
       d_win <= '0';
       tie <= '0';
       elsif(bankrupt = '1') then
            n_state <= "1111111111111111";
       elsif(rising_edge(clk)) then
          case p_state is
       WHEN mode_sel =>
             if(iobus(0) = '1') then
                easy <= '1';
                medium <= '0';
                hard <= '0';
                n_state <= nxt_round;
                
             elsif(iobus(1) = '1') then
                easy <= '0';
                medium <= '1';
                hard <= '0';
                n_state <= nxt_round;
                
             elsif(iobus(2) = '1') then
                easy <= '0';
                medium <= '0';
                hard <= '1';
                n_state <= nxt_round;
                
             end if;                
       WHEN user_bet =>
             if(iobus/="0000") then
                bet_am_int <= iobus;
                n_state <= bet_conf;
                mem_clear <= '0';
             end if;
      WHEN bet_conf =>
             if(can_bet='1') then
               --bet_am_int <= "0000";
                n_state <= user_1;
                --n_state <= deal_1;
             else
                n_state <= user_bet;
             end if;
       WHEN user_1 =>
            if(iochoice(0) = '1' AND ps_t1 = '0') then
                phit <= '1';
                ps_t1 <= '1';
                ps_t2 <= '0';
                ps_t3 <= '0';
                ps_t4 <= '0';
                ds_t1 <= '0';
                ds_t2 <= '0';
                ds_t3 <= '0';
                ds_t4 <= '0';  
            elsif(p_card_rdy = '1') then
                phit <= '0';
                if(p_card = "0001") then
                     p_ace <= '1';
                    p_score <= "01011";
                else
                    p_score <= unsigned('0' & p_card);
                end if;
                n_state <= deal_1;
            end if;
       WHEN deal_1 =>
           if(ds_t1 = '0') then
               dhit <= '1';
               ps_t1 <= '0';
               ps_t2 <= '0';
               ps_t3 <= '0';
               ps_t4 <= '0';
               ds_t1 <= '1';
               ds_t2 <= '0';
               ds_t3 <= '0';
               ds_t4 <= '0';  
          elsif(d_card_rdy = '1') then
               dhit <= '0';
          if(d_card = "0001") then
              d_ace <= '1';
              d_score <= "01011";
          else
              d_score <= unsigned('0' & d_card);
          end if;
              n_state <= user_2;
          end if;
       WHEN user_2 =>
          dec_made <= '0';
          if(iochoice(0)='1' AND ps_t2 = '0') then
              phit <= '1';
              ps_t1 <= '0';
              ps_t2 <= '1';
              ps_t3 <= '0';
              ps_t4 <= '0';
              ds_t1 <= '0';
              ds_t2 <= '0';
              ds_t3 <= '0';
              ds_t4 <= '0'; 
          elsif(iochoice(1) = '1' AND ps_t2 = '0') then
              p_stand <= '1';
              n_state <= deal_2;
          elsif(p_card_rdy = '1' AND phit = '1') then
              phit <= '0';
              p_score <= p_score + unsigned('0' & p_card);
              n_state <= deal_2;
          end if;
   WHEN deal_2 =>
       if(ds_t2 = '0') then
           dhit <= '1';
           ps_t1 <= '0';
           ps_t2 <= '0';
           ps_t3 <= '0';
           ps_t4 <= '0';
           ds_t1 <= '0';
           ds_t2 <= '1';
           ds_t3 <= '0';
           ds_t4 <= '0'; 
       elsif(d_card_rdy = '1' AND dhit = '1') then
           dhit <= '0';
           d_score <= d_score + unsigned('0' & d_card);
           n_state <= user_3;
       end if;
    WHEN user_3 =>
        dec_made <= '0';
       if( (p_stand = '1') OR (iochoice(1) = '1' AND ps_t3 ='0' AND buff_state /= user_3)) then
           p_stand <= '1';
           n_state <= deal_3;
      elsif(iochoice(0) = '1' AND ps_t3='0' AND buff_state /= user_3) then
           phit <= '1';
           ps_t1 <= '0';
           ps_t2 <= '0';
           ps_t3 <= '1';
           ps_t4 <= '0';
           ds_t1 <= '0';
           ds_t2 <= '0';
           ds_t3 <= '0';
           ds_t4 <= '0';
      elsif(p_card_rdy = '1' AND phit = '1' AND buff_state /= user_3) then
           phit <= '0';
           p_score <= p_score + unsigned('0' & p_card);
      elsif(p_score > "10101") then
           n_state <= broke;
           buff_state <= user_3;
      elsif(p_score = "10101") then
           n_state <= user_win;
      elsif(ps_t3='1') then
           n_state <= deal_3;
      end if;
   WHEN deal_3 =>
      if(dec_made = '0') then
        if(easy = '1') then
            if(d_score >= "01111") then
                stand_dec <= '1';
                hit_dec <= '0';
                dec_made <= '1';
            else
                hit_dec <= '1';
                stand_dec <= '0';
                dec_made <= '1';
            end if;
         elsif(medium = '1') then
            if(d_score >= "10001" OR (to_integer(d_score) - to_integer(p_score) >= 10)) then
                stand_dec <= '1';
                hit_dec <= '0';
                dec_made <= '1';
            else
                hit_dec <= '1';
                stand_dec <= '0';
                dec_made <= '1';
            end if; 
         else
            if((d_score >= "10001" AND d_score <= "10010" AND (to_integer(d_score) - to_integer(p_score) >= 5)) OR d_score > "10010") then
                stand_dec <= '1';
                hit_dec <= '0';
                dec_made <= '1';
            else
                hit_dec <= '1';
                stand_dec <= '0';
                dec_made <= '1';
            end if;
         end if;
    elsif(stand_dec = '1' AND buff_state /= deal_3) then
        d_stand <= '1';
        n_state <= user_4;
        stand_dec <= '0';
      elsif(ds_t3 = '0' AND hit_dec='1' AND buff_state /= deal_3) then
        dhit <= '1';
        hit_dec <= '0';
        ps_t1 <= '0';
        ps_t2 <= '0';
        ps_t3 <= '0';
        ps_t4 <= '0';
        ds_t1 <= '0';
        ds_t2 <= '0';
        ds_t3 <= '1';
        ds_t4 <= '0';
     elsif(dhit = '1' AND d_card_rdy = '1' AND buff_state /= deal_3) then
        dhit <= '0';
        d_score <= d_score + unsigned('0' & d_card);
     elsif(d_score > "10101") then
        n_state <= broke;
        buff_state <= deal_3;
        
     elsif(d_score = "10101") then
        n_state <= deal_win;
        
     elsif(ds_t3='1' AND dhit = '0') then
        n_state <= user_4;
        
     end if;
    WHEN user_4 =>
        dec_made <= '0';
        if( (p_stand = '1') OR (iochoice(1) = '1' AND ps_t4 ='0' AND buff_state /= user_4)) then
            p_stand <= '1';
            n_state <= deal_4;
       elsif(iochoice(0) = '1' AND ps_t4='0' AND buff_state /= user_4) then
            phit <= '1';
            ps_t1 <= '0';
            ps_t2 <= '0';
            ps_t3 <= '0';
            ps_t4 <= '1';
            ds_t1 <= '0';
            ds_t2 <= '0';
            ds_t3 <= '0';
            ds_t4 <= '0';
       elsif(p_card_rdy = '1' AND phit = '1' AND buff_state /= user_4) then
            phit <= '0';
            p_score <= p_score + unsigned('0' & p_card);
       elsif(p_score > "10101") then
            n_state <= broke;
            buff_state <= user_4;
       elsif(p_score = "10101") then
            n_state <= user_win;
       elsif(ps_t4='1') then
            n_state <= deal_4;
       end if;
    WHEN deal_4 =>
   if(dec_made = '0') then
      if(easy = '1') then
          if(d_score >= "10000") then
              stand_dec <= '1';
              hit_dec <= '0';
              dec_made <= '1';
          else
              hit_dec <= '1';
              stand_dec <= '0';
              dec_made <= '1';
          end if;
       elsif(medium = '1') then
          if(d_score >= "10010" OR (to_integer(d_score) - to_integer(p_score) >= 10)) then
              stand_dec <= '1';
              hit_dec <= '0';
              dec_made <= '1';
          else
              hit_dec <= '1';
              stand_dec <= '0';
              dec_made <= '1';
          end if; 
       else
          if((d_score >= "10010" AND d_score <= "10011" AND (to_integer(d_score) - to_integer(p_score) >= 5)) OR d_score > "10011") then
              stand_dec <= '1';
              hit_dec <= '0';
              dec_made <= '1';
          else
              hit_dec <= '1';
              stand_dec <= '0';
              dec_made <= '1';
          end if;
       end if;
       elsif(d_stand = '1' OR (stand_dec = '1' AND buff_state /= deal_4)) then
         d_stand <= '1';
         n_state <= score_calc;
         stand_dec <= '1';
       elsif(ds_t4 = '0' AND hit_dec ='1' AND buff_state /= deal_4) then
         dhit <= '1';
         hit_dec <= '1';
         ps_t1 <= '0';
         ps_t2 <= '0';
         ps_t3 <= '0';
         ps_t4 <= '0';
         ds_t1 <= '0';
         ds_t2 <= '0';
         ds_t3 <= '0';
         ds_t4 <= '1';
      elsif(dhit = '1' AND d_card_rdy = '1' AND buff_state /= deal_4) then
         dhit <= '0';
         d_score <= d_score + unsigned('0' & d_card);
      elsif(d_score > "10101") then
         n_state <= broke;
         buff_state <= deal_4; 
      elsif(d_score = "10101") then
         n_state <= deal_win;
      elsif(ds_t4='1' AND dhit = '0') then
         n_state <= score_calc;
         buff_state <= deal_4;
      end if;               
   WHEN nxt_round =>
          
          buff_state<= nxt_round;
          ps_t1 <= '0';
          ps_t2 <= '0';
          ps_t3 <= '0';
          ps_t4 <= '0';
          ds_t1 <= '0';
          ds_t2 <= '0';
          ds_t3 <= '0';
          ds_t4 <= '0';
          p_stand <= '0';
          d_stand <= '0';
          p_win   <= '0';
          d_win   <= '0';
          p_ace   <= '0';
          d_ace   <= '0';
          stand_dec <= '0';
          hit_dec <= '0';
          dec_made <= '0';
          bet_am_int <= "0000";
          
			 p_score <= "00000";
          
			 d_score <= "00000";
			 mem_clear <= '1';
			 tie <= '0';
			
			     n_state <= user_bet;
			
	WHEN score_calc =>
	     if(d_score > "10101") then
	       n_state <= broke;
	     elsif(p_score > "10101") then
	       n_state <= broke;
	     elsif( d_score > p_score) then
	       n_state <= deal_win;
	     elsif( p_score > d_score) then
	       n_state <= user_win;
	     else
	       tie <= '1';
	       
	           n_state <= nxt_round;
	       
	     end if;
	WHEN broke =>
	      if(d_score > "10101") then
	        if(d_ace = '1') then
	           d_score <= d_score - "01010";
	           n_state <= buff_state;
	           d_ace <= '0';
	        else
	           n_state <= user_win;
	        end if;
	      elsif(p_score > "10101") then
	        if(p_ace = '1') then
	           p_score <= p_score - "01010";
	           n_state <= buff_state;
	           p_ace <= '0';
	        else
	           n_state <= deal_win;
	        end if;
	     end if;
	WHEN user_win =>
	       n_state <= nxt_round;
	       p_win <= '1';
	       d_win <= '0';
	     
	WHEN deal_win =>
	       n_state <= nxt_round;
	       p_win <= '0';
	       d_win <= '1';
		 when others =>
			  n_state <= n_state;
          end case;
      end if;
      end process seq;      
end Behavioral;
