----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 26.06.2016 18:56:00
-- Design Name: 
-- Module Name: wallet - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity wallet is
port( clk,reset: in std_logic;
      bet_amount: in std_logic_vector(3 downto 0);
      budget: out std_logic_vector(8 downto 0);
      can_bet: out std_logic;
      mem_clr: in std_logic;
      player_win,dealer_win: in std_logic;
      player_bankrupt: out std_logic
);
end wallet;

architecture Behavioral of wallet is
shared variable pocket: signed(8 downto 0):="001110011";
shared variable bet: unsigned(3 downto 0):="0000";
signal bet_signal,win_signal: std_logic:= '0';
signal p_brupt,can: std_logic:= '0';
signal adsub: std_logic:= '0';
begin

player_bankrupt <= p_brupt;
can_bet <= can;
budget <= std_logic_vector(pocket);

pfoglio: process(clk,reset,p_brupt,can)
variable buff: integer:=0;
begin
    if(reset='1') then
    pocket := "001110011";
    bet := "0000";
    p_brupt <= '0';
    buff := 0;
    bet_signal <= '0';
    win_signal <= '0';
    can <= '0';
    elsif( mem_clr = '1') then
        bet_signal <= '0';
        win_signal <= '0';
        can <= '0';
    elsif(rising_edge(clk)) then
    if(bet_amount /= "0000" AND bet_signal = '0') then
      bet := unsigned(bet_amount);
      if(to_integer(bet) <= to_integer(pocket)) then
      buff := to_integer(pocket) - to_integer(bet);
      pocket := to_signed(buff,9);
      can <= '1';
      bet_signal <= '1';
      end if;
    elsif(player_win = '1' AND win_signal = '0') then
     buff := to_integer(pocket) + to_integer(bet(2 downto 0) & bet(3));
     pocket := to_signed(buff,9);
     win_signal <= '1';
    elsif(dealer_win = '1' AND win_signal ='0' AND (to_integer(pocket)) <= 0) then
     p_brupt <= '1';
     win_signal <= '1';
    end if;
    end if;
end process pfoglio;
    
end Behavioral;
