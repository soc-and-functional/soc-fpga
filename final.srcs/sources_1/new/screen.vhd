library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity screen is port(
    video_on:in std_logic;
    pixel_x,pixel_y:in std_logic_vector(9 downto 0);
    ---d_cd_col,p_cd_col: in std_logic_vector(2 downto 0);
    ---d_cd_num,p_cd_num: in std_logic_vector(3 downto 0);
    d1_cd_col,d2_cd_col,d3_cd_col,d4_cd_col: in std_logic_vector(2 downto 0);
    p1_cd_col,p2_cd_col,p3_cd_col,p4_cd_col: in std_logic_vector(2 downto 0);
    d1_cd_num,d2_cd_num,d3_cd_num,d4_cd_num: in std_logic_vector(3 downto 0);
    p1_cd_num,p2_cd_num,p3_cd_num,p4_cd_num: in std_logic_vector(3 downto 0);
    p_score, d_score: in std_logic_vector(4 downto 0);
    p_bet: in std_logic_vector(3 downto 0);
    iobus: in std_logic_vector(3 downto 0);
    ---d_cd_turn,p_cd_turn: in std_logic_vector(2 downto 0);
    p_budget: in std_logic_vector(8 downto 0);
    rgb_out :out std_logic_vector(15 downto 0);
    diff_ch: in std_logic_vector(1 downto 0)
    );
end entity;

architecture arch of screen is

--difficulty simbols
constant Sym_T: integer:= 12;
constant Sym_B: integer:= 20;
constant Sym1_l: integer:= 588;
constant Sym1_r: integer:= 596;
constant Sym2_l: integer:= 600;
constant Sym2_r: integer:= 608;
constant Sym3_l: integer:= 612;
constant Sym3_r: integer:= 620;

--- stripes
constant Bar1_T: integer:= 30;
constant Bar1_B: integer:= 35;
constant Bar2_T: integer:= 235;
constant Bar2_B: integer:= 240;
constant Bar3_T: integer:= 242;
constant Bar3_B: integer:= 247;
constant Bar4_T: integer:= 455;
constant Bar4_B: integer:= 460;
--- dealer card1 boundary
constant d1_cd_brdr_t1: integer:=70;
constant d1_cd_brdr_t2: integer:=73;
constant d1_cd_brdr_b1: integer:=190;
constant d1_cd_brdr_b2: integer:=193;
constant d1_cd_brdr_l1: integer:=30;
constant d1_cd_brdr_l2: integer:=33;
constant d1_cd_brdr_r1: integer:=130;
constant d1_cd_brdr_r2: integer:=133;
--- player card1 boundary
constant p1_cd_brdr_t1: integer:=265;
constant p1_cd_brdr_t2: integer:=268;
constant p1_cd_brdr_b1: integer:=385;
constant p1_cd_brdr_b2: integer:=388;
constant p1_cd_brdr_l1: integer:=30;
constant p1_cd_brdr_l2: integer:=33;
constant p1_cd_brdr_r1: integer:=130;
constant p1_cd_brdr_r2: integer:=133;

--- dealer card2 boundary
constant d2_cd_brdr_t1: integer:=70;
constant d2_cd_brdr_t2: integer:=73;
constant d2_cd_brdr_b1: integer:=190;
constant d2_cd_brdr_b2: integer:=193;
constant d2_cd_brdr_l1: integer:=140;
constant d2_cd_brdr_l2: integer:=143;
constant d2_cd_brdr_r1: integer:=240;
constant d2_cd_brdr_r2: integer:=243;
--- player card2 boundary
constant p2_cd_brdr_t1: integer:=265;
constant p2_cd_brdr_t2: integer:=268;
constant p2_cd_brdr_b1: integer:=385;
constant p2_cd_brdr_b2: integer:=388;
constant p2_cd_brdr_l1: integer:=140;
constant p2_cd_brdr_l2: integer:=143;
constant p2_cd_brdr_r1: integer:=240;
constant p2_cd_brdr_r2: integer:=243;

--- dealer card3 boundary
constant d3_cd_brdr_t1: integer:=70;
constant d3_cd_brdr_t2: integer:=73;
constant d3_cd_brdr_b1: integer:=190;
constant d3_cd_brdr_b2: integer:=193;
constant d3_cd_brdr_l1: integer:=250;
constant d3_cd_brdr_l2: integer:=253;
constant d3_cd_brdr_r1: integer:=350;
constant d3_cd_brdr_r2: integer:=353;
--- player card3 boundary
constant p3_cd_brdr_t1: integer:=265;
constant p3_cd_brdr_t2: integer:=268;
constant p3_cd_brdr_b1: integer:=385;
constant p3_cd_brdr_b2: integer:=388;
constant p3_cd_brdr_l1: integer:=250;
constant p3_cd_brdr_l2: integer:=253;
constant p3_cd_brdr_r1: integer:=350;
constant p3_cd_brdr_r2: integer:=353;

--- dealer card4 boundary
constant d4_cd_brdr_t1: integer:=70;
constant d4_cd_brdr_t2: integer:=73;
constant d4_cd_brdr_b1: integer:=190;
constant d4_cd_brdr_b2: integer:=193;
constant d4_cd_brdr_l1: integer:=360;
constant d4_cd_brdr_l2: integer:=363;
constant d4_cd_brdr_r1: integer:=460;
constant d4_cd_brdr_r2: integer:=463;
--- player card4 boundary
constant p4_cd_brdr_t1: integer:=265;
constant p4_cd_brdr_t2: integer:=268;
constant p4_cd_brdr_b1: integer:=385;
constant p4_cd_brdr_b2: integer:=388;
constant p4_cd_brdr_l1: integer:=360;
constant p4_cd_brdr_l2: integer:=363;
constant p4_cd_brdr_r1: integer:=460;
constant p4_cd_brdr_r2: integer:=463;

--- dealer symbol 1 boundary
 constant d11_col_t: integer:=80;
 constant d11_col_b: integer:=88;
 constant d11_col_l: integer:=40;
 constant d11_col_r: integer:=48;
 constant d21_col_t: integer:=174;
 constant d21_col_b: integer:=182;
 constant d21_col_l: integer:=115;
 constant d21_col_r: integer:=123;
 --- dealer number 1 boundary
 constant d11_num_t: integer:=80;
 constant d11_num_b: integer:=88;
 constant d11_num_l: integer:=115;
 constant d11_num_r: integer:=122;
 constant d21_num_t: integer:=174;
 constant d21_num_b: integer:=182;
 constant d21_num_l: integer:=40;
 constant d21_num_r: integer:=47;
 
--- player symbol1 boundary
   constant p11_col_t: integer:=275;
   constant p11_col_b: integer:=283;
   constant p11_col_l: integer:=40;
   constant p11_col_r: integer:=48;
   constant p21_col_t: integer:=369;
   constant p21_col_b: integer:=377;
   constant p21_col_l: integer:=115;
   constant p21_col_r: integer:=123;
   --- player number1 boundary
   constant p11_num_t: integer:=275;
   constant p11_num_b: integer:=283;
   constant p11_num_l: integer:=115;
   constant p11_num_r: integer:=122;
   constant p21_num_t: integer:=369;
   constant p21_num_b: integer:=377;
   constant p21_num_l: integer:=40;
   constant p21_num_r: integer:=47;
 
 
 --- dealer symbol 2boundary
  constant d12_col_t: integer:=80;
  constant d12_col_b: integer:=88;
  constant d12_col_l: integer:=150;
  constant d12_col_r: integer:=158;
  constant d22_col_t: integer:=174;
  constant d22_col_b: integer:=182;
  constant d22_col_l: integer:=225;
  constant d22_col_r: integer:=233;
  --- dealer number 2boundary
  constant d12_num_t: integer:=80;
  constant d12_num_b: integer:=88;
  constant d12_num_l: integer:=225;
  constant d12_num_r: integer:=232;
  constant d22_num_t: integer:=174;
  constant d22_num_b: integer:=182;
  constant d22_num_l: integer:=150;
  constant d22_num_r: integer:=157;

  --- player symbol2 boundary
  constant p12_col_t: integer:=275;
  constant p12_col_b: integer:=283;
  constant p12_col_l: integer:=150;
  constant p12_col_r: integer:=158;
  constant p22_col_t: integer:=369;
  constant p22_col_b: integer:=377;
  constant p22_col_l: integer:=225;
  constant p22_col_r: integer:=233;
  --- player number2 boundary
  constant p12_num_t: integer:=275;
  constant p12_num_b: integer:=283;
  constant p12_num_l: integer:=225;
  constant p12_num_r: integer:=232;
  constant p22_num_t: integer:=369;
  constant p22_num_b: integer:=377;
  constant p22_num_l: integer:=150;
  constant p22_num_r: integer:=157;
  

--- dealer symbol 3boundary
 constant d13_col_t: integer:=80;
 constant d13_col_b: integer:=88;
 constant d13_col_l: integer:=260;
 constant d13_col_r: integer:=268;
 constant d23_col_t: integer:=174;
 constant d23_col_b: integer:=182;
 constant d23_col_l: integer:=335;
 constant d23_col_r: integer:=343;
 --- dealer number  3boundary
 constant d13_num_t: integer:=80;
 constant d13_num_b: integer:=88;
 constant d13_num_l: integer:=335;
 constant d13_num_r: integer:=342;
 constant d23_num_t: integer:=174;
 constant d23_num_b: integer:=182;
 constant d23_num_l: integer:=260;
 constant d23_num_r: integer:=267;
 
--- player symbol 3boundary
   constant p13_col_t: integer:=275;
   constant p13_col_b: integer:=284;
   constant p13_col_l: integer:=260;
   constant p13_col_r: integer:=268;
   constant p23_col_t: integer:=369;
   constant p23_col_b: integer:=377;
   constant p23_col_l: integer:=335;
   constant p23_col_r: integer:=343;
   --- player number 3boundary
   constant p13_num_t: integer:=275;
   constant p13_num_b: integer:=283;
   constant p13_num_l: integer:=335;
   constant p13_num_r: integer:=342;
   constant p23_num_t: integer:=369;
   constant p23_num_b: integer:=377;
   constant p23_num_l: integer:=260;
   constant p23_num_r: integer:=267;
  
  
--- dealer symbol 4boundary
  constant d14_col_t: integer:=80;
  constant d14_col_b: integer:=88;
  constant d14_col_l: integer:=370;
  constant d14_col_r: integer:=378;
  constant d24_col_t: integer:=174;
  constant d24_col_b: integer:=182;
  constant d24_col_l: integer:=445;
  constant d24_col_r: integer:=453;
  --- dealer number 4boundary
  constant d14_num_t: integer:=80;
  constant d14_num_b: integer:=88;
  constant d14_num_l: integer:=445;
  constant d14_num_r: integer:=452;
  constant d24_num_t: integer:=174;
  constant d24_num_b: integer:=182;
  constant d24_num_l: integer:=370;
  constant d24_num_r: integer:=377;
  
  --- player symbol 4boundary
  constant p14_col_t: integer:=275;
  constant p14_col_b: integer:=283;
  constant p14_col_l: integer:=370;
  constant p14_col_r: integer:=378;
  constant p24_col_t: integer:=369;
  constant p24_col_b: integer:=377;
  constant p24_col_l: integer:=445;
  constant p24_col_r: integer:=453;
  --- player number4 boundary
  constant p14_num_t: integer:=275;
  constant p14_num_b: integer:=283;
  constant p14_num_l: integer:=445;
  constant p14_num_r: integer:=452;
  constant p24_num_t: integer:=369;
  constant p24_num_b: integer:=377;
  constant p24_num_l: integer:=370;
  constant p24_num_r: integer:=377;



 --- player score
constant p_score_num1_t: integer:=465;
constant p_score_num1_b: integer:=473;
constant p_score_num1_l: integer:=67;
constant p_score_num1_r: integer:=74;
constant p_score_num2_t: integer:=465;
constant p_score_num2_b: integer:=473;
constant p_score_num2_l: integer:=77;
constant p_score_num2_r: integer:=84;
--- Player Bet
constant p_bet_num1_t: integer:=465;
constant p_bet_num1_b: integer:=473;
constant p_bet_num1_l: integer:=127;
constant p_bet_num1_r: integer:=134;
constant p_bet_num2_t: integer:=465;
constant p_bet_num2_b: integer:=473;
constant p_bet_num2_l: integer:=137;
constant p_bet_num2_r: integer:=144;
--- Player budget
constant p_budget_num1_t: integer:=465;
constant p_budget_num1_b: integer:=473;
constant p_budget_num1_l: integer:=217;
constant p_budget_num1_r: integer:=224;
constant p_budget_num2_t: integer:=465;
constant p_budget_num2_b: integer:=473;
constant p_budget_num2_l: integer:=227;
constant p_budget_num2_r: integer:=234;
constant p_budget_num3_t: integer:=465;
constant p_budget_num3_b: integer:=473;
constant p_budget_num3_l: integer:=237;
constant p_budget_num3_r: integer:=244;
--- dealer score
constant d_score_num1_t: integer:=12;
constant d_score_num1_b: integer:=20;
constant d_score_num1_l: integer:=67;
constant d_score_num1_r: integer:=74;
constant d_score_num2_t: integer:=12;
constant d_score_num2_b: integer:=20;
constant d_score_num2_l: integer:=77;
constant d_score_num2_r: integer:=84;
  
 --- card types definitions
type card_color is array (0 to 8) of std_logic_vector(0 to 8);
constant hearts: card_color := (
"011000110",
"111101111",
"111111111",
"111111111",
"111111111",
"011111110",
"001111100",
"000111000",
"000010000");
constant spades: card_color :=(
"000111000",
"000111000",
"000111000",
"111010111",
"111111111",
"111010111",
"000111000",
"001111100",
"111111111");
constant clubs: card_color :=(
"000010000",
"000111000",
"001111100",
"011111110",
"111111111",
"111111111",
"110111011",
"000111000",
"111111111");
constant diamond: card_color :=(
"000010000",
"000111000",
"001111100",
"011111110",
"111111111",
"011111110",
"001111100",
"000111000",
"000010000");
--- Numbers ROM
type num_type is array(0 to 8) of std_logic_vector(7 downto 0);
constant num0: num_type:=(
    "01111100", -- 2  *****
    "11000110", -- 3 **   **
    "11001110", -- 5 **  ***
    "11011110", -- 6 ** ****
    "11110110", -- 7 **** **
    "11100110", -- 8 ***  **
    "11000110", -- 9 **   **
    "11000110", -- a **   **
    "01111100"); -- b  *****
constant num1: num_type:=(
   "00011000", -- 2
   "00111000", -- 3
   "01111000", -- 4    **
   "00011000", -- 5   ***
   "00011000", -- 6  ****
   "00011000", -- 8    **
   "00011000", -- 9    **
   "00011000", -- a    **
   "01111110"); -- b    **
constant numA: num_type:=(
    "00010000", -- 2    *
    "00111000", -- 3   ***
    "01101100", -- 4  ** **
    "11000110", -- 5 **   **
    "11000110", -- 6 **   **
    "11111110", -- 7 *******
    "11000110", -- 8 **   **
    "11000110", -- 9 **   **
    "11000110"); -- a **   **
constant numJ:num_type:=(
    "00011110", -- 2    ****
    "00001100", -- 4     **
    "00001100", -- 5     **
    "00001100", -- 6     **
    "00001100", -- 7     **
    "11001100", -- 8 **  **
    "11001100", -- 9 **  **
    "11001100", -- a **  **
    "01111000"); -- b  ****
constant numK: num_type:=(
    "11100110", -- 2 ***  **
    "01100110", -- 3  **  **
    "01100110", -- 4  **  **
    "01101100", -- 5  ** **
    "01111000", -- 6  ****
    "01101100", -- 8  ** **
    "01100110", -- 9  **  **
    "01100110", -- a  **  **
    "11100110"); -- b ***  **
constant numQ:num_type:=(
    "01111100", -- 2  *****
    "11000110", -- 3 **   **
    "11000110", -- 6 **   **
    "11000110", -- 8 **   **
    "11010110", -- 9 ** * **
    "11011110", -- a ** ****
    "01111100", -- b  *****
    "00001100", -- c     **
    "00001110"); -- d     ***
constant num2:num_type:=(
   "01111100", -- 2  *****
   "11000110", -- 3 **   **
   "00000110", -- 4      **
   "00001100", -- 5     **
   "00011000", -- 6    **
   "00110000", -- 7   **
   "01100000",
   "11000110", -- a **   **
   "11111110"); -- b *******
constant num3: num_type:=(
   "01111100", -- 2  *****
   "11000110", -- 3 **   **
   "00000110", -- 4      **
   "00000110", -- 5      **
   "00111100", -- 6   ****
   "00000110", -- 7      **
   "00000110",
   "11000110", -- a **   **
   "01111100"); -- b  *****
   -- code x34
constant num4: num_type:=(

   "00001100", -- 2     **
   "00011100", -- 3    ***
   "00111100", -- 4   ****
   "01101100", -- 5  ** **
   "11001100", -- 6 **  **
   "11111110", -- 7 *******
   "00001100", -- 8     **
   "00001100", -- 9     **
   "00001100"); -- a     **
constant num5: num_type:=(
   "11111110", -- 2 *******
   "11000000", -- 3 **
   "11000000", -- 4 **
   "11000000", -- 5 **
   "11111100", -- 6 ******
   "00000110",
   "00000110", -- 9      **
   "11000110", -- a **   **
   "01111100");
constant num6: num_type:=(
    "00111000", -- 2   ***
   "01100000", -- 3  **
   "11000000", -- 4 **
   "11000000", -- 5 **
   "11111100", -- 6 ******
   "11000110", -- 7 **   **
   "11000110", -- 9 **   **
   "11000110", -- a **   **
   "01111100"); -- b  *****
constant num7: num_type:=(
   "11111110", -- 2 *******
   "11000110", -- 3 **   **
   "00000110", -- 4      **
   "00000110", -- 5      **
   "00001100", -- 6     **
   "00011000", -- 7    **
   "00110000", -- 8   **
   "00110000", -- 9   **
   "00110000");
constant num8: num_type:=(
   "01111100", -- 2  *****
   "11000110", -- 3 **   **
   "11000110", -- 4 **   **
   "11000110", -- 5 **   **
   "01111100", -- 6  *****
   "11000110", -- 8 **   **
   "11000110", -- 9 **   **
   "11000110", -- a **   **
   "01111100");
constant num9: num_type :=(
   "01111100", -- 2  *****
   "11000110", -- 3 **   **
   "11000110", -- 4 **   **
   "11000110", -- 5 **   **
   "01111110", -- 6  ******
   "00000110", -- 7      **
   "00000110", -- 9      **
   "00001100", -- a     **
   "01111000");
   
   
--- symbol 1signals
    signal d11s_col_t: unsigned(9 downto 0):= to_unsigned(d11_col_t,10);
    signal d11s_col_b: unsigned(9 downto 0):= to_unsigned(d11_col_b,10);
    signal d11s_col_l: unsigned(9 downto 0):=to_unsigned(d11_col_l,10);
    signal d11s_col_r: unsigned(9 downto 0):=to_unsigned(d11_col_r,10);
    signal d21s_col_t: unsigned(9 downto 0):= to_unsigned(d21_col_t,10);
    signal d21s_col_b: unsigned(9 downto 0):= to_unsigned(d21_col_b,10);
    signal d21s_col_l: unsigned(9 downto 0):=to_unsigned(d21_col_l,10);
    signal d21s_col_r: unsigned(9 downto 0):=to_unsigned(d21_col_r,10);
    signal p11s_col_t: unsigned(9 downto 0):= to_unsigned(p11_col_t,10);
    signal p11s_col_b: unsigned(9 downto 0):= to_unsigned(p11_col_b,10);
    signal p11s_col_l: unsigned(9 downto 0):=to_unsigned(p11_col_l,10);
    signal p11s_col_r: unsigned(9 downto 0):=to_unsigned(p11_col_r,10);
    signal p21s_col_t: unsigned(9 downto 0):= to_unsigned(p21_col_t,10);
    signal p21s_col_b: unsigned(9 downto 0):= to_unsigned(p21_col_b,10);
    signal p21s_col_l: unsigned(9 downto 0):=to_unsigned(p21_col_l,10);
    signal p21s_col_r: unsigned(9 downto 0):=to_unsigned(p21_col_r,10);
    --- number unsigned 1declaration
    signal d11s_num_t: unsigned(9 downto 0):= to_unsigned(d11_num_t,10);
    signal d11s_num_b: unsigned(9 downto 0):= to_unsigned(d11_num_b,10);
    signal d11s_num_l: unsigned(9 downto 0):=to_unsigned(d11_num_l,10);
    signal d11s_num_r: unsigned(9 downto 0):=to_unsigned(d11_num_r,10);
    signal d21s_num_t: unsigned(9 downto 0):= to_unsigned(d21_num_t,10);
    signal d21s_num_b: unsigned(9 downto 0):= to_unsigned(d21_num_b,10);
    signal d21s_num_l: unsigned(9 downto 0):=to_unsigned(d21_num_l,10);
    signal d21s_num_r: unsigned(9 downto 0):=to_unsigned(d21_num_r,10);
    signal p11s_num_t: unsigned(9 downto 0):= to_unsigned(p11_num_t,10);
    signal p11s_num_b: unsigned(9 downto 0):= to_unsigned(p11_num_b,10);
    signal p11s_num_l: unsigned(9 downto 0):=to_unsigned(p11_num_l,10);
    signal p11s_num_r: unsigned(9 downto 0):=to_unsigned(p11_num_r,10);
    signal p21s_num_t: unsigned(9 downto 0):= to_unsigned(p21_num_t,10);
    signal p21s_num_b: unsigned(9 downto 0):= to_unsigned(p21_num_b,10);
    signal p21s_num_l: unsigned(9 downto 0):=to_unsigned(p21_num_l,10);
    signal p21s_num_r: unsigned(9 downto 0):=to_unsigned(p21_num_r,10);
   
   --- symbol 2signals
    signal d12s_col_t: unsigned(9 downto 0):= to_unsigned(d12_col_t,10);
    signal d12s_col_b: unsigned(9 downto 0):= to_unsigned(d12_col_b,10);
    signal d12s_col_l: unsigned(9 downto 0):=to_unsigned(d12_col_l,10);
    signal d12s_col_r: unsigned(9 downto 0):=to_unsigned(d12_col_r,10);
    signal d22s_col_t: unsigned(9 downto 0):= to_unsigned(d22_col_t,10);
    signal d22s_col_b: unsigned(9 downto 0):= to_unsigned(d22_col_b,10);
    signal d22s_col_l: unsigned(9 downto 0):=to_unsigned(d22_col_l,10);
    signal d22s_col_r: unsigned(9 downto 0):=to_unsigned(d22_col_r,10);
    signal p12s_col_t: unsigned(9 downto 0):= to_unsigned(p12_col_t,10);
    signal p12s_col_b: unsigned(9 downto 0):= to_unsigned(p12_col_b,10);
    signal p12s_col_l: unsigned(9 downto 0):=to_unsigned(p12_col_l,10);
    signal p12s_col_r: unsigned(9 downto 0):=to_unsigned(p12_col_r,10);
    signal p22s_col_t: unsigned(9 downto 0):= to_unsigned(p22_col_t,10);
    signal p22s_col_b: unsigned(9 downto 0):= to_unsigned(p22_col_b,10);
    signal p22s_col_l: unsigned(9 downto 0):=to_unsigned(p22_col_l,10);
    signal p22s_col_r: unsigned(9 downto 0):=to_unsigned(p22_col_r,10);
    --- number unsigned2 declaration
    signal d12s_num_t: unsigned(9 downto 0):= to_unsigned(d12_num_t,10);
    signal d12s_num_b: unsigned(9 downto 0):= to_unsigned(d12_num_b,10);
    signal d12s_num_l: unsigned(9 downto 0):=to_unsigned(d12_num_l,10);
    signal d12s_num_r: unsigned(9 downto 0):=to_unsigned(d12_num_r,10);
    signal d22s_num_t: unsigned(9 downto 0):= to_unsigned(d22_num_t,10);
    signal d22s_num_b: unsigned(9 downto 0):= to_unsigned(d22_num_b,10);
    signal d22s_num_l: unsigned(9 downto 0):=to_unsigned(d22_num_l,10);
    signal d22s_num_r: unsigned(9 downto 0):=to_unsigned(d22_num_r,10);
    signal p12s_num_t: unsigned(9 downto 0):= to_unsigned(p12_num_t,10);
    signal p12s_num_b: unsigned(9 downto 0):= to_unsigned(p12_num_b,10);
    signal p12s_num_l: unsigned(9 downto 0):=to_unsigned(p12_num_l,10);
    signal p12s_num_r: unsigned(9 downto 0):=to_unsigned(p12_num_r,10);
    signal p22s_num_t: unsigned(9 downto 0):= to_unsigned(p22_num_t,10);
    signal p22s_num_b: unsigned(9 downto 0):= to_unsigned(p22_num_b,10);
    signal p22s_num_l: unsigned(9 downto 0):=to_unsigned(p22_num_l,10);
    signal p22s_num_r: unsigned(9 downto 0):=to_unsigned(p22_num_r,10);
   
   --- symbol signals3
    signal d13s_col_t: unsigned(9 downto 0):= to_unsigned(d13_col_t,10);
    signal d13s_col_b: unsigned(9 downto 0):= to_unsigned(d13_col_b,10);
    signal d13s_col_l: unsigned(9 downto 0):=to_unsigned(d13_col_l,10);
    signal d13s_col_r: unsigned(9 downto 0):=to_unsigned(d13_col_r,10);
    signal d23s_col_t: unsigned(9 downto 0):= to_unsigned(d23_col_t,10);
    signal d23s_col_b: unsigned(9 downto 0):= to_unsigned(d23_col_b,10);
    signal d23s_col_l: unsigned(9 downto 0):=to_unsigned(d23_col_l,10);
    signal d23s_col_r: unsigned(9 downto 0):=to_unsigned(d23_col_r,10);
    signal p13s_col_t: unsigned(9 downto 0):= to_unsigned(p13_col_t,10);
    signal p13s_col_b: unsigned(9 downto 0):= to_unsigned(p13_col_b,10);
    signal p13s_col_l: unsigned(9 downto 0):=to_unsigned(p13_col_l,10);
    signal p13s_col_r: unsigned(9 downto 0):=to_unsigned(p13_col_r,10);
    signal p23s_col_t: unsigned(9 downto 0):= to_unsigned(p23_col_t,10);
    signal p23s_col_b: unsigned(9 downto 0):= to_unsigned(p23_col_b,10);
    signal p23s_col_l: unsigned(9 downto 0):=to_unsigned(p23_col_l,10);
    signal p23s_col_r: unsigned(9 downto 0):=to_unsigned(p23_col_r,10);
    --- number unsigned declaration3
    signal d13s_num_t: unsigned(9 downto 0):= to_unsigned(d13_num_t,10);
    signal d13s_num_b: unsigned(9 downto 0):= to_unsigned(d13_num_b,10);
    signal d13s_num_l: unsigned(9 downto 0):=to_unsigned(d13_num_l,10);
    signal d13s_num_r: unsigned(9 downto 0):=to_unsigned(d13_num_r,10);
    signal d23s_num_t: unsigned(9 downto 0):= to_unsigned(d23_num_t,10);
    signal d23s_num_b: unsigned(9 downto 0):= to_unsigned(d23_num_b,10);
    signal d23s_num_l: unsigned(9 downto 0):=to_unsigned(d23_num_l,10);
    signal d23s_num_r: unsigned(9 downto 0):=to_unsigned(d23_num_r,10);
    signal p13s_num_t: unsigned(9 downto 0):= to_unsigned(p13_num_t,10);
    signal p13s_num_b: unsigned(9 downto 0):= to_unsigned(p13_num_b,10);
    signal p13s_num_l: unsigned(9 downto 0):=to_unsigned(p13_num_l,10);
    signal p13s_num_r: unsigned(9 downto 0):=to_unsigned(p13_num_r,10);
    signal p23s_num_t: unsigned(9 downto 0):= to_unsigned(p23_num_t,10);
    signal p23s_num_b: unsigned(9 downto 0):= to_unsigned(p23_num_b,10);
    signal p23s_num_l: unsigned(9 downto 0):=to_unsigned(p23_num_l,10);
    signal p23s_num_r: unsigned(9 downto 0):=to_unsigned(p23_num_r,10);
    
   --- symbol signals4
     signal d14s_col_t: unsigned(9 downto 0):= to_unsigned(d14_col_t,10);
     signal d14s_col_b: unsigned(9 downto 0):= to_unsigned(d14_col_b,10);
     signal d14s_col_l: unsigned(9 downto 0):=to_unsigned(d14_col_l,10);
     signal d14s_col_r: unsigned(9 downto 0):=to_unsigned(d14_col_r,10);
     signal d24s_col_t: unsigned(9 downto 0):= to_unsigned(d24_col_t,10);
     signal d24s_col_b: unsigned(9 downto 0):= to_unsigned(d24_col_b,10);
     signal d24s_col_l: unsigned(9 downto 0):=to_unsigned(d24_col_l,10);
     signal d24s_col_r: unsigned(9 downto 0):=to_unsigned(d24_col_r,10);
     signal p14s_col_t: unsigned(9 downto 0):= to_unsigned(p14_col_t,10);
     signal p14s_col_b: unsigned(9 downto 0):= to_unsigned(p14_col_b,10);
     signal p14s_col_l: unsigned(9 downto 0):=to_unsigned(p14_col_l,10);
     signal p14s_col_r: unsigned(9 downto 0):=to_unsigned(p14_col_r,10);
     signal p24s_col_t: unsigned(9 downto 0):= to_unsigned(p24_col_t,10);
     signal p24s_col_b: unsigned(9 downto 0):= to_unsigned(p24_col_b,10);
     signal p24s_col_l: unsigned(9 downto 0):=to_unsigned(p24_col_l,10);
     signal p24s_col_r: unsigned(9 downto 0):=to_unsigned(p24_col_r,10);
     
     --- number unsigned declaration4
     signal d14s_num_t: unsigned(9 downto 0):= to_unsigned(d14_num_t,10);
     signal d14s_num_b: unsigned(9 downto 0):= to_unsigned(d14_num_b,10);
     signal d14s_num_l: unsigned(9 downto 0):=to_unsigned(d14_num_l,10);
     signal d14s_num_r: unsigned(9 downto 0):=to_unsigned(d14_num_r,10);
     signal d24s_num_t: unsigned(9 downto 0):= to_unsigned(d24_num_t,10);
     signal d24s_num_b: unsigned(9 downto 0):= to_unsigned(d24_num_b,10);
     signal d24s_num_l: unsigned(9 downto 0):=to_unsigned(d24_num_l,10);
     signal d24s_num_r: unsigned(9 downto 0):=to_unsigned(d24_num_r,10);
     signal p14s_num_t: unsigned(9 downto 0):= to_unsigned(p14_num_t,10);
     signal p14s_num_b: unsigned(9 downto 0):= to_unsigned(p14_num_b,10);
     signal p14s_num_l: unsigned(9 downto 0):=to_unsigned(p14_num_l,10);
     signal p14s_num_r: unsigned(9 downto 0):=to_unsigned(p14_num_r,10);
     signal p24s_num_t: unsigned(9 downto 0):= to_unsigned(p24_num_t,10);
     signal p24s_num_b: unsigned(9 downto 0):= to_unsigned(p24_num_b,10);
     signal p24s_num_l: unsigned(9 downto 0):=to_unsigned(p24_num_l,10);
     signal p24s_num_r: unsigned(9 downto 0):=to_unsigned(p24_num_r,10);

 --- score unsigned player
 signal ps_score_num1_t: unsigned(9 downto 0):= to_unsigned(p_score_num1_t,10);
 signal ps_score_num1_b: unsigned(9 downto 0):= to_unsigned(p_score_num1_b,10);
 signal ps_score_num1_l: unsigned(9 downto 0):=to_unsigned(p_score_num1_l,10);
 signal ps_score_num1_r: unsigned(9 downto 0):=to_unsigned(p_score_num1_r,10);
 signal ps_score_num2_t: unsigned(9 downto 0):= to_unsigned(p_score_num2_t,10);
 signal ps_score_num2_b: unsigned(9 downto 0):= to_unsigned(p_score_num2_b,10);
 signal ps_score_num2_l: unsigned(9 downto 0):=to_unsigned(p_score_num2_l,10);
 signal ps_score_num2_r: unsigned(9 downto 0):=to_unsigned(p_score_num2_r,10);
 --- score unsigned dealer
 signal ds_score_num1_t: unsigned(9 downto 0):= to_unsigned(d_score_num1_t,10);
 signal ds_score_num1_b: unsigned(9 downto 0):= to_unsigned(d_score_num1_b,10);
 signal ds_score_num1_l: unsigned(9 downto 0):=to_unsigned(d_score_num1_l,10);
 signal ds_score_num1_r: unsigned(9 downto 0):=to_unsigned(d_score_num1_r,10);
 signal ds_score_num2_t: unsigned(9 downto 0):= to_unsigned(d_score_num2_t,10);
 signal ds_score_num2_b: unsigned(9 downto 0):= to_unsigned(d_score_num2_b,10);
 signal ds_score_num2_l: unsigned(9 downto 0):=to_unsigned(d_score_num2_l,10);
 signal ds_score_num2_r: unsigned(9 downto 0):=to_unsigned(d_score_num2_r,10);
 --- bet unsigned player
 signal ps_bet_num1_t: unsigned(9 downto 0):= to_unsigned(p_bet_num1_t,10);
 signal ps_bet_num1_b: unsigned(9 downto 0):= to_unsigned(p_bet_num1_b,10);
 signal ps_bet_num1_l: unsigned(9 downto 0):=to_unsigned(p_bet_num1_l,10);
 signal ps_bet_num1_r: unsigned(9 downto 0):=to_unsigned(p_bet_num1_r,10);
 signal ps_bet_num2_t: unsigned(9 downto 0):= to_unsigned(p_bet_num2_t,10);
 signal ps_bet_num2_b: unsigned(9 downto 0):= to_unsigned(p_bet_num2_b,10);
 signal ps_bet_num2_l: unsigned(9 downto 0):=to_unsigned(p_bet_num2_l,10);
 signal ps_bet_num2_r: unsigned(9 downto 0):=to_unsigned(p_bet_num2_r,10);
--- budget unsigned
 signal ps_budget_num1_t: unsigned(9 downto 0):= to_unsigned(p_budget_num1_t,10);
 signal ps_budget_num1_b: unsigned(9 downto 0):= to_unsigned(p_budget_num1_b,10);
 signal ps_budget_num1_l: unsigned(9 downto 0):=to_unsigned(p_budget_num1_l,10);
 signal ps_budget_num1_r: unsigned(9 downto 0):=to_unsigned(p_budget_num1_r,10);
 signal ps_budget_num2_t: unsigned(9 downto 0):= to_unsigned(p_budget_num2_t,10);
 signal ps_budget_num2_b: unsigned(9 downto 0):= to_unsigned(p_budget_num2_b,10);
 signal ps_budget_num2_l: unsigned(9 downto 0):=to_unsigned(p_budget_num2_l,10);
 signal ps_budget_num2_r: unsigned(9 downto 0):=to_unsigned(p_budget_num2_r,10); 
 signal ps_budget_num3_t: unsigned(9 downto 0):= to_unsigned(p_budget_num3_t,10);
 signal ps_budget_num3_b: unsigned(9 downto 0):= to_unsigned(p_budget_num3_b,10);
 signal ps_budget_num3_l: unsigned(9 downto 0):=to_unsigned(p_budget_num3_l,10);
 signal ps_budget_num3_r: unsigned(9 downto 0):=to_unsigned(p_budget_num3_r,10); 

signal p_x,p_y: unsigned(9 downto 0);
--- card types 1
signal d11_cd_type_addr,d11_cd_type_col: unsigned(3 downto 0);
signal d11_cd_type_data: std_logic_vector(8 downto 0);
signal d11_cd_type_bit: std_logic;
signal p11_cd_type_addr,p11_cd_type_col: unsigned(3 downto 0);
signal p11_cd_type_data: std_logic_vector(8 downto 0);
signal p11_cd_type_bit: std_logic;
signal d21_cd_type_addr,d21_cd_type_col: unsigned(3 downto 0);
signal d21_cd_type_data: std_logic_vector(8 downto 0);
signal d21_cd_type_bit: std_logic;
signal p21_cd_type_addr,p21_cd_type_col: unsigned(3 downto 0);
signal p21_cd_type_data: std_logic_vector(8 downto 0);
signal p21_cd_type_bit: std_logic;

--- for numbers 1
signal d11_cd_num_addr,d11_cd_num_col: unsigned(3 downto 0);
signal d11_cd_num_data: std_logic_vector(7 downto 0);
signal d11_cd_num_bit: std_logic;
signal p11_cd_num_addr,p11_cd_num_col: unsigned(3 downto 0);
signal p11_cd_num_data: std_logic_vector(7 downto 0);
signal p11_cd_num_bit: std_logic;
signal d21_cd_num_addr,d21_cd_num_col: unsigned(3 downto 0);
signal d21_cd_num_data: std_logic_vector(7 downto 0);
signal d21_cd_num_bit: std_logic;
signal p21_cd_num_addr,p21_cd_num_col: unsigned(3 downto 0);
signal p21_cd_num_data: std_logic_vector(7 downto 0);
signal p21_cd_num_bit: std_logic;

--- card types 2
signal d12_cd_type_addr,d12_cd_type_col: unsigned(3 downto 0);
signal d12_cd_type_data: std_logic_vector(8 downto 0);
signal d12_cd_type_bit: std_logic;
signal p12_cd_type_addr,p12_cd_type_col: unsigned(3 downto 0);
signal p12_cd_type_data: std_logic_vector(8 downto 0);
signal p12_cd_type_bit: std_logic;
signal d22_cd_type_addr,d22_cd_type_col: unsigned(3 downto 0);
signal d22_cd_type_data: std_logic_vector(8 downto 0);
signal d22_cd_type_bit: std_logic;
signal p22_cd_type_addr,p22_cd_type_col: unsigned(3 downto 0);
signal p22_cd_type_data: std_logic_vector(8 downto 0);
signal p22_cd_type_bit: std_logic;

--- for numbers 2
signal d12_cd_num_addr,d12_cd_num_col: unsigned(3 downto 0);
signal d12_cd_num_data: std_logic_vector(7 downto 0);
signal d12_cd_num_bit: std_logic;
signal p12_cd_num_addr,p12_cd_num_col: unsigned(3 downto 0);
signal p12_cd_num_data: std_logic_vector(7 downto 0);
signal p12_cd_num_bit: std_logic;
signal d22_cd_num_addr,d22_cd_num_col: unsigned(3 downto 0);
signal d22_cd_num_data: std_logic_vector(7 downto 0);
signal d22_cd_num_bit: std_logic;
signal p22_cd_num_addr,p22_cd_num_col: unsigned(3 downto 0);
signal p22_cd_num_data: std_logic_vector(7 downto 0);
signal p22_cd_num_bit: std_logic;

--- card types 3
signal d13_cd_type_addr,d13_cd_type_col: unsigned(3 downto 0);
signal d13_cd_type_data: std_logic_vector(8 downto 0);
signal d13_cd_type_bit: std_logic;
signal p13_cd_type_addr,p13_cd_type_col: unsigned(3 downto 0);
signal p13_cd_type_data: std_logic_vector(8 downto 0);
signal p13_cd_type_bit: std_logic;
signal d23_cd_type_addr,d23_cd_type_col: unsigned(3 downto 0);
signal d23_cd_type_data: std_logic_vector(8 downto 0);
signal d23_cd_type_bit: std_logic;
signal p23_cd_type_addr,p23_cd_type_col: unsigned(3 downto 0);
signal p23_cd_type_data: std_logic_vector(8 downto 0);
signal p23_cd_type_bit: std_logic;

--- for numbers 3
signal d13_cd_num_addr,d13_cd_num_col: unsigned(3 downto 0);
signal d13_cd_num_data: std_logic_vector(7 downto 0);
signal d13_cd_num_bit: std_logic;
signal p13_cd_num_addr,p13_cd_num_col: unsigned(3 downto 0);
signal p13_cd_num_data: std_logic_vector(7 downto 0);
signal p13_cd_num_bit: std_logic;
signal d23_cd_num_addr,d23_cd_num_col: unsigned(3 downto 0);
signal d23_cd_num_data: std_logic_vector(7 downto 0);
signal d23_cd_num_bit: std_logic;
signal p23_cd_num_addr,p23_cd_num_col: unsigned(3 downto 0);
signal p23_cd_num_data: std_logic_vector(7 downto 0);
signal p23_cd_num_bit: std_logic;

--- card types 4
signal d14_cd_type_addr,d14_cd_type_col: unsigned(3 downto 0);
signal d14_cd_type_data: std_logic_vector(8 downto 0);
signal d14_cd_type_bit: std_logic;
signal p14_cd_type_addr,p14_cd_type_col: unsigned(3 downto 0);
signal p14_cd_type_data: std_logic_vector(8 downto 0);
signal p14_cd_type_bit: std_logic;
signal d24_cd_type_addr,d24_cd_type_col: unsigned(3 downto 0);
signal d24_cd_type_data: std_logic_vector(8 downto 0);
signal d24_cd_type_bit: std_logic;
signal p24_cd_type_addr,p24_cd_type_col: unsigned(3 downto 0);
signal p24_cd_type_data: std_logic_vector(8 downto 0);
signal p24_cd_type_bit: std_logic;

--- for numbers 4
signal d14_cd_num_addr,d14_cd_num_col: unsigned(3 downto 0);
signal d14_cd_num_data: std_logic_vector(7 downto 0);
signal d14_cd_num_bit: std_logic;
signal p14_cd_num_addr,p14_cd_num_col: unsigned(3 downto 0);
signal p14_cd_num_data: std_logic_vector(7 downto 0);
signal p14_cd_num_bit: std_logic;
signal d24_cd_num_addr,d24_cd_num_col: unsigned(3 downto 0);
signal d24_cd_num_data: std_logic_vector(7 downto 0);
signal d24_cd_num_bit: std_logic;
signal p24_cd_num_addr,p24_cd_num_col: unsigned(3 downto 0);
signal p24_cd_num_data: std_logic_vector(7 downto 0);
signal p24_cd_num_bit: std_logic;

--- score address data and bit
signal p_score_num1_addr,p_score_num1_col: unsigned(3 downto 0);
signal p_score_num1_data: std_logic_vector(7 downto 0);
signal p_score_num1_bit: std_logic;
signal p_score_num2_addr,p_score_num2_col: unsigned(3 downto 0);
signal p_score_num2_data: std_logic_vector(7 downto 0);
signal p_score_num2_bit: std_logic;
signal p_score_int: integer range 0 to 31;

signal d_score_num1_addr,d_score_num1_col: unsigned(3 downto 0);
signal d_score_num1_data: std_logic_vector(7 downto 0);
signal d_score_num1_bit: std_logic;
signal d_score_num2_addr,d_score_num2_col: unsigned(3 downto 0);
signal d_score_num2_data: std_logic_vector(7 downto 0);
signal d_score_num2_bit: std_logic;
signal d_score_int: integer range 0 to 31;
--- bet address data and bit

signal p_bet_num1_addr,p_bet_num1_col: unsigned(3 downto 0);
signal p_bet_num1_data: std_logic_vector(7 downto 0);
signal p_bet_num1_bit: std_logic;
signal p_bet_num2_addr,p_bet_num2_col: unsigned(3 downto 0);
signal p_bet_num2_data: std_logic_vector(7 downto 0);
signal p_bet_num2_bit: std_logic;
signal p_bet_int: integer range 0 to 16;
--- budget address data and bit
signal p_budget_num1_addr,p_budget_num1_col: unsigned(3 downto 0);
signal p_budget_num1_data: std_logic_vector(7 downto 0);
signal p_budget_num1_bit: std_logic;
signal p_budget_num2_addr,p_budget_num2_col: unsigned(3 downto 0);
signal p_budget_num2_data: std_logic_vector(7 downto 0);
signal p_budget_num2_bit: std_logic;
signal p_budget_num3_addr,p_budget_num3_col: unsigned(3 downto 0);
signal p_budget_num3_data: std_logic_vector(7 downto 0);
signal p_budget_num3_bit: std_logic;
signal p_budget_int: integer range 0 to 256;

--signal for symbols
signal sym1_on, sym2_on, sym3_on: std_logic;
signal bar_on:std_logic;

--- bits on 1
signal dcard1_bd_on,pcard1_bd_on,dcard1_sym_bit_on,pcard1_sym_bit_on: std_logic;
signal dcard1_num_bit_on,pcard1_num_bit_on: std_logic;
signal dcard1_sym_on,pcard1_sym_on, dcard1_num_on,pcard1_num_on: std_logic_vector (1 downto 0);

--- bits on 2
signal dcard2_bd_on,pcard2_bd_on,dcard2_sym_bit_on,pcard2_sym_bit_on: std_logic;
signal dcard2_num_bit_on,pcard2_num_bit_on: std_logic;
signal dcard2_sym_on,pcard2_sym_on, dcard2_num_on,pcard2_num_on: std_logic_vector (1 downto 0);

--- bits on 3
signal dcard3_bd_on,pcard3_bd_on,dcard3_sym_bit_on,pcard3_sym_bit_on: std_logic;
signal dcard3_num_bit_on,pcard3_num_bit_on: std_logic;
signal dcard3_sym_on,pcard3_sym_on,dcard3_num_on,pcard3_num_on: std_logic_vector (1 downto 0);

--- bits on 4
signal dcard4_bd_on,pcard4_bd_on,dcard4_sym_bit_on,pcard4_sym_bit_on: std_logic;
signal dcard4_num_bit_on,pcard4_num_bit_on: std_logic;
signal dcard4_sym_on,pcard4_sym_on,dcard4_num_on,pcard4_num_on: std_logic_vector (1 downto 0);


signal p_bet_on, p_budget_on: std_logic_vector(1 downto 0);
signal p_score1_on,p_score2_on,d_score1_on,d_score2_on: std_logic;
constant score_rgb,bet_rgb,budget_rgb: std_logic_vector(15 downto 0):="1010101010101010";
signal bar_rgb: std_logic_vector(15 downto 0);
---rgb on 1
signal pcard1_rgb,dcard1_rgb: std_logic_vector(15 downto 0);
---rgb on 2
signal pcard2_rgb,dcard2_rgb: std_logic_vector(15 downto 0);
---rgb on 3
signal pcard3_rgb,dcard3_rgb: std_logic_vector(15 downto 0);
---rgb on 4
signal pcard4_rgb,dcard4_rgb: std_logic_vector(15 downto 0);


signal b1_on,b2_on,b3_on,b4_on : std_logic;
signal b1_rgb,b2_rgb,b3_rgb,b4_rgb: std_logic_vector(15 downto 0);
begin



    p_x <= unsigned(pixel_x);
    p_y <= unsigned(pixel_y);
    --- Normal boundaries
    b1_on <= '1' when (p_y>=Bar1_T AND p_y<=Bar1_B) else '0';
    b2_on <= '1' when (p_y>=Bar2_T AND p_y<=Bar2_B) else '0';
    b3_on <= '1' when (p_y>=Bar3_T AND p_y<=Bar3_B) else '0';
    b4_on <= '1' when (p_y>=Bar4_T AND p_y<=Bar4_B) else '0';
             
    
    b1_rgb <= "0000011111100000";
    b2_rgb <= "0000011111100000";
    b3_rgb <= "0000011111100000";
    b4_rgb <= "0000011111100000";
 
 --difficult symbol boundaries set
        sym1_on  <= '1' when((p_y>=Sym_T) AND (p_y<=Sym_B) AND (p_x>=Sym1_l) AND (p_x <=Sym1_r) AND ((diff_ch="01") OR (diff_ch="10") OR (diff_ch="11"))) else
                    '0';
        sym2_on  <= '1' when((p_y>=Sym_T) AND (p_y<=Sym_B) AND (p_x>=Sym2_l) AND (p_x <=Sym2_r) AND ((diff_ch="10") OR (diff_ch="11"))) else
                    '0';
        sym3_on  <= '1' when((p_y>=Sym_T) AND (p_y<=Sym_B) AND (p_x>=Sym3_l) AND (p_x <=Sym3_r) AND ((diff_ch="11"))) else
                    '0';         
                  
    --- Card boundaries set 1
        dcard1_bd_on <= '1' when(((p_y>=d1_cd_brdr_t1) AND (p_y<=d1_cd_brdr_t2) AND (p_x>=d1_cd_brdr_l1) AND (p_x <= d1_cd_brdr_r2))
                           OR   ((p_y>=d1_cd_brdr_b1) AND (p_y<=d1_cd_brdr_b2) AND (p_x>=d1_cd_brdr_l1) AND (p_x <= d1_cd_brdr_r2))
                           OR   ((p_x>=d1_cd_brdr_l1) AND (p_x<=d1_cd_brdr_l2) AND (p_y>=d1_cd_brdr_t1) AND (p_y <= d1_cd_brdr_b2))
                           OR   ((p_x>=d1_cd_brdr_r1) AND (p_x<=d1_cd_brdr_r2) AND (p_y>=d1_cd_brdr_t1) AND (p_y <= d1_cd_brdr_b2)))
                else '0';
        pcard1_bd_on <= '1' when(((p_y>=p1_cd_brdr_t1) AND (p_y<=p1_cd_brdr_t2) AND (p_x>=p1_cd_brdr_l1) AND (p_x <= p1_cd_brdr_r2))
                           OR   ((p_y>=p1_cd_brdr_b1) AND (p_y<=p1_cd_brdr_b2) AND (p_x>=p1_cd_brdr_l1) AND (p_x <= p1_cd_brdr_r2))
                           OR   ((p_x>=p1_cd_brdr_l1) AND (p_x<=p1_cd_brdr_l2) AND (p_y>=p1_cd_brdr_t1) AND (p_y <= p1_cd_brdr_b2))
                           OR   ((p_x>=p1_cd_brdr_r1) AND (p_x<=p1_cd_brdr_r2) AND (p_y>=p1_cd_brdr_t1) AND (p_y <= p1_cd_brdr_b2)))
                        else '0';
    
        --- Card boundaries set 2
        dcard2_bd_on <= '1' when(((p_y>=d2_cd_brdr_t1) AND (p_y<=d2_cd_brdr_t2) AND (p_x>=d2_cd_brdr_l1) AND (p_x <= d2_cd_brdr_r2))
                           OR   ((p_y>=d2_cd_brdr_b1) AND (p_y<=d2_cd_brdr_b2) AND (p_x>=d2_cd_brdr_l1) AND (p_x <= d2_cd_brdr_r2))
                           OR   ((p_x>=d2_cd_brdr_l1) AND (p_x<=d2_cd_brdr_l2) AND (p_y>=d2_cd_brdr_t1) AND (p_y <= d2_cd_brdr_b2))
                           OR   ((p_x>=d2_cd_brdr_r1) AND (p_x<=d2_cd_brdr_r2) AND (p_y>=d2_cd_brdr_t1) AND (p_y <= d2_cd_brdr_b2)))
                else '0';
        pcard2_bd_on <= '1' when(((p_y>=p2_cd_brdr_t1) AND (p_y<=p2_cd_brdr_t2) AND (p_x>=p2_cd_brdr_l1) AND (p_x <= p2_cd_brdr_r2))
                           OR   ((p_y>=p2_cd_brdr_b1) AND (p_y<=p2_cd_brdr_b2) AND (p_x>=p2_cd_brdr_l1) AND (p_x <= p2_cd_brdr_r2))
                           OR   ((p_x>=p2_cd_brdr_l1) AND (p_x<=p2_cd_brdr_l2) AND (p_y>=p2_cd_brdr_t1) AND (p_y <= p2_cd_brdr_b2))
                           OR   ((p_x>=p2_cd_brdr_r1) AND (p_x<=p2_cd_brdr_r2) AND (p_y>=p2_cd_brdr_t1) AND (p_y <= p2_cd_brdr_b2)))
                        else '0';
        --- Card boundaries set 3
        dcard3_bd_on <= '1' when(((p_y>=d3_cd_brdr_t1) AND (p_y<=d3_cd_brdr_t2) AND (p_x>=d3_cd_brdr_l1) AND (p_x <= d3_cd_brdr_r2))
                           OR   ((p_y>=d3_cd_brdr_b1) AND (p_y<=d3_cd_brdr_b2) AND (p_x>=d3_cd_brdr_l1) AND (p_x <= d3_cd_brdr_r2))
                           OR   ((p_x>=d3_cd_brdr_l1) AND (p_x<=d3_cd_brdr_l2) AND (p_y>=d3_cd_brdr_t1) AND (p_y <= d3_cd_brdr_b2))
                           OR   ((p_x>=d3_cd_brdr_r1) AND (p_x<=d3_cd_brdr_r2) AND (p_y>=d3_cd_brdr_t1) AND (p_y <= d3_cd_brdr_b2)))
                else '0';
        pcard3_bd_on <= '1' when(((p_y>=p3_cd_brdr_t1) AND (p_y<=p3_cd_brdr_t2) AND (p_x>=p3_cd_brdr_l1) AND (p_x <= p3_cd_brdr_r2))
                           OR   ((p_y>=p3_cd_brdr_b1) AND (p_y<=p3_cd_brdr_b2) AND (p_x>=p3_cd_brdr_l1) AND (p_x <= p3_cd_brdr_r2))
                           OR   ((p_x>=p3_cd_brdr_l1) AND (p_x<=p3_cd_brdr_l2) AND (p_y>=p3_cd_brdr_t1) AND (p_y <= p3_cd_brdr_b2))
                           OR   ((p_x>=p3_cd_brdr_r1) AND (p_x<=p3_cd_brdr_r2) AND (p_y>=p3_cd_brdr_t1) AND (p_y <= p3_cd_brdr_b2)))
                        else '0';
    
        --- Card boundaries set 4
        dcard4_bd_on <= '1' when(((p_y>=d4_cd_brdr_t1) AND (p_y<=d4_cd_brdr_t2) AND (p_x>=d4_cd_brdr_l1) AND (p_x <= d4_cd_brdr_r2))
                           OR   ((p_y>=d4_cd_brdr_b1) AND (p_y<=d4_cd_brdr_b2) AND (p_x>=d4_cd_brdr_l1) AND (p_x <= d4_cd_brdr_r2))
                           OR   ((p_x>=d4_cd_brdr_l1) AND (p_x<=d4_cd_brdr_l2) AND (p_y>=d4_cd_brdr_t1) AND (p_y <= d4_cd_brdr_b2))
                           OR   ((p_x>=d4_cd_brdr_r1) AND (p_x<=d4_cd_brdr_r2) AND (p_y>=d4_cd_brdr_t1) AND (p_y <= d4_cd_brdr_b2)))
                else '0';
        pcard4_bd_on <= '1' when(((p_y>=p4_cd_brdr_t1) AND (p_y<=p4_cd_brdr_t2) AND (p_x>=p4_cd_brdr_l1) AND (p_x <= p4_cd_brdr_r2))
                           OR   ((p_y>=p4_cd_brdr_b1) AND (p_y<=p4_cd_brdr_b2) AND (p_x>=p4_cd_brdr_l1) AND (p_x <= p4_cd_brdr_r2))
                           OR   ((p_x>=p4_cd_brdr_l1) AND (p_x<=p4_cd_brdr_l2) AND (p_y>=p4_cd_brdr_t1) AND (p_y <= p4_cd_brdr_b2))
                           OR   ((p_x>=p4_cd_brdr_r1) AND (p_x<=p4_cd_brdr_r2) AND (p_y>=p4_cd_brdr_t1) AND (p_y <= p4_cd_brdr_b2)))
                        else '0';
    --- symbol boundaries set
    --- player score
    p_score1_on <= '1' when ((p_y >= p_score_num1_t) AND (p_y<=p_score_num1_b) AND (p_x>=p_score_num1_l) AND (p_x <= p_score_num1_r))
                            else '0';
                                
    p_score_int <= to_integer(unsigned(p_score));
    p_score_num1_addr <= p_y(3 downto 0) - ps_score_num1_t(3 downto 0);
    p_score_num1_col <= ps_score_num1_r(3 downto 0) - p_x(3 downto 0);
    p_score_num1_data<= num0(to_integer(p_score_num1_addr)) when (p_score_int / 10 = 0) else
                        num1(to_integer(p_score_num1_addr)) when (p_score_int / 10 = 1) else
                        num2(to_integer(p_score_num1_addr)) when (p_score_int / 10 = 2) else
                        num3(to_integer(p_score_num1_addr));
    
    p_score2_on <= '1' when ((p_y>=p_score_num2_t) AND (p_y<=p_score_num2_b) AND (p_x>=p_score_num2_l) AND (p_x<=p_score_num2_r)) else
                   '0';
    
    p_score_num2_addr <= p_y(3 downto 0) - ps_score_num2_t(3 downto 0);
    p_score_num2_col <=ps_score_num2_r(3 downto 0) - p_x(3 downto 0); 
    p_score_num2_data<= num0(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 0) else
                            num1(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 1) else
                            num2(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 2) else
                            num3(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 3) else
                            num4(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 4) else
                            num5(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 5) else
                            num6(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 6) else
                            num7(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 7) else
                            num8(to_integer(p_score_num2_addr)) when (p_score_int mod 10 = 8) else
                            num9(to_integer(p_score_num2_addr));
    p_score_num1_bit <= p_score_num1_data(to_integer(p_score_num1_col));
    p_score_num2_bit <= p_score_num2_data(to_integer(p_score_num2_col));
    --- Dealer score
        d_score1_on <= '1' when ((p_y>=d_score_num1_t) AND (p_y<=d_score_num1_b) AND (p_x >=d_score_num1_l) AND (p_x<=d_score_num1_r))
                    else '0';
        d_score2_on <= '1' when ((p_y>=d_score_num2_t) AND (p_y<=d_score_num2_b) AND (p_x >=d_score_num2_l) AND (p_x<=d_score_num2_r))
                                else '0';
        d_score_int <= to_integer(unsigned(d_score));
        d_score_num1_addr <= p_y(3 downto 0) - ds_score_num1_t(3 downto 0);
        d_score_num1_col <=ds_score_num1_r(3 downto 0) - p_x(3 downto 0) ;
        d_score_num1_data<= num0(to_integer(d_score_num1_addr)) when (d_score_int / 10 = 0) else
                            num1(to_integer(d_score_num1_addr)) when (d_score_int / 10 = 1) else
                            num2(to_integer(d_score_num1_addr)) when (d_score_int / 10 = 2) else
                            num3(to_integer(d_score_num1_addr));
        
        d_score_num2_addr <= p_y(3 downto 0) - ds_score_num2_t(3 downto 0);
        d_score_num2_col <= ds_score_num2_r(3 downto 0) - p_x(3 downto 0) ; 
        d_score_num2_data<= num0(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 0) else
                                num1(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 1) else
                                num2(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 2) else
                                num3(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 3) else
                                num4(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 4) else
                                num5(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 5) else
                                num6(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 6) else
                                num7(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 7) else
                                num8(to_integer(d_score_num2_addr)) when (d_score_int mod 10 = 8) else
                                num9(to_integer(d_score_num2_addr));
        d_score_num1_bit <= d_score_num1_data(to_integer(d_score_num1_col));
        d_score_num2_bit <= d_score_num2_data(to_integer(d_score_num2_col));
    --- Player bet
         p_bet_on <= "01" when ((p_y>=p_bet_num1_t) AND (p_y<=p_bet_num1_b) AND (p_x>=p_bet_num1_l) AND (p_x<=p_bet_num1_r)) else
                     "10" when ((p_y>=p_bet_num2_t) AND (p_y<=p_bet_num2_b) AND (p_x>=p_bet_num2_l) AND (p_x<=p_bet_num2_r))
                                else "00";
        p_bet_int <= to_integer(unsigned(p_bet));
        p_bet_num1_addr <= p_y(3 downto 0) - ps_bet_num1_t(3 downto 0);
        p_bet_num1_col <= ps_bet_num1_r(3 downto 0) - p_x(3 downto 0)  ;
        p_bet_num1_data<= num0(to_integer(p_bet_num1_addr)) when (p_bet_int / 10 = 0) else
                            num1(to_integer(p_bet_num1_addr));
        
        p_bet_num2_addr <= p_y(3 downto 0) - ps_bet_num2_t(3 downto 0);
        p_bet_num2_col <= ps_bet_num2_r(3 downto 0) - p_x(3 downto 0);
        p_bet_num2_data<= num0(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 0) else
                                num1(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 1) else
                                num2(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 2) else
                                num3(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 3) else
                                num4(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 4) else
                                num5(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 5) else
                                num6(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 6) else
                                num7(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 7) else
                                num8(to_integer(p_bet_num2_addr)) when (p_bet_int mod 10 = 8) else
                                num9(to_integer(p_bet_num2_addr));
        p_bet_num1_bit <= p_bet_num1_data(to_integer(p_bet_num1_col));
        p_bet_num2_bit <= p_bet_num2_data(to_integer(p_bet_num2_col));
   
   
    --- Player budget
            p_budget_on <= "01" when ((p_y>=p_budget_num1_t) AND (p_y<=p_budget_num1_b) AND (p_x>=p_budget_num1_l) AND (p_x<=p_budget_num1_r)) else
                           "10" when ((p_y>=p_budget_num2_t) AND (p_y<=p_budget_num2_b) AND (p_x>=p_budget_num2_l) AND (p_x<=p_budget_num2_r)) else
                           "11" when ((p_y>=p_budget_num3_t) AND (p_y<=p_budget_num3_b) AND (p_x>=p_budget_num3_l) AND (p_x<=p_budget_num3_r)) else 
                           "00";
                p_budget_int <= to_integer(unsigned(p_budget));
                p_budget_num1_addr <= p_y(3 downto 0) - ps_budget_num1_t(3 downto 0);
                p_budget_num1_col <= ps_budget_num1_r(3 downto 0) - p_x(3 downto 0);
                p_budget_num1_data<=num0(to_integer(p_budget_num1_addr)) when (p_budget_int / 100 = 0) else
                                    num1(to_integer(p_budget_num1_addr)) when (p_budget_int / 100 = 1) else
                                    num2(to_integer(p_budget_num1_addr));
                
                p_budget_num2_addr <= p_y(3 downto 0) - ps_budget_num2_t(3 downto 0);
                p_budget_num2_col <= ps_budget_num2_r(3 downto 0) - p_x(3 downto 0);
                p_budget_num2_data<= num0(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 0) else
                                        num1(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 1) else
                                        num2(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 2) else
                                        num3(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 3) else
                                        num4(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 4) else
                                        num5(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 5) else
                                        num6(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 6) else
                                        num7(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 7) else
                                        num8(to_integer(p_budget_num2_addr)) when ((p_budget_int mod 100)/10 = 8) else
                                        num9(to_integer(p_budget_num2_addr));
                p_budget_num3_addr <= p_y(3 downto 0) - ps_budget_num3_t(3 downto 0);
                p_budget_num3_col <= ps_budget_num3_r(3 downto 0) - p_x(3 downto 0);
                p_budget_num3_data<= num0(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 0) else
                                        num1(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 1) else
                                        num2(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 2) else
                                        num3(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 3) else
                                        num4(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 4) else
                                        num5(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 5) else
                                        num6(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 6) else
                                        num7(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 7) else
                                        num8(to_integer(p_budget_num3_addr)) when ((p_budget_int mod 100) mod 10= 8) else
                                        num9(to_integer(p_budget_num3_addr));
                p_budget_num1_bit <= p_budget_num1_data(to_integer(p_budget_num1_col));
                p_budget_num2_bit <= p_budget_num2_data(to_integer(p_budget_num2_col));
                p_budget_num3_bit <= p_budget_num3_data(to_integer(p_budget_num3_col));
    ---first & second number of dealer card 1
                dcard1_num_on <= "01" when ((p_y>=d11_num_t) AND (p_y<=d11_num_b) AND (p_x>=d11_num_l) AND (p_x <= d11_num_r)) else
                                 "10" when ((p_y>=d21_num_t) AND (p_y<=d21_num_b) AND (p_x>=d21_num_l) AND (p_x <= d21_num_r)) else 
                                 "00";
                d11_cd_num_addr <= p_y(3 downto 0) - d11s_num_t(3 downto 0);
                d11_cd_num_col <= d11s_num_r(3 downto 0) - p_x(3 downto 0);
                d11_cd_num_data <= numA(to_integer(d11_cd_num_addr)) when ((d1_cd_num="0001") OR (d1_cd_num="1011")) else
                                                   num2(to_integer(d11_cd_num_addr)) when d1_cd_num="0010" else
                                                   num3(to_integer(d11_cd_num_addr)) when d1_cd_num="0011" else
                                                   num4(to_integer(d11_cd_num_addr)) when d1_cd_num="0100" else
                                                   num5(to_integer(d11_cd_num_addr)) when d1_cd_num="0101" else
                                                   num6(to_integer(d11_cd_num_addr)) when d1_cd_num="0110" else
                                                   num7(to_integer(d11_cd_num_addr)) when d1_cd_num="0111" else
                                                   numJ(to_integer(d11_cd_num_addr)) when d1_cd_num="1000" else
                                                   numQ(to_integer(d11_cd_num_addr)) when d1_cd_num="1001" else
                                                   numK(to_integer(d11_cd_num_addr));
                d21_cd_num_addr <= p_y(3 downto 0) - d21s_num_t(3 downto 0);
                d21_cd_num_col <= d21s_num_r(3 downto 0) - p_x(3 downto 0);
                d21_cd_num_data <= numA(to_integer(d21_cd_num_addr)) when ((d1_cd_num="0001") OR (d1_cd_num="1011")) else
                                                       num2(to_integer(d21_cd_num_addr)) when d1_cd_num="0010" else
                                                       num3(to_integer(d21_cd_num_addr)) when d1_cd_num="0011" else
                                                       num4(to_integer(d21_cd_num_addr)) when d1_cd_num="0100" else
                                                       num5(to_integer(d21_cd_num_addr)) when d1_cd_num="0101" else
                                                       num6(to_integer(d21_cd_num_addr)) when d1_cd_num="0110" else
                                                       num7(to_integer(d21_cd_num_addr)) when d1_cd_num="0111" else
                                                       numJ(to_integer(d21_cd_num_addr)) when d1_cd_num="1000" else
                                                       numQ(to_integer(d21_cd_num_addr)) when d1_cd_num="1001" else
                                                       numK(to_integer(d21_cd_num_addr));
                
                d11_cd_num_bit <= d11_cd_num_data(to_integer(d11_cd_num_col));
                d21_cd_num_bit <= d21_cd_num_data(to_integer(d21_cd_num_col));
                --- first & second number of player card 1
                pcard1_num_on <= "01" when ((p_y>=p11_num_t) AND (p_y<=p11_num_b) AND (p_x>=p11_num_l) AND (p_x <= p11_num_r)) else
                                 "10" when ((p_y>=p21_num_t) AND (p_y<=p21_num_b) AND (p_x>=p21_num_l) AND (p_x <= p21_num_r)) else
                                 "00";
                    p11_cd_num_addr <= p_y(3 downto 0) - p11s_num_t(3 downto 0);
                    p11_cd_num_col <= p11s_num_r(3 downto 0) - p_x(3 downto 0) ;
                    p11_cd_num_data <= numA(to_integer(p11_cd_num_addr)) when ((p1_cd_num="0001") OR (p1_cd_num="1011")) else
                                       num2(to_integer(p11_cd_num_addr)) when p1_cd_num="0010" else
                                       num3(to_integer(p11_cd_num_addr)) when p1_cd_num="0011" else
                                       num4(to_integer(p11_cd_num_addr)) when p1_cd_num="0100" else
                                       num5(to_integer(p11_cd_num_addr)) when p1_cd_num="0101" else
                                       num6(to_integer(p11_cd_num_addr)) when p1_cd_num="0110" else
                                       num7(to_integer(p11_cd_num_addr)) when p1_cd_num="0111" else
                                       numJ(to_integer(p11_cd_num_addr)) when p1_cd_num="1000" else
                                       numQ(to_integer(p11_cd_num_addr)) when p1_cd_num="1001" else
                                       numK(to_integer(p11_cd_num_addr));
                    p21_cd_num_addr <= p_y(3 downto 0) - p21s_num_t(3 downto 0);
                    p21_cd_num_col <= p21s_num_r(3 downto 0) - p_x(3 downto 0) ;
                    p21_cd_num_data <= numA(to_integer(p21_cd_num_addr)) when ((p1_cd_num="0001") OR (p1_cd_num="1011")) else
                                      num2(to_integer(p21_cd_num_addr)) when p1_cd_num="0010" else
                                      num3(to_integer(p21_cd_num_addr)) when p1_cd_num="0011" else
                                      num4(to_integer(p21_cd_num_addr)) when p1_cd_num="0100" else
                                      num5(to_integer(p21_cd_num_addr)) when p1_cd_num="0101" else
                                      num6(to_integer(p21_cd_num_addr)) when p1_cd_num="0110" else
                                      num7(to_integer(p21_cd_num_addr)) when p1_cd_num="0111" else
                                      numJ(to_integer(p21_cd_num_addr)) when p1_cd_num="1000" else
                                      numQ(to_integer(p21_cd_num_addr)) when p1_cd_num="1001" else
                                      numK(to_integer(p21_cd_num_addr));
                    
                    p11_cd_num_bit <= p11_cd_num_data(to_integer(p11_cd_num_col));
                    p21_cd_num_bit <= p21_cd_num_data(to_integer(p21_cd_num_col));
                ---first symbol of dealer card 1
                dcard1_sym_on <= "01" when ((p_y >= d11s_col_t) AND (p_y<=d11s_col_b) AND (p_x >= d11s_col_l) AND (p_x <= d11s_col_r)) else
                                 "10" when ((p_y >= d21s_col_t) AND (p_y<=d21s_col_b) AND (p_x >= d21s_col_l) AND (p_x <= d21s_col_r)) else
                                 "00";
                d11_cd_type_addr <= p_y(3 downto 0) - d11s_col_t(3 downto 0);
                d11_cd_type_col <= d11s_col_r(3 downto 0) - p_x(3 downto 0);
                d11_cd_type_data <= hearts(to_integer(d11_cd_type_addr)) when d1_cd_col="100" else
                                   diamond(to_integer(d11_cd_type_addr)) when d1_cd_col="101" else
                                   clubs(to_integer(d11_cd_type_addr)) when d1_cd_col="110" else
                                   spades(to_integer(d11_cd_type_addr)); 
                d11_cd_type_bit <= d11_cd_type_data(to_integer(d11_cd_type_col));
                --- second symbol of dealer card 1
                d21_cd_type_addr <= p_y(3 downto 0) - d21s_col_t(3 downto 0);
                d21_cd_type_col <= d21s_col_r(3 downto 0) - p_x(3 downto 0);
                d21_cd_type_data <= hearts(to_integer(d21_cd_type_addr)) when d1_cd_col="100" else
                                       diamond(to_integer(d21_cd_type_addr)) when d1_cd_col="101" else
                                       clubs(to_integer(d21_cd_type_addr)) when d1_cd_col="110" else
                                       spades(to_integer(d21_cd_type_addr));
                d21_cd_type_bit <= d21_cd_type_data(to_integer(d21_cd_type_col));                          
                --- first symbol of player card 1
                pcard1_sym_on <= "01" when ((p_y >= p11s_col_t) AND (p_y<=p11s_col_b) AND (p_x >= p11s_col_l) AND (p_x <= p11s_col_r)) else
                                 "10" when ((p_y >= p21s_col_t) AND (p_y<=p21s_col_b) AND (p_x >= p21s_col_l) AND (p_x <= p21s_col_r)) else
                                 "00";
                p11_cd_type_addr <= p_y(3 downto 0) - p11s_col_t(3 downto 0);
                p11_cd_type_col <= p11s_col_r(3 downto 0) - p_x(3 downto 0);
                p11_cd_type_data <= hearts(to_integer(p11_cd_type_addr)) when p1_cd_col="100" else
                                       diamond(to_integer(p11_cd_type_addr)) when p1_cd_col="101" else
                                       clubs(to_integer(p11_cd_type_addr)) when p1_cd_col="110" else
                                       spades(to_integer(p11_cd_type_addr));
                p11_cd_type_bit <= p11_cd_type_data(to_integer(p11_cd_type_col)); 
               ---- second symbol of player card 1
                   p21_cd_type_addr <= p_y(3 downto 0) - p21s_col_t(3 downto 0);
                   p21_cd_type_col <= p21s_col_r(3 downto 0) - p_x(3 downto 0);
                   p21_cd_type_data <= hearts(to_integer(p21_cd_type_addr)) when p1_cd_col="100" else
                                      diamond(to_integer(p21_cd_type_addr)) when p1_cd_col="101" else
                                      clubs(to_integer(p21_cd_type_addr)) when p1_cd_col="110" else
                                      spades(to_integer(p21_cd_type_addr)); 
                  p21_cd_type_bit <= p21_cd_type_data(to_integer(p21_cd_type_col));
            
                ---first & second number of dealer card 2
                dcard2_num_on <= "01" when ((p_y >= d12s_num_t) AND (p_y<=d12s_num_b) AND (p_x >= d12s_num_l) AND (p_x <= d12s_num_r)) else
                                 "10" when ((p_y >= d22s_num_t) AND (p_y<=d22s_num_b) AND (p_x >= d22s_num_l) AND (p_x <= d22s_num_r)) else
                                 "00";
                d12_cd_num_addr <= p_y(3 downto 0) - d12s_num_t(3 downto 0);
                d12_cd_num_col <= d12s_num_r(3 downto 0) - p_x(3 downto 0);
                d12_cd_num_data <= numA(to_integer(d12_cd_num_addr)) when ((d2_cd_num="0001") OR (d2_cd_num="1011")) else
                                                   num2(to_integer(d12_cd_num_addr)) when d2_cd_num="0010" else
                                                   num3(to_integer(d12_cd_num_addr)) when d2_cd_num="0011" else
                                                   num4(to_integer(d12_cd_num_addr)) when d2_cd_num="0100" else
                                                   num5(to_integer(d12_cd_num_addr)) when d2_cd_num="0101" else
                                                   num6(to_integer(d12_cd_num_addr)) when d2_cd_num="0110" else
                                                   num7(to_integer(d12_cd_num_addr)) when d2_cd_num="0111" else
                                                   numJ(to_integer(d12_cd_num_addr)) when d2_cd_num="1000" else
                                                   numQ(to_integer(d12_cd_num_addr)) when d2_cd_num="1001" else
                                                   numK(to_integer(d12_cd_num_addr));
                d22_cd_num_addr <= p_y(3 downto 0) - d22s_num_t(3 downto 0);
                d22_cd_num_col <= d22s_num_r(3 downto 0) - p_x(3 downto 0);
                d22_cd_num_data <= numA(to_integer(d22_cd_num_addr)) when ((d2_cd_num="0001") OR (d2_cd_num="1011")) else
                                                       num2(to_integer(d22_cd_num_addr)) when d2_cd_num="0010" else
                                                       num3(to_integer(d22_cd_num_addr)) when d2_cd_num="0011" else
                                                       num4(to_integer(d22_cd_num_addr)) when d2_cd_num="0100" else
                                                       num5(to_integer(d22_cd_num_addr)) when d2_cd_num="0101" else
                                                       num6(to_integer(d22_cd_num_addr)) when d2_cd_num="0110" else
                                                       num7(to_integer(d22_cd_num_addr)) when d2_cd_num="0111" else
                                                       numJ(to_integer(d22_cd_num_addr)) when d2_cd_num="1000" else
                                                       numQ(to_integer(d22_cd_num_addr)) when d2_cd_num="1001" else
                                                       numK(to_integer(d22_cd_num_addr));
                
                d12_cd_num_bit <= d12_cd_num_data(to_integer(d12_cd_num_col));
                d22_cd_num_bit <= d22_cd_num_data(to_integer(d22_cd_num_col));
                --- first & second number of player card 2
                pcard2_num_on <= "01" when ((p_y >= p12s_num_t) AND (p_y<=p12s_num_b) AND (p_x >= p12s_num_l) AND (p_x <= p12s_num_r)) else
                                 "10" when ((p_y >= p22s_num_t) AND (p_y<=p22s_num_b) AND (p_x >= p22s_num_l) AND (p_x <= p22s_num_r)) else
                                 "00";
                    p12_cd_num_addr <= p_y(3 downto 0) - p12s_num_t(3 downto 0);
                    p12_cd_num_col <= p12s_num_r(3 downto 0) - p_x(3 downto 0);
                    p12_cd_num_data <= numA(to_integer(p12_cd_num_addr)) when ((p2_cd_num="0001") OR (p2_cd_num="1011")) else
                                       num2(to_integer(p12_cd_num_addr)) when p2_cd_num="0010" else
                                       num3(to_integer(p12_cd_num_addr)) when p2_cd_num="0011" else
                                       num4(to_integer(p12_cd_num_addr)) when p2_cd_num="0100" else
                                       num5(to_integer(p12_cd_num_addr)) when p2_cd_num="0101" else
                                       num6(to_integer(p12_cd_num_addr)) when p2_cd_num="0110" else
                                       num7(to_integer(p12_cd_num_addr)) when p2_cd_num="0111" else
                                       numJ(to_integer(p12_cd_num_addr)) when p2_cd_num="1000" else
                                       numQ(to_integer(p12_cd_num_addr)) when p2_cd_num="1001" else
                                       numK(to_integer(p12_cd_num_addr));
                    p22_cd_num_addr <= p_y(3 downto 0) - p22s_num_t(3 downto 0);
                    p22_cd_num_col <= p22s_num_r(3 downto 0) - p_x(3 downto 0);
                    p22_cd_num_data <= numA(to_integer(p22_cd_num_addr)) when ((p2_cd_num="0001") OR (p2_cd_num="1011")) else
                                      num2(to_integer(p22_cd_num_addr)) when p2_cd_num="0010" else
                                      num3(to_integer(p22_cd_num_addr)) when p2_cd_num="0011" else
                                      num4(to_integer(p22_cd_num_addr)) when p2_cd_num="0100" else
                                      num5(to_integer(p22_cd_num_addr)) when p2_cd_num="0101" else
                                      num6(to_integer(p22_cd_num_addr)) when p2_cd_num="0110" else
                                      num7(to_integer(p22_cd_num_addr)) when p2_cd_num="0111" else
                                      numJ(to_integer(p22_cd_num_addr)) when p2_cd_num="1000" else
                                      numQ(to_integer(p22_cd_num_addr)) when p2_cd_num="1001" else
                                      numK(to_integer(p22_cd_num_addr));
                    
                    p12_cd_num_bit <= p12_cd_num_data(to_integer(p12_cd_num_col));
                    p22_cd_num_bit <= p22_cd_num_data(to_integer(p22_cd_num_col));
                ---first symbol of dealer card 2
                dcard2_sym_on <= "01" when ((p_y >= d12s_col_t) AND (p_y<=d12s_col_b) AND (p_x >= d12s_col_l) AND (p_x <= d12s_col_r)) else
                                 "10" when ((p_y >= d22s_col_t) AND (p_y<=d22s_col_b) AND (p_x >= d22s_col_l) AND (p_x <= d22s_col_r)) else
                                 "00";
                d12_cd_type_addr <= p_y(3 downto 0) - d12s_col_t(3 downto 0);
                d12_cd_type_col <= d12s_col_r(3 downto 0) - p_x(3 downto 0);
                d12_cd_type_data <= hearts(to_integer(d12_cd_type_addr)) when d2_cd_col="100" else
                                   diamond(to_integer(d12_cd_type_addr)) when d2_cd_col="101" else
                                   clubs(to_integer(d12_cd_type_addr)) when d2_cd_col="110" else
                                   spades(to_integer(d12_cd_type_addr)); 
                d12_cd_type_bit <= d12_cd_type_data(to_integer(d12_cd_type_col));
                --- second symbol of dealer card 2
                d22_cd_type_addr <= p_y(3 downto 0) - d22s_col_t(3 downto 0);
                d22_cd_type_col <= d22s_col_r(3 downto 0) - p_x(3 downto 0);
                d22_cd_type_data <= hearts(to_integer(d22_cd_type_addr)) when d2_cd_col="100" else
                                       diamond(to_integer(d22_cd_type_addr)) when d2_cd_col="101" else
                                       clubs(to_integer(d22_cd_type_addr)) when d2_cd_col="110" else
                                       spades(to_integer(d22_cd_type_addr));
                d22_cd_type_bit <= d22_cd_type_data(to_integer(d22_cd_type_col));                          
                --- first symbol of player card 2
                pcard2_sym_on <= "01" when ((p_y >= p12s_col_t) AND (p_y<=p12s_col_b) AND (p_x >= p12s_col_l) AND (p_x <= p12s_col_r)) else
                                 "10" when ((p_y >= p22s_col_t) AND (p_y<=p22s_col_b) AND (p_x >= p22s_col_l) AND (p_x <= p22s_col_r)) else
                                 "00";
                p12_cd_type_addr <= p_y(3 downto 0) - p12s_col_t(3 downto 0);
                p12_cd_type_col <= p12s_col_r(3 downto 0) - p_x(3 downto 0);
                p12_cd_type_data <= hearts(to_integer(p12_cd_type_addr)) when p2_cd_col="100" else
                                       diamond(to_integer(p12_cd_type_addr)) when p2_cd_col="101" else
                                       clubs(to_integer(p12_cd_type_addr)) when p2_cd_col="110" else
                                       spades(to_integer(p12_cd_type_addr));
                p12_cd_type_bit <= p12_cd_type_data(to_integer(p12_cd_type_col)); 
               ---- second symbol of player card 2 
                   p22_cd_type_addr <= p_y(3 downto 0) - p22s_col_t(3 downto 0);
                   p22_cd_type_col <=p22s_col_r(3 downto 0) - p_x(3 downto 0);
                   p22_cd_type_data <= hearts(to_integer(p22_cd_type_addr)) when p2_cd_col="100" else
                                      diamond(to_integer(p22_cd_type_addr)) when p2_cd_col="101" else
                                      clubs(to_integer(p22_cd_type_addr)) when p2_cd_col="110" else
                                      spades(to_integer(p22_cd_type_addr)); 
                  p22_cd_type_bit <= p22_cd_type_data(to_integer(p22_cd_type_col));
              
                ---first & second number of dealer card 3
                  dcard3_num_on <= "01" when ((p_y >= d13s_num_t) AND (p_y<=d13s_num_b) AND (p_x >= d13s_num_l) AND (p_x <= d13s_num_r)) else
                                   "10" when ((p_y >= d23s_num_t) AND (p_y<=d23s_num_b) AND (p_x >= d23s_num_l) AND (p_x <= d23s_num_r)) else
                                   "00";
                  d13_cd_num_addr <= p_y(3 downto 0) - d13s_num_t(3 downto 0);
                  d13_cd_num_col <= d13s_num_r(3 downto 0) -  p_x(3 downto 0);
                  d13_cd_num_data <= numA(to_integer(d13_cd_num_addr)) when ((d3_cd_num="0001") OR (d3_cd_num="1011")) else
                                                     num2(to_integer(d13_cd_num_addr)) when d3_cd_num="0010" else
                                                     num3(to_integer(d13_cd_num_addr)) when d3_cd_num="0011" else
                                                     num4(to_integer(d13_cd_num_addr)) when d3_cd_num="0100" else
                                                     num5(to_integer(d13_cd_num_addr)) when d3_cd_num="0101" else
                                                     num6(to_integer(d13_cd_num_addr)) when d3_cd_num="0110" else
                                                     num7(to_integer(d13_cd_num_addr)) when d3_cd_num="0111" else
                                                     numJ(to_integer(d13_cd_num_addr)) when d3_cd_num="1000" else
                                                     numQ(to_integer(d13_cd_num_addr)) when d3_cd_num="1001" else
                                                     numK(to_integer(d13_cd_num_addr));
                  d23_cd_num_addr <= p_y(3 downto 0) - d23s_num_t(3 downto 0);
                  d23_cd_num_col <= d23s_num_r(3 downto 0) -  p_x(3 downto 0);
                  d23_cd_num_data <= numA(to_integer(d23_cd_num_addr)) when ((d3_cd_num="0001") OR (d3_cd_num="1011")) else
                                                         num2(to_integer(d23_cd_num_addr)) when d3_cd_num="0010" else
                                                         num3(to_integer(d23_cd_num_addr)) when d3_cd_num="0011" else
                                                         num4(to_integer(d23_cd_num_addr)) when d3_cd_num="0100" else
                                                         num5(to_integer(d23_cd_num_addr)) when d3_cd_num="0101" else
                                                         num6(to_integer(d23_cd_num_addr)) when d3_cd_num="0110" else
                                                         num7(to_integer(d23_cd_num_addr)) when d3_cd_num="0111" else
                                                         numJ(to_integer(d23_cd_num_addr)) when d3_cd_num="1000" else
                                                         numQ(to_integer(d23_cd_num_addr)) when d3_cd_num="1001" else
                                                         numK(to_integer(d23_cd_num_addr));
                  
                  d13_cd_num_bit <= d13_cd_num_data(to_integer(d13_cd_num_col));
                  d23_cd_num_bit <= d23_cd_num_data(to_integer(d23_cd_num_col));
                  --- first & second number of player card 3
                  pcard3_num_on <= "01" when ((p_y >= p13s_num_t) AND (p_y<=p13s_num_b) AND (p_x >= p13s_num_l) AND (p_x <= p13s_num_r)) else
                                   "10" when ((p_y >= p23s_num_t) AND (p_y<=p23s_num_b) AND (p_x >= p23s_num_l) AND (p_x <= p23s_num_r)) else
                                   "00";
                      p13_cd_num_addr <= p_y(3 downto 0) - p13s_num_t(3 downto 0);
                      p13_cd_num_col <= p13s_num_r(3 downto 0) -  p_x(3 downto 0);
                      p13_cd_num_data <= numA(to_integer(p13_cd_num_addr)) when ((p3_cd_num="0001") OR (p3_cd_num="1011")) else
                                         num2(to_integer(p13_cd_num_addr)) when p3_cd_num="0010" else
                                         num3(to_integer(p13_cd_num_addr)) when p3_cd_num="0011" else
                                         num4(to_integer(p13_cd_num_addr)) when p3_cd_num="0100" else
                                         num5(to_integer(p13_cd_num_addr)) when p3_cd_num="0101" else
                                         num6(to_integer(p13_cd_num_addr)) when p3_cd_num="0110" else
                                         num7(to_integer(p13_cd_num_addr)) when p3_cd_num="0111" else
                                         numJ(to_integer(p13_cd_num_addr)) when p3_cd_num="1000" else
                                         numQ(to_integer(p13_cd_num_addr)) when p3_cd_num="1001" else
                                         numK(to_integer(p13_cd_num_addr));
                      p23_cd_num_addr <= p_y(3 downto 0) - p23s_num_t(3 downto 0);
                      p23_cd_num_col <= p23s_num_r(3 downto 0) -  p_x(3 downto 0);
                      p23_cd_num_data <= numA(to_integer(p23_cd_num_addr)) when ((p3_cd_num="0001") OR (p3_cd_num="1011")) else
                                        num2(to_integer(p23_cd_num_addr)) when p3_cd_num="0010" else
                                        num3(to_integer(p23_cd_num_addr)) when p3_cd_num="0011" else
                                        num4(to_integer(p23_cd_num_addr)) when p3_cd_num="0100" else
                                        num5(to_integer(p23_cd_num_addr)) when p3_cd_num="0101" else
                                        num6(to_integer(p23_cd_num_addr)) when p3_cd_num="0110" else
                                        num7(to_integer(p23_cd_num_addr)) when p3_cd_num="0111" else
                                        numJ(to_integer(p23_cd_num_addr)) when p3_cd_num="1000" else
                                        numQ(to_integer(p23_cd_num_addr)) when p3_cd_num="1001" else
                                        numK(to_integer(p23_cd_num_addr));
                      
                      p13_cd_num_bit <= p13_cd_num_data(to_integer(p13_cd_num_col));
                      p23_cd_num_bit <= p23_cd_num_data(to_integer(p23_cd_num_col));
                  ---first symbol of dealer card 3
                  dcard3_sym_on <= "01" when ((p_y >= d13s_col_t) AND (p_y<=d13s_col_b) AND (p_x >= d13s_col_l) AND (p_x<= d13s_col_r)) else
                                   "10" when ((p_y >= d23s_col_t) AND (p_y<=d23s_col_b) AND (p_x >= d23s_col_l) AND (p_x<= d23s_col_r)) else
                                   "00";
                  d13_cd_type_addr <= p_y(3 downto 0) - d13s_col_t(3 downto 0);
                  d13_cd_type_col <= d13s_col_r(3 downto 0) -  p_x(3 downto 0);
                  d13_cd_type_data <= hearts(to_integer(d13_cd_type_addr)) when d3_cd_col="100" else
                                     diamond(to_integer(d13_cd_type_addr)) when d3_cd_col="101" else
                                     clubs(to_integer(d13_cd_type_addr)) when d3_cd_col="110" else
                                     spades(to_integer(d13_cd_type_addr)); 
                  d13_cd_type_bit <= d13_cd_type_data(to_integer(d13_cd_type_col));
                  --- second symbol of dealer card 3 
                  d23_cd_type_addr <= p_y(3 downto 0) - d23s_col_t(3 downto 0);
                  d23_cd_type_col <= d23s_col_r(3 downto 0) -  p_x(3 downto 0);
                  d23_cd_type_data <= hearts(to_integer(d23_cd_type_addr)) when d3_cd_col="100" else
                                         diamond(to_integer(d23_cd_type_addr)) when d3_cd_col="101" else
                                         clubs(to_integer(d23_cd_type_addr)) when d3_cd_col="110" else
                                         spades(to_integer(d23_cd_type_addr));
                  d23_cd_type_bit <= d23_cd_type_data(to_integer(d23_cd_type_col));                          
                  --- first symbol of player card 3
                  pcard3_sym_on <= "01" when ((p_y >= p13s_col_t) AND (p_y<=p13s_col_b) AND (p_x >= p13s_col_l) AND (p_x <= p13s_col_r)) else
                                   "10" when ((p_y >= p23s_col_t) AND (p_y<=p23s_col_b) AND (p_x >= p23s_col_l) AND (p_x <= p23s_col_r)) else
                                   "00";
                  p13_cd_type_addr <= p_y(3 downto 0) - p13s_col_t(3 downto 0);
                  p13_cd_type_col <= p13s_col_r(3 downto 0) -  p_x(3 downto 0);
                  p13_cd_type_data <= hearts(to_integer(p13_cd_type_addr)) when p3_cd_col="100" else
                                         diamond(to_integer(p13_cd_type_addr)) when p3_cd_col="101" else
                                         clubs(to_integer(p13_cd_type_addr)) when p3_cd_col="110" else
                                         spades(to_integer(p13_cd_type_addr));
                  p13_cd_type_bit <= p13_cd_type_data(to_integer(p13_cd_type_col)); 
                 ---- second symbol of player card 3
                     p23_cd_type_addr <= p_y(3 downto 0) - p23s_col_t(3 downto 0);
                     p23_cd_type_col <= p23s_col_r(3 downto 0) -  p_x(3 downto 0);
                     p23_cd_type_data <= hearts(to_integer(p23_cd_type_addr)) when p3_cd_col="100" else
                                        diamond(to_integer(p23_cd_type_addr)) when p3_cd_col="101" else
                                        clubs(to_integer(p23_cd_type_addr)) when p3_cd_col="110" else
                                        spades(to_integer(p23_cd_type_addr)); 
                    p23_cd_type_bit <= p23_cd_type_data(to_integer(p23_cd_type_col));
            
                ---first & second number of dealer card 4
                dcard4_num_on <= "01" when ((p_y >= d14s_num_t) AND (p_y<=d14s_num_b) AND (p_x >= d14s_num_l) AND (p_x <= d14s_num_r)) else
                                 "10" when ((p_y >= d24s_num_t) AND (p_y<=d24s_num_b) AND (p_x >= d24s_num_l) AND (p_x <= d24s_num_r)) else
                                 "00";
                d14_cd_num_addr <= p_y(3 downto 0) - d14s_num_t(3 downto 0);
                d14_cd_num_col <= d14s_num_r(3 downto 0) - p_x(3 downto 0);
                d14_cd_num_data <= numA(to_integer(d14_cd_num_addr)) when ((d4_cd_num="0001") OR (d4_cd_num="1011")) else
                                                   num2(to_integer(d14_cd_num_addr)) when d4_cd_num="0010" else
                                                   num3(to_integer(d14_cd_num_addr)) when d4_cd_num="0011" else
                                                   num4(to_integer(d14_cd_num_addr)) when d4_cd_num="0100" else
                                                   num5(to_integer(d14_cd_num_addr)) when d4_cd_num="0101" else
                                                   num6(to_integer(d14_cd_num_addr)) when d4_cd_num="0110" else
                                                   num7(to_integer(d14_cd_num_addr)) when d4_cd_num="0111" else
                                                   numJ(to_integer(d14_cd_num_addr)) when d4_cd_num="1000" else
                                                   numQ(to_integer(d14_cd_num_addr)) when d4_cd_num="1001" else
                                                   numK(to_integer(d14_cd_num_addr));
                                                   
                d24_cd_num_addr <= p_y(3 downto 0) - d24s_num_t(3 downto 0);
                d24_cd_num_col <= d24s_num_r(3 downto 0) - p_x(3 downto 0);
                d24_cd_num_data <= numA(to_integer(d24_cd_num_addr)) when ((d4_cd_num="0001") OR (d4_cd_num="1011")) else
                                                       num2(to_integer(d24_cd_num_addr)) when d4_cd_num="0010" else
                                                       num3(to_integer(d24_cd_num_addr)) when d4_cd_num="0011" else
                                                       num4(to_integer(d24_cd_num_addr)) when d4_cd_num="0100" else
                                                       num5(to_integer(d24_cd_num_addr)) when d4_cd_num="0101" else
                                                       num6(to_integer(d24_cd_num_addr)) when d4_cd_num="0110" else
                                                       num7(to_integer(d24_cd_num_addr)) when d4_cd_num="0111" else
                                                       numJ(to_integer(d24_cd_num_addr)) when d4_cd_num="1000" else
                                                       numQ(to_integer(d24_cd_num_addr)) when d4_cd_num="1001" else
                                                       numK(to_integer(d24_cd_num_addr));
                
                d14_cd_num_bit <= d14_cd_num_data(to_integer(d14_cd_num_col));
                d24_cd_num_bit <= d24_cd_num_data(to_integer(d24_cd_num_col));
                --- first & second number of player card 4
                pcard4_num_on <= "01" when ((p_y >= p14s_num_t) AND (p_y<=p14s_num_b) AND (p_x >= p14s_num_l) AND (p_x <= p14s_num_r)) else
                                 "10" when ((p_y >= p24s_num_t) AND (p_y<=p24s_num_b) AND (p_x >= p24s_num_l) AND (p_x <= p24s_num_r)) else
                                 "00";
                    p14_cd_num_addr <= p_y(3 downto 0) - p14s_num_t(3 downto 0);
                    p14_cd_num_col <= p14s_num_r(3 downto 0) - p_x(3 downto 0);
                    p14_cd_num_data <= numA(to_integer(p14_cd_num_addr)) when ((p4_cd_num="0001") OR (p4_cd_num="1011")) else
                                       num2(to_integer(p14_cd_num_addr)) when p4_cd_num="0010" else
                                       num3(to_integer(p14_cd_num_addr)) when p4_cd_num="0011" else
                                       num4(to_integer(p14_cd_num_addr)) when p4_cd_num="0100" else
                                       num5(to_integer(p14_cd_num_addr)) when p4_cd_num="0101" else
                                       num6(to_integer(p14_cd_num_addr)) when p4_cd_num="0110" else
                                       num7(to_integer(p14_cd_num_addr)) when p4_cd_num="0111" else
                                       numJ(to_integer(p14_cd_num_addr)) when p4_cd_num="1000" else
                                       numQ(to_integer(p14_cd_num_addr)) when p4_cd_num="1001" else
                                       numK(to_integer(p14_cd_num_addr));
                    p24_cd_num_addr <= p_y(3 downto 0) - p24s_num_t(3 downto 0);
                    p24_cd_num_col <= p24s_num_r(3 downto 0) - p_x(3 downto 0);
                    p24_cd_num_data <= numA(to_integer(p24_cd_num_addr)) when ((p4_cd_num="0001") OR (p4_cd_num="1011")) else
                                      num2(to_integer(p24_cd_num_addr)) when p4_cd_num="0010" else
                                      num3(to_integer(p24_cd_num_addr)) when p4_cd_num="0011" else
                                      num4(to_integer(p24_cd_num_addr)) when p4_cd_num="0100" else
                                      num5(to_integer(p24_cd_num_addr)) when p4_cd_num="0101" else
                                      num6(to_integer(p24_cd_num_addr)) when p4_cd_num="0110" else
                                      num7(to_integer(p24_cd_num_addr)) when p4_cd_num="0111" else
                                      numJ(to_integer(p24_cd_num_addr)) when p4_cd_num="1000" else
                                      numQ(to_integer(p24_cd_num_addr)) when p4_cd_num="1001" else
                                      numK(to_integer(p24_cd_num_addr));
                    
                    p14_cd_num_bit <= p14_cd_num_data(to_integer(p14_cd_num_col));
                    p24_cd_num_bit <= p24_cd_num_data(to_integer(p24_cd_num_col));
                ---first symbol of dealer card 4
                dcard4_sym_on <= "01" when ((p_y >= d14s_col_t) AND (p_y<=d14s_col_b) AND (p_x >= d14s_col_l) AND (p_x <= d14s_col_r)) else
                                 "10" when ((p_y >= d24s_col_t) AND (p_y<=d24s_col_b) AND (p_x >= d24s_col_l) AND (p_x <= d24s_col_r)) else
                                 "00";
                d14_cd_type_addr <= p_y(3 downto 0) - d14s_col_t(3 downto 0);
                d14_cd_type_col <= d14s_col_r(3 downto 0) - p_x(3 downto 0);
                d14_cd_type_data <= hearts(to_integer(d14_cd_type_addr)) when d4_cd_col="100" else
                                   diamond(to_integer(d14_cd_type_addr)) when d4_cd_col="101" else
                                   clubs(to_integer(d14_cd_type_addr)) when d4_cd_col="110" else
                                   spades(to_integer(d14_cd_type_addr)); 
                d14_cd_type_bit <= d14_cd_type_data(to_integer(d14_cd_type_col));
                
                --- second symbol of dealer card 4
                d24_cd_type_addr <= p_y(3 downto 0) - d24s_col_t(3 downto 0);
                d24_cd_type_col <= d24s_col_r(3 downto 0) - p_x(3 downto 0);
                d24_cd_type_data <= hearts(to_integer(d24_cd_type_addr)) when d4_cd_col="100" else
                                       diamond(to_integer(d24_cd_type_addr)) when d4_cd_col="101" else
                                       clubs(to_integer(d24_cd_type_addr)) when d4_cd_col="110" else
                                       spades(to_integer(d24_cd_type_addr));
                d24_cd_type_bit <= d24_cd_type_data(to_integer(d24_cd_type_col));                          
                --- first symbol of player card 4
                pcard4_sym_on <= "01" when ((p_y >= p14s_col_t) AND (p_y<=p14s_col_b) AND (p_x >= p14s_col_l) AND (p_x <= p14s_col_r)) else
                                 "10" when ((p_y >= p24s_col_t) AND (p_y<=p24s_col_b) AND (p_x >= p24s_col_l) AND (p_x <= p24s_col_r)) else
                                 "00";
                p14_cd_type_addr <= p_y(3 downto 0) - p14s_col_t(3 downto 0);
                p14_cd_type_col <= p14s_col_r(3 downto 0) - p_x(3 downto 0);
                p14_cd_type_data <= hearts(to_integer(p14_cd_type_addr)) when p4_cd_col="100" else
                                       diamond(to_integer(p14_cd_type_addr)) when p4_cd_col="101" else
                                       clubs(to_integer(p14_cd_type_addr)) when p4_cd_col="110" else
                                       spades(to_integer(p14_cd_type_addr));
                p14_cd_type_bit <= p14_cd_type_data(to_integer(p14_cd_type_col)); 
               ---- second symbol of player card 4
                   p24_cd_type_addr <= p_y(3 downto 0) - p24s_col_t(3 downto 0);
                   p24_cd_type_col <= p24s_col_r(3 downto 0) - p_x(3 downto 0);
                   p24_cd_type_data <= hearts(to_integer(p24_cd_type_addr)) when p4_cd_col="100" else
                                      diamond(to_integer(p24_cd_type_addr)) when p4_cd_col="101" else
                                      clubs(to_integer(p24_cd_type_addr)) when p4_cd_col="110" else
                                      spades(to_integer(p24_cd_type_addr)); 
                  p24_cd_type_bit <= p24_cd_type_data(to_integer(p24_cd_type_col));
   ---bit mode on setting for player and dealer
---- bits 1
      dcard1_sym_bit_on <='1' when (((dcard1_sym_on="01") AND (d11_cd_type_bit='1')) OR ((dcard1_sym_on="10") AND (d21_cd_type_bit='1'))) else
                          '0';
      pcard1_sym_bit_on <='1' when (((pcard1_sym_on="01") AND (p11_cd_type_bit='1')) OR ((pcard1_sym_on="10") AND (p21_cd_type_bit='1'))) else
                          '0';
      dcard1_num_bit_on <='1' when (((dcard1_num_on="01") AND (d11_cd_num_bit='1')) OR ((dcard1_num_on="10") AND (d21_cd_num_bit='1'))) else
                          '0';
      pcard1_num_bit_on <='1' when (((pcard1_num_on="01") AND (p11_cd_num_bit='1')) OR ((pcard1_num_on="10") AND (p21_cd_num_bit='1'))) else
                          '0';
      pcard1_rgb <= "0000000000011111" when (p1_cd_col="100" OR p1_cd_col="101") else
                   "0000000000000000" when (p1_cd_col="110" OR p1_cd_col="111") else
                   "1111111111111111";
      dcard1_rgb <= "0000000000011111" when (d1_cd_col="100" OR d1_cd_col="101") else
                   "0000000000000000" when (d1_cd_col="110" OR d1_cd_col="111") else
                   "1111111111111111";
                   
    ---- bits 2               
    dcard2_sym_bit_on <='1' when (((dcard2_sym_on="01") AND (d12_cd_type_bit='1')) OR ((dcard2_sym_on="10") AND (d22_cd_type_bit='1'))) else
                        '0';
    pcard2_sym_bit_on <='1' when (((pcard2_sym_on="01") AND (p12_cd_type_bit='1')) OR ((pcard2_sym_on="10") AND (p22_cd_type_bit='1'))) else
                        '0';
    dcard2_num_bit_on <='1' when (((dcard2_num_on="01") AND (d12_cd_num_bit='1')) OR ((dcard2_num_on="10") AND (d22_cd_num_bit='1'))) else
                        '0';
    pcard2_num_bit_on <='1' when (((pcard2_num_on="01") AND (p12_cd_num_bit='1')) OR ((pcard2_num_on="10") AND (p22_cd_num_bit='1'))) else
                        '0';
      pcard2_rgb <= "0000000000011111" when (p2_cd_col="100" OR p2_cd_col="101") else
                    "0000000000000000" when (p2_cd_col="110" OR p2_cd_col="111") else
                    "1111111111111111";
      dcard2_rgb <= "0000000000011111" when (d2_cd_col="100" OR d2_cd_col="101") else
                    "0000000000000000" when (d2_cd_col="110" OR d2_cd_col="111") else
                    "1111111111111111";
                    
   ---- bits 3
   dcard3_sym_bit_on <='1' when (((dcard3_sym_on="01") AND (d13_cd_type_bit='1')) OR ((dcard3_sym_on="10") AND (d23_cd_type_bit='1'))) else
                       '0';
   pcard3_sym_bit_on <='1' when (((pcard3_sym_on="01") AND (p13_cd_type_bit='1')) OR ((pcard3_sym_on="10") AND (p23_cd_type_bit='1'))) else
                       '0';
   dcard3_num_bit_on <='1' when (((dcard3_num_on="01") AND (d13_cd_num_bit='1')) OR ((dcard3_num_on="10") AND (d23_cd_num_bit='1'))) else
                       '0';
   pcard3_num_bit_on <='1' when (((pcard3_num_on="01") AND (p13_cd_num_bit='1')) OR ((pcard3_num_on="10") AND (p23_cd_num_bit='1'))) else
                       '0';
      pcard3_rgb <= "0000000000011111" when (p3_cd_col="100" OR p3_cd_col="101") else
                    "0000000000000000" when (p3_cd_col="110" OR p3_cd_col="111") else
                    "1111111111111111";
      dcard3_rgb <= "0000000000011111" when (d3_cd_col="100" OR d3_cd_col="101") else
                    "0000000000000000" when (d3_cd_col="110" OR d3_cd_col="111") else
                    "1111111111111111";
   --- bits 4
   dcard4_sym_bit_on <='1' when (((dcard4_sym_on="01") AND (d14_cd_type_bit='1')) OR ((dcard4_sym_on="10") AND (d24_cd_type_bit='1'))) else
                       '0';
   pcard4_sym_bit_on <='1' when (((pcard4_sym_on="01") AND (p14_cd_type_bit='1')) OR ((pcard4_sym_on="10") AND (p24_cd_type_bit='1'))) else
                       '0';
   dcard4_num_bit_on <='1' when (((dcard4_num_on="01") AND (d14_cd_num_bit='1')) OR ((dcard4_num_on="10") AND (d24_cd_num_bit='1'))) else
                       '0';
   pcard4_num_bit_on <='1' when (((pcard4_num_on="01") AND (p14_cd_num_bit='1')) OR ((pcard4_num_on="10") AND (p24_cd_num_bit='1'))) else
                       '0';
      pcard4_rgb <= "0000000000011111" when (p4_cd_col="100" OR p4_cd_col="101") else
                    "0000000000000000" when (p4_cd_col="110" OR p4_cd_col="111") else
                    "1111111111111111";
      dcard4_rgb <= "0000000000011111" when (d4_cd_col="100" OR d4_cd_col="101") else
                    "0000000000000000" when (d4_cd_col="110" OR d4_cd_col="111") else
                    "1111111111111111";
                    
 --creating yello
 
 
 
          
    process(video_on,bar_on,dcard1_sym_bit_on,pcard1_sym_bit_on,pcard1_bd_on,dcard1_bd_on,dcard2_sym_bit_on,pcard2_sym_bit_on,pcard2_bd_on,dcard2_bd_on,dcard3_sym_bit_on,pcard3_sym_bit_on,pcard3_bd_on,dcard3_bd_on,dcard4_sym_bit_on,pcard3_sym_bit_on,pcard4_bd_on,dcard4_bd_on)
    begin
        if(video_on='0') then
            rgb_out <= "0000000000000000";
        else
            if(b1_on = '1') then
                rgb_out<=b1_rgb;
            elsif(b2_on = '1') then 
                rgb_out<=b2_rgb;
            elsif(b3_on = '1') then
                rgb_out <= b3_rgb;
            elsif(b4_on ='1') then
                rgb_out <= b3_rgb;
            elsif (dcard1_sym_bit_on='1' OR dcard1_num_bit_on='1' OR dcard1_bd_on='1') then
                                    rgb_out<=dcard1_rgb;
           elsif (pcard1_num_bit_on='1' OR pcard1_sym_bit_on='1' OR pcard1_bd_on='1') then
                                    rgb_out<=pcard1_rgb;
           elsif (dcard2_sym_bit_on='1' OR dcard2_num_bit_on='1' OR dcard2_bd_on='1') then
                                    rgb_out<=dcard2_rgb;
           elsif (pcard2_num_bit_on='1' OR pcard2_sym_bit_on='1' OR pcard2_bd_on='1') then
                                    rgb_out<=pcard2_rgb;
           elsif (dcard3_sym_bit_on='1' OR dcard3_num_bit_on='1' OR dcard3_bd_on='1') then
                                    rgb_out<=dcard3_rgb;
           elsif (pcard3_num_bit_on='1' OR pcard3_sym_bit_on='1' OR pcard3_bd_on='1') then
                                    rgb_out<=pcard3_rgb;
           elsif (dcard4_sym_bit_on='1' OR dcard4_num_bit_on='1' OR dcard4_bd_on='1') then
                                    rgb_out<=dcard4_rgb;
           elsif (pcard4_num_bit_on='1' OR pcard4_sym_bit_on='1' OR pcard4_bd_on='1') then
                                    rgb_out<=pcard4_rgb;
           elsif ((p_score1_on = '1' AND p_score_num1_bit = '1') OR (( p_score2_on= '1') AND (p_score_num2_bit = '1'))) then
                   rgb_out <= score_rgb;
           elsif ((d_score1_on = '1' AND d_score_num1_bit = '1') OR ((d_score2_on = '1') AND (d_score_num2_bit = '1'))) then
                rgb_out <= score_rgb;
           elsif (((p_bet_on="01") AND (p_bet_num1_bit='1')) OR ((p_bet_on="10") AND (p_bet_num2_bit='1'))) then
                rgb_out <= bet_rgb;
           elsif (((p_budget_on = "01") AND (p_budget_num1_bit = '1')) OR ((p_budget_on = "10") AND (p_budget_num2_bit = '1'))  OR ((p_budget_on = "11") AND  (p_budget_num3_bit = '1'))) then
                rgb_out <= budget_rgb;
           elsif ((sym1_on='1') OR (sym2_on='1') OR (sym3_on='1')) then
                   rgb_out<="1001000011000110";        
           else
          rgb_out<="1111111111111111";
            end if;
        end if;
     end process;
end arch;