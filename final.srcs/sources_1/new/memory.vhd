----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:25:33 05/19/2016 
-- Design Name: 
-- Module Name:    mem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity memory is
port (reset,CLK,getA,getB: in std_logic;
      mem_clr: in std_logic;
		card_outA,card_outB: out std_logic_vector(3 downto 0);
		card_colA,card_colB:out std_logic_vector(2 downto 0);
		card_rdyA,card_rdyB: out std_logic:='0');
end memory;

architecture crd_mem of memory is
type validity_store is array (39 downto 0) of std_logic;
signal val_storage: validity_store:= (others => '0');
type card_store is array(39 downto 0) of std_logic_vector(5 downto 0);
constant cd_storage : card_store :=("000001","000010","000011","000100","000101","000110","000111","001000","001001","001010",
									"010001","010010","010011","010100","010101","010110","010111","011000","011001","011010",
								    "100001","100010","100011","100100","100101","100110","100111","101000","101001","101010",
									"110001","110010","110011","110100","110101","110110","110111","111000","111001","111010");

constant N: integer := 5;
signal counter: unsigned(N-1 downto 0):= ( others => '0' );
shared variable add_flag: std_logic;

signal temp_card: integer:=0;
signal temp_signal_card: std_logic_vector(5 downto 0):= (others=>'0');
signal rand_trigger: std_logic:='0';
signal readyA,readyB: std_logic:= '0';
signal card_numA,card_numB: std_logic_vector(3 downto 0) := "0000";
signal colA,colB: std_logic_vector(2 downto 0) := "000";
begin
    card_outA <= card_numA;
    card_outB <= card_numB;
    card_colA <= colA;
    card_colB <= colB;    
	
	process(CLK,reset,getA,getB,temp_card)
	begin
	add_flag := not (counter(N-1));
	if(reset = '1' OR mem_clr = '1') then
	val_storage <= "0000000000000000000000000000000000000000";
	counter <= (others => '0');
	elsif (rising_edge(CLK)) then
	   if(add_flag = '1') then
	   counter <= counter + 1;
	   else
	   if(getA= '1' OR getB='1') then
	       rand_trigger <= '1';
	   end if;
	   if(rand_trigger = '1') then
	       if((temp_card < 39) AND (val_storage(temp_card) = '0')) then
	       rand_trigger <= '0';
	       if(getA = '1') then
	           card_numA <= cd_storage(temp_card)(3 downto 0);
	           colA <= '1' & cd_storage(temp_card)(5 downto 4);
	           card_numB <= "0000";
	           colB <= "000";
	           val_storage(temp_card) <= '1';
	           readyA <= '1';
	           counter <= (others => '0');
	       elsif(getB = '1') then
	           card_numB <= cd_storage(temp_card)(3 downto 0);
               colB <= '1' & cd_storage(temp_card)(5 downto 4);
               card_numA <= "0000";
               colA <= "000";
               val_storage(temp_card) <= '1';
               readyB <= '1';
               counter <= (others => '0');
           end if;
           end if;
        elsif(readyA = '1') then
         readyA <= '0';
         
        elsif(readyB = '1') then
          readyB <= '0';
          
        end if;
        end if;
	end if;
	end process;
	
	
	card_rdyB <= readyB;
	card_rdyA <= readyA;
	
	
	process(clk,temp_card)
	begin
	if(rising_edge(clk)) then
	   temp_card <= temp_card + 1;
	   if(temp_card > 38) then
	       temp_card <= 0;
	   end if;
	 end if;
	 end process;
	
end crd_mem;
