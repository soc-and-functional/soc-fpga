-- DBounce.vhd


--//////////////////////// Button Debounceer ///////////////////////////--
-- ***********************************************************************
-- FileName: DBounce.vhd
-- FPGA: Microsemi IGLOO AGLN250V2
-- IDE: Libero v9.1.4.0 SP4 
--
-- HDL IS PROVIDED "AS IS." DIGI-KEY EXPRESSLY DISCLAIMS ANY
-- WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT NOT
-- LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
-- PARTICULAR PURPOSE, OR NON-INFRINGEMENT. IN NO EVENT SHALL DIGI-KEY
-- BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL
-- DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR EQUIPMENT, COST OF
-- PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY OR SERVICES, ANY CLAIMS
-- BY THIRD PARTIES (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF),
-- ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION, OR OTHER SIMILAR COSTS.
-- DIGI-KEY ALSO DISCLAIMS ANY LIABILITY FOR PATENT OR COPYRIGHT
-- INFRINGEMENT.
--
-- Version History
-- Version 1.0 12/06/2011 Tony Storey
-- Initial Public Release
-- Small Footprint Button Debouncer


library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity HWreset is
    Port(
        clk : in std_logic;
        button_in   : in std_logic;
		  b_upper: out std_logic;
		  b_lower: out std_logic
		  --leds: out std_logic_vector(3 downto 0)
        );
end HWreset;

architecture arch of HWreset is

	 constant a    				: integer := 10000000;
    constant N              : integer := 15;   -- 2^20 * 1/(33MHz) = 32ms
    signal q_reg    : unsigned(N-1 downto 0);
	 signal q_next		: unsigned(N-1 downto 0);
    signal DFF1, DFF2       : std_logic;
    signal q_reset, q_add   : std_logic;
	 signal DB_out					: std_logic;
	 signal count :integer:=0;
	signal flag: std_logic;
	signal buffer_out1: std_logic_vector(1 downto 0) := "00";
	signal buffer_out2: std_logic_vector(1 downto 0);
	signal b_out: std_logic_vector(1 downto 0);


begin
	
	
	b_upper <= b_out(1);
	b_lower <= b_out(0);
	
    -- COUNTER FOR TIMING 
    q_next <= (others => '0') when q_reset = '1' else  -- resets the counter 
                    q_reg + 1 when q_add = '1' else    -- increment count if commanded
                    q_reg;  
						  
		b_out <= buffer_out2;
	buffer_out2 <= buffer_out1;
    --leds <= ("00" & b_out);
    
    -- SYNCHRO REG UPDATE
    process(clk)
    begin
        if(rising_edge(clk)) then
            
                q_reg <= q_next;            -- update counter reg
            
        end if;
    end process;

    -- Flip Flop Inputs
    process(clk, button_in)
    begin
        
        if(rising_edge(clk)) then
            
                DFF1 <= button_in;
                DFF2 <= DFF1;
            
        end if;
    end process;
    q_reset <= DFF1 xor DFF2;           -- if DFF1 and DFF2 are different q_reset <= '1';

    -- Counter Control Based on MSB of counter, q_reg
    process(clk, q_reg, DB_out)
    begin
        
        if(rising_edge(clk)) then
            q_add <= not(q_reg(N-1));        -- enables the counter whe msb is not '1'
            if(q_reg(N-1) = '1') then
                DB_out <= DFF2;
            else
                DB_out <= DB_out;
            end if;
        end if;

    end process;
	 

	
	process(clk)
		begin
			
			if(clk'event and clk='1' and DB_out = '1') then
				count <= count +1;   --increment counter.
				
				
			end if;
--see whether counter value is reached,if yes set the flag.
			if(count = (a)) then
			
				count <= 0;
				flag <='1';
			else
				flag <='0';
			end if;
				
			
		end process;
		
	process(clk)
		begin
		
			if(rising_edge(clk))	then
			
				if(DB_out = '1' AND count = 0)	then
				
					if(buffer_out2 = "00") then
						buffer_out1 <= "01";
                
						else if(buffer_out2 = "01") then
							buffer_out1 <= "10";
                         
							else if(buffer_out2 = "10") then
								buffer_out1 <= "00";
							end if;
						end if;
                end if;
               
              end if;
			end if;
	end process;
				
end arch;
