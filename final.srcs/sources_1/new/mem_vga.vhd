----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.07.2016 11:06:21
-- Design Name: 
-- Module Name: mem_vga - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mem_vga is port
   (clr: in std_logic;
    p_t1,p_t2,p_t3,p_t4,d_t1,d_t2,d_t3,d_t4: in std_logic;
    p_cd_num,d_cd_num: in std_logic_vector(3 downto 0);
    p_cd_col,d_cd_col: in std_logic_vector(2 downto 0);
    p_score,d_score: in std_logic_vector(4 downto 0);
    p_bet: in std_logic_vector(3 downto 0);
    p_budget: in std_logic_vector(8 downto 0);
    easy,medium_in,hard: in std_logic;
    
    d_cd1_col,d_cd2_col,d_cd3_col,d_cd4_col: out std_logic_vector( 2 downto 0);
    p_cd1_col,p_cd2_col,p_cd3_col,p_cd4_col: out std_logic_vector( 2 downto 0);
    d_cd1_num,d_cd2_num,d_cd3_num,d_cd4_num: out std_logic_vector(3 downto 0);
    p_cd1_num,p_cd2_num,p_cd3_num,p_cd4_num: out std_logic_vector(3 downto 0);
    
    
    p_score_ext,d_score_ext: out std_logic_vector(4 downto 0);
    bet: out std_logic_vector(3 downto 0);
    budget: out std_logic_vector(8 downto 0);
    diff_ext: out std_logic_vector(1 downto 0)
    );
end mem_vga;

architecture Behavioral of mem_vga is
signal diff: std_logic_vector(1 downto 0) := "00";
signal p_cd1_num_reg,d_cd1_num_reg,p_cd2_num_reg,d_cd2_num_reg,p_cd3_num_reg,d_cd3_num_reg,p_cd4_num_reg,d_cd4_num_reg:  std_logic_vector(3 downto 0):="0000";
signal p_cd1_col_reg,d_cd1_col_reg,p_cd2_col_reg,d_cd2_col_reg,p_cd3_col_reg,d_cd3_col_reg,p_cd4_col_reg,d_cd4_col_reg: std_logic_vector(2 downto 0):="000";
signal p_score_ext_reg,d_score_ext_reg: std_logic_vector(4 downto 0):="00000";
signal budget_reg: std_logic_vector(8 downto 0):="000000000";
signal bet_reg: std_logic_vector(3 downto 0):="0000";
begin
p_cd1_num<=p_cd1_num_reg;
p_cd2_num<=p_cd2_num_reg;
p_cd3_num<=p_cd3_num_reg;
p_cd4_num<=p_cd4_num_reg;

p_cd1_col<=p_cd1_col_reg;
p_cd2_col<=p_cd2_col_reg;
p_cd3_col<=p_cd3_col_reg;
p_cd4_col<=p_cd4_col_reg;

d_cd1_num<=d_cd1_num_reg;
d_cd2_num<=d_cd2_num_reg;
d_cd3_num<=d_cd3_num_reg;
d_cd4_num<=d_cd4_num_reg;

d_cd1_col<=d_cd1_col_reg;
d_cd2_col<=d_cd2_col_reg;
d_cd3_col<=d_cd3_col_reg;
d_cd4_col<=d_cd4_col_reg;

p_score_ext<=p_score_ext_reg;
d_score_ext<=d_score_ext_reg;

budget<=budget_reg;
bet <= bet_reg;

diff_ext <= diff;

diff <= "01" when easy = '1' else
        "10" when medium_in = '1' else
        "11" when hard = '1' else
        "00";

p_cd1_num_reg<= "0000" when clr='1' else
            p_cd_num when p_t1 = '1' else
            p_cd1_num_reg;
p_cd2_num_reg<= "0000" when clr='1' else
            p_cd_num when p_t2 = '1' else
            p_cd2_num_reg;
p_cd3_num_reg<= "0000" when clr='1' else
            p_cd_num when p_t3 = '1' else
            p_cd3_num_reg;
p_cd4_num_reg<= "0000" when clr='1' else
            p_cd_num when p_t4 = '1' else
            p_cd4_num_reg;

p_cd1_col_reg<= "000" when clr='1' else
            p_cd_col when p_t1 = '1' else
            p_cd1_col_reg;
p_cd2_col_reg<= "000" when clr='1' else
            p_cd_col when p_t2 = '1' else
            p_cd2_col_reg;
p_cd3_col_reg<= "000" when clr='1' else
            p_cd_col when p_t3 = '1' else
            p_cd3_col_reg;
p_cd4_col_reg<= "000" when clr='1' else
            p_cd_col when p_t4 = '1' else
            p_cd4_col_reg;

d_cd1_num_reg<= "0000" when clr='1' else
                        d_cd_num when d_t1 = '1' else
                        d_cd1_num_reg;
d_cd2_num_reg<= "0000" when clr='1' else
                        d_cd_num when d_t2 = '1' else
                        d_cd2_num_reg;
d_cd3_num_reg<= "0000" when clr='1' else
                        d_cd_num when d_t3 = '1' else
                        d_cd3_num_reg;
d_cd4_num_reg<= "0000" when clr='1' else
                        d_cd_num when d_t4 = '1' else
                        d_cd4_num_reg;
            
d_cd1_col_reg<= "000" when clr='1' else
                        d_cd_col when d_t1 = '1' else
                        d_cd1_col_reg;
d_cd2_col_reg<= "000" when clr='1' else
                        d_cd_col when d_t2 = '1' else
                        d_cd2_col_reg;
d_cd3_col_reg<= "000" when clr='1' else
                        d_cd_col when d_t3 = '1' else
                        d_cd3_col_reg;
d_cd4_col_reg<= "000" when clr='1' else
                        d_cd_col when d_t4 = '1' else
                        d_cd4_col_reg;

p_score_ext_reg <= "00000" when clr='1' else
                   p_score when p_score/="00000" else
                   p_score_ext_reg;
d_score_ext_reg <= "00000" when clr='1' else
                   d_score when d_score/="00000" else
                   d_score_ext_reg;

budget_reg <= p_budget;

bet_reg <= p_bet when p_bet/="0000" else
           "0000" when clr='1' else
           bet_reg;

end Behavioral;
