

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity ioblock is

    port(sw: in std_logic_vector(3 downto 0);  --hardwired together
        leds: out std_logic_vector(3 downto 0);
        buttons: in std_logic_vector (2 downto 0);  
        clk: in std_logic;             --stuff
        rdy_A: in std_logic;
        easy,medium_in,hard: in std_logic;
        rdy_B,p_hit,d_hit: in std_logic;
        state_exit: in std_logic_vector(3 downto 0);
        iobus: out std_logic_vector(3 downto 0);    --sw get muxed here
        add_flag: in std_logic;
        iochoice: out std_logic_vector(1 downto 0));--buttons get muxed here
        
end ioblock;

architecture architect of ioblock is
        
         constant a            : integer := 100000000;
      constant N              : integer := 16;
        signal previousbuttons1: std_logic_vector(2 downto 0);
        signal previousbuttons2: std_logic_vector(2 downto 0);
        signal pcounter : unsigned(N downto 0);
        signal ncounter : unsigned(N downto 0);
        signal int_reset,add : std_logic;
        signal buffer_buttons: std_logic_vector (2 downto 0) := "000"; 
      signal temp_sw : std_logic_vector (3 downto 0) := "0000";
         signal count :integer:=0;
  signal flag: std_logic;
    signal enable: std_logic;
    
    signal fallingbuff: std_logic_vector(1 downto 0);

begin
    
    ncounter <= (others => '0') when int_reset = '1' else
           pcounter + 1 when add = '1' else    -- increment count if commanded
                pcounter;                           --SEE HERE                                        **
    iobus <= temp_sw;
   enable <= '0';
   --iochoice <= temp_sw(1 downto 0);
   
   
   
    process(clk, enable)
    begin
        if(rising_edge(clk)) then           --ENABLE MODULE
        
            if(not(enable) = '0') then
                pcounter <= (others => '0');   -- reset counter, see ABOVE                            **
            else
                pcounter <= ncounter;            -- next iteration
                leds <= state_exit;                      -- more hardwiring
                
            end if;
        end if;
    end process;
    
    process(clk, buttons)
    begin
        
        if(rising_edge(clk)) then
            if(not(enable) = '0') then
                previousbuttons1 <= (others => '0');
                previousbuttons2 <= (others => '0');
            else
                previousbuttons1 <= buttons(2 downto 0);
                previousbuttons2 <= previousbuttons1;
            end if;
        end if;
    end process;
   
    int_reset <= (previousbuttons1(0) xor previousbuttons2(0)) xor (previousbuttons2(1) xor previousbuttons1(1)) XOR (previousbuttons2(2) xor previousbuttons1(2)); -- if any of those bits is different
    
    process(clk)
    begin
         
         if(rising_edge(clk)) then
             add <= not(pcounter(N));        -- enables the counter when msb is not '1'
             if(pcounter(N) = '1') then
                 buffer_buttons(2 downto 0) <= previousbuttons2;
             else
                 buffer_buttons <= buffer_buttons;
             end if;
         end if;
 
     end process;
     
    process(clk, sw)            --'enter' button to have the bet reported to iobus
    begin
    
        if(rising_edge(clk)) then
            
            if(buffer_buttons(2) = '1') then
                
                temp_sw <= sw;
                
            elsif(buffer_buttons(2) = '0') then
        
          temp_sw <= "0000";
          
          end if;
          
--            if(temp_sw /= "0000") then
--                temp_sw <= "0000";
--             end if;
                
        end if;  
            
    end process;
   
     process(clk)
    begin
    
if(rising_edge(clk) and (buffer_buttons(1) = '1' OR buffer_buttons(0) = '1')) then
            count <= count +1;   --increment counter.
            
            
          end if;
    --see whether counter value is reached,if yes set the flag.
          if(count = (a)) then
          
            count <= 0;
            flag <='1';
          else
            flag <='0';
          end if;
            
          
        end process;
       
--         process(clk)
--        begin
        
--          if(rising_edge(clk))  then
          
--            if((buffer_buttons(1) = '1' OR buffer_buttons(0) = '1') AND count /= 0)  then
            
--              iochoice <= "00";
            
--            elsif(buffer_buttons(1) = '1' OR buffer_buttons(0) = '1') then
              
--              iochoice <= buffer_buttons(1 downto 0);
            
--            else
              
--              iochoice <= "00";
                   
--                  end if;
--          end if;
--      end process;
        
                            
        process(clk,buttons)
        
        begin
                
                
            if(rising_edge(clk)) then
            
                fallingbuff(0) <= buttons(0);
                fallingbuff(1) <= buttons(1);
                
                if(fallingbuff(0) = '1' AND buttons(0) = '0') then
                
                    iochoice(0) <= '1';
                    
                else 
                
                    iochoice(0) <= '0';
                
                end if;
                
                if(fallingbuff(1) = '1' AND buttons(1) = '0') then
                                
                                    iochoice(1) <= '1';
                                    
                                else 
                                
                                    iochoice(1) <= '0';
                                
                                end if;
            end if;
                 
        
        end process;
    end architect;


--library IEEE;
--use IEEE.STD_LOGIC_1164.ALL;

---- Uncomment the following library declaration if using
---- arithmetic functions with Signed or Unsigned values
----use IEEE.NUMERIC_STD.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx leaf cells in this code.
----library UNISIM;
----use UNISIM.VComponents.all;

--entity ioblock is
--port(
--        sw: in std_logic_vector(3 downto 0);  --hardwired together
--        leds: out std_logic_vector(3 downto 0);
--        buttons: in std_logic_vector (2 downto 0);  
--        clk: in std_logic;
--        iobus: out std_logic_vector(3 downto 0);    --sw get muxed here
--        iochoice: out std_logic_vector(1 downto 0);  --buttons get muxed here
--        rst: in std_logic
--);
--end ioblock;

--architecture ioarch of ioblock is

--    constant difficulty: std_logic_vector(1 downto 0):= "01";
--    constant regulario: std_logic_vector(1 downto 0):= "10";
--    signal n_state,p_state: std_logic_vector(1 downto 0):= "01";
--    signal prev_buttons: std_logic_vector(2 downto 0);
--    signal int_buttons: std_logic_vector(2 downto 0);

--begin
    
    
--    --state changing process
--    process(clk,rst)
--    begin
        
--        if(rising_edge(clk)) then
        
--            p_state <= n_state;
        
--        end if;
       
--    end process;
    
    
--    --doing shit process
--    process(clk, buttons, sw)
    
--    begin
--    if(rst = '1') then
    
--    n_state <= "01";
    
--    elsif(rising_edge(clk)) then
        
--        prev_buttons <= buttons;
--        leds <= prev_buttons(2 downto 1) & p_state;
        
--        case p_state is
        
--            WHEN difficulty =>
                
--                if(buttons /= "000") then
                    
--                    --leds <= '1' & sw(2 downto 0);
                    
                    
--                else
                    
--                    --leds <= '1' & sw(2 downto 0);
                    
--                    if(prev_buttons(2) = '1' AND buttons(2) = '0') then
                    
--                        iobus(2 downto 0) <= sw(2 downto 0);
--                        n_state <= regulario;
                    
--                    end if;
--                end if;            
             
--             WHEN regulario =>
             
--                if(buttons /= "000") then
                
--                    leds <= sw;
                    
                 
--                elsif(buttons = "000") then
                
--                    leds <= sw;
--                    prev_buttons <= buttons;
                    
--                    if(prev_buttons(2) = '1' AND buttons(2) = '0') then
                 
--                        iobus(2 downto 0) <= sw(2 downto 0);
                 
--                    elsif(prev_buttons(1) = '1' AND buttons(1) = '0') then
                    
--                        iochoice <= "01";
                    
--                    elsif(prev_buttons(0) = '1' AND buttons(0) = '0') then
                            
--                        iochoice <= "10";
                            
--                    else
                    
--                        iochoice <= (others => '0');
--                        iobus <= (others => '0');
                        
--                    end if;  
                    
--                end if;
                
--             WHEN others =>
             
--                n_state <= n_state;
                
--        end case;
    
--    end if;
    
--    end process;
--end ioarch;
