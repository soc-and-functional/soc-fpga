----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09.06.2016 15:37:32
-- Design Name: 
-- Module Name: rgb_selector - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rgb_selector is Port 
(   
    win, lose, draw: in std_logic;
    clk: in std_logic;
    selector: in std_logic;
    rgb_screen :in std_logic_vector(15 downto 0);
    rgb_title :in std_logic_vector(15 downto 0);
    rgb_end :in std_logic_vector(15 downto 0);
    rgb_out :out std_logic_vector(15 downto 0)
 );
end rgb_selector;

architecture Behavioral of rgb_selector is
signal sel: std_logic;
begin

process(selector)
begin
    sel <= selector;
end process;

    rgb_out<= rgb_title when ((sel='0') AND (win='0') AND (lose='0')) else
              rgb_end when ((win='1') OR (lose='1') OR (draw='1')) else
              rgb_screen;
    

end Behavioral;
