-- (c) Copyright 1995-2016 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:module_ref:screen:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_screen_0_0 IS
  PORT (
    video_on : IN STD_LOGIC;
    pixel_x : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    pixel_y : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
    d1_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    d2_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    d3_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    d4_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    p1_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    p2_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    p3_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    p4_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
    d1_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d2_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d3_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d4_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p1_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p2_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p3_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p4_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p_score : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    d_score : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
    p_bet : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    iobus : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    p_budget : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    rgb_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    diff_ch : IN STD_LOGIC_VECTOR(1 DOWNTO 0)
  );
END design_1_screen_0_0;

ARCHITECTURE design_1_screen_0_0_arch OF design_1_screen_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_screen_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT screen IS
    PORT (
      video_on : IN STD_LOGIC;
      pixel_x : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
      pixel_y : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
      d1_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      d2_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      d3_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      d4_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      p1_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      p2_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      p3_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      p4_cd_col : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
      d1_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      d2_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      d3_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      d4_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p1_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p2_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p3_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p4_cd_num : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p_score : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      d_score : IN STD_LOGIC_VECTOR(4 DOWNTO 0);
      p_bet : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      iobus : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      p_budget : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
      rgb_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      diff_ch : IN STD_LOGIC_VECTOR(1 DOWNTO 0)
    );
  END COMPONENT screen;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_screen_0_0_arch: ARCHITECTURE IS "screen,Vivado 2016.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_screen_0_0_arch : ARCHITECTURE IS "design_1_screen_0_0,screen,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_screen_0_0_arch: ARCHITECTURE IS "design_1_screen_0_0,screen,{x_ipProduct=Vivado 2016.1,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=screen,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED}";
BEGIN
  U0 : screen
    PORT MAP (
      video_on => video_on,
      pixel_x => pixel_x,
      pixel_y => pixel_y,
      d1_cd_col => d1_cd_col,
      d2_cd_col => d2_cd_col,
      d3_cd_col => d3_cd_col,
      d4_cd_col => d4_cd_col,
      p1_cd_col => p1_cd_col,
      p2_cd_col => p2_cd_col,
      p3_cd_col => p3_cd_col,
      p4_cd_col => p4_cd_col,
      d1_cd_num => d1_cd_num,
      d2_cd_num => d2_cd_num,
      d3_cd_num => d3_cd_num,
      d4_cd_num => d4_cd_num,
      p1_cd_num => p1_cd_num,
      p2_cd_num => p2_cd_num,
      p3_cd_num => p3_cd_num,
      p4_cd_num => p4_cd_num,
      p_score => p_score,
      d_score => d_score,
      p_bet => p_bet,
      iobus => iobus,
      p_budget => p_budget,
      rgb_out => rgb_out,
      diff_ch => diff_ch
    );
END design_1_screen_0_0_arch;
