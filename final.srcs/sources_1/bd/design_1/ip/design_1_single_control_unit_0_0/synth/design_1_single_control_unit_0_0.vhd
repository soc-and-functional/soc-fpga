-- (c) Copyright 1995-2016 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: xilinx.com:module_ref:single_control_unit:1.0
-- IP Revision: 1

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_single_control_unit_0_0 IS
  PORT (
    clk : IN STD_LOGIC;
    reset : IN STD_LOGIC;
    iobus : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    iochoice : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    can_bet : IN STD_LOGIC;
    bankrupt : IN STD_LOGIC;
    p_card_rdy : IN STD_LOGIC;
    d_card_rdy : IN STD_LOGIC;
    bet_amount : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    player_hit : OUT STD_LOGIC;
    dealer_hit : OUT STD_LOGIC;
    dealer_win : OUT STD_LOGIC;
    player_win : OUT STD_LOGIC;
    p_score_ext : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    d_score_ext : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    state_exit : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
    add_flag_ext : OUT STD_LOGIC;
    p_t1 : OUT STD_LOGIC;
    p_t2 : OUT STD_LOGIC;
    p_t3 : OUT STD_LOGIC;
    p_t4 : OUT STD_LOGIC;
    d_t1 : OUT STD_LOGIC;
    d_t2 : OUT STD_LOGIC;
    d_t3 : OUT STD_LOGIC;
    d_t4 : OUT STD_LOGIC;
    p_card : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d_card : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    mem_clear : OUT STD_LOGIC;
    easy_out : OUT STD_LOGIC;
    medium_out : OUT STD_LOGIC;
    hard_out : OUT STD_LOGIC;
    tie : OUT STD_LOGIC;
    vga_debug : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
  );
END design_1_single_control_unit_0_0;

ARCHITECTURE design_1_single_control_unit_0_0_arch OF design_1_single_control_unit_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_single_control_unit_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT single_control_unit IS
    PORT (
      clk : IN STD_LOGIC;
      reset : IN STD_LOGIC;
      iobus : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      iochoice : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
      can_bet : IN STD_LOGIC;
      bankrupt : IN STD_LOGIC;
      p_card_rdy : IN STD_LOGIC;
      d_card_rdy : IN STD_LOGIC;
      bet_amount : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      player_hit : OUT STD_LOGIC;
      dealer_hit : OUT STD_LOGIC;
      dealer_win : OUT STD_LOGIC;
      player_win : OUT STD_LOGIC;
      p_score_ext : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      d_score_ext : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      state_exit : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      add_flag_ext : OUT STD_LOGIC;
      p_t1 : OUT STD_LOGIC;
      p_t2 : OUT STD_LOGIC;
      p_t3 : OUT STD_LOGIC;
      p_t4 : OUT STD_LOGIC;
      d_t1 : OUT STD_LOGIC;
      d_t2 : OUT STD_LOGIC;
      d_t3 : OUT STD_LOGIC;
      d_t4 : OUT STD_LOGIC;
      p_card : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      d_card : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      mem_clear : OUT STD_LOGIC;
      easy_out : OUT STD_LOGIC;
      medium_out : OUT STD_LOGIC;
      hard_out : OUT STD_LOGIC;
      tie : OUT STD_LOGIC;
      vga_debug : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
    );
  END COMPONENT single_control_unit;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_single_control_unit_0_0_arch: ARCHITECTURE IS "single_control_unit,Vivado 2016.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_single_control_unit_0_0_arch : ARCHITECTURE IS "design_1_single_control_unit_0_0,single_control_unit,{}";
  ATTRIBUTE CORE_GENERATION_INFO : STRING;
  ATTRIBUTE CORE_GENERATION_INFO OF design_1_single_control_unit_0_0_arch: ARCHITECTURE IS "design_1_single_control_unit_0_0,single_control_unit,{x_ipProduct=Vivado 2016.1,x_ipVendor=xilinx.com,x_ipLibrary=module_ref,x_ipName=single_control_unit,x_ipVersion=1.0,x_ipCoreRevision=1,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
  ATTRIBUTE X_INTERFACE_INFO OF reset: SIGNAL IS "xilinx.com:signal:reset:1.0 reset RST";
BEGIN
  U0 : single_control_unit
    PORT MAP (
      clk => clk,
      reset => reset,
      iobus => iobus,
      iochoice => iochoice,
      can_bet => can_bet,
      bankrupt => bankrupt,
      p_card_rdy => p_card_rdy,
      d_card_rdy => d_card_rdy,
      bet_amount => bet_amount,
      player_hit => player_hit,
      dealer_hit => dealer_hit,
      dealer_win => dealer_win,
      player_win => player_win,
      p_score_ext => p_score_ext,
      d_score_ext => d_score_ext,
      state_exit => state_exit,
      add_flag_ext => add_flag_ext,
      p_t1 => p_t1,
      p_t2 => p_t2,
      p_t3 => p_t3,
      p_t4 => p_t4,
      d_t1 => d_t1,
      d_t2 => d_t2,
      d_t3 => d_t3,
      d_t4 => d_t4,
      p_card => p_card,
      d_card => d_card,
      mem_clear => mem_clear,
      easy_out => easy_out,
      medium_out => medium_out,
      hard_out => hard_out,
      tie => tie,
      vga_debug => vga_debug
    );
END design_1_single_control_unit_0_0_arch;
