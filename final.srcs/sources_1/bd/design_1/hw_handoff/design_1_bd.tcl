
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2016.1
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# HWreset, firstscreen, ioblock, lastscreen, mem_vga, memory, rgb_selector, screen, single_control_unit, vga_sync, wallet

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z010clg400-1
   set_property BOARD_PART digilentinc.com:zybo:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_msg_id "BD_TCL-001" "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_msg_id "BD_TCL-002" "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_msg_id "BD_TCL-003" "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_msg_id "BD_TCL-004" "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_msg_id "BD_TCL-005" "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_msg_id "BD_TCL-114" "ERROR" $errMsg}
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports

  # Create ports
  set button_in [ create_bd_port -dir I button_in ]
  set buttons [ create_bd_port -dir I -from 2 -to 0 buttons ]
  set hsync [ create_bd_port -dir O hsync ]
  set leds [ create_bd_port -dir O -from 3 -to 0 leds ]
  set rgb_out [ create_bd_port -dir O -from 15 -to 0 rgb_out ]
  set sw [ create_bd_port -dir I -from 3 -to 0 sw ]
  set sys_clock [ create_bd_port -dir I -type clk sys_clock ]
  set_property -dict [ list \
CONFIG.FREQ_HZ {125000000} \
CONFIG.PHASE {0.000} \
 ] $sys_clock
  set vsync [ create_bd_port -dir O vsync ]

  # Create instance: HWreset_0, and set properties
  set block_name HWreset
  set block_cell_name HWreset_0
  if { [catch {set HWreset_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $HWreset_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.3 clk_wiz_0 ]
  set_property -dict [ list \
CONFIG.CLKIN1_JITTER_PS {80.0} \
CONFIG.CLKOUT1_DRIVES {BUFG} \
CONFIG.CLKOUT1_JITTER {288.707} \
CONFIG.CLKOUT1_PHASE_ERROR {258.220} \
CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {50.000} \
CONFIG.CLKOUT2_DRIVES {BUFG} \
CONFIG.CLKOUT3_DRIVES {BUFG} \
CONFIG.CLKOUT4_DRIVES {BUFG} \
CONFIG.CLKOUT5_DRIVES {BUFG} \
CONFIG.CLKOUT6_DRIVES {BUFG} \
CONFIG.CLKOUT7_DRIVES {BUFG} \
CONFIG.CLK_IN1_BOARD_INTERFACE {sys_clock} \
CONFIG.MMCM_CLKFBOUT_MULT_F {34} \
CONFIG.MMCM_CLKIN1_PERIOD {8.0} \
CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F {17} \
CONFIG.MMCM_COMPENSATION {ZHOLD} \
CONFIG.MMCM_DIVCLK_DIVIDE {5} \
CONFIG.PRIMITIVE {PLL} \
CONFIG.USE_BOARD_FLOW {true} \
CONFIG.USE_LOCKED {false} \
CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Need to retain value_src of defaults
  set_property -dict [ list \
CONFIG.MMCM_CLKIN2_PERIOD.VALUE_SRC {DEFAULT} \
 ] $clk_wiz_0

  # Create instance: firstscreen_0, and set properties
  set block_name firstscreen
  set block_cell_name firstscreen_0
  if { [catch {set firstscreen_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $firstscreen_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: ioblock_0, and set properties
  set block_name ioblock
  set block_cell_name ioblock_0
  if { [catch {set ioblock_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $ioblock_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: lastscreen_0, and set properties
  set block_name lastscreen
  set block_cell_name lastscreen_0
  if { [catch {set lastscreen_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $lastscreen_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: mem_vga_0, and set properties
  set block_name mem_vga
  set block_cell_name mem_vga_0
  if { [catch {set mem_vga_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $mem_vga_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: memory_0, and set properties
  set block_name memory
  set block_cell_name memory_0
  if { [catch {set memory_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $memory_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: rgb_selector_0, and set properties
  set block_name rgb_selector
  set block_cell_name rgb_selector_0
  if { [catch {set rgb_selector_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $rgb_selector_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: screen_0, and set properties
  set block_name screen
  set block_cell_name screen_0
  if { [catch {set screen_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $screen_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: single_control_unit_0, and set properties
  set block_name single_control_unit
  set block_cell_name single_control_unit_0
  if { [catch {set single_control_unit_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $single_control_unit_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: vga_sync_0, and set properties
  set block_name vga_sync
  set block_cell_name vga_sync_0
  if { [catch {set vga_sync_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $vga_sync_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: wallet_0, and set properties
  set block_name wallet
  set block_cell_name wallet_0
  if { [catch {set wallet_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $wallet_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create port connections
  connect_bd_net -net HWreset_0_b_lower [get_bd_pins HWreset_0/b_lower] [get_bd_pins rgb_selector_0/selector]
  connect_bd_net -net HWreset_0_b_upper [get_bd_pins HWreset_0/b_upper] [get_bd_pins memory_0/reset] [get_bd_pins single_control_unit_0/reset] [get_bd_pins vga_sync_0/reset] [get_bd_pins wallet_0/reset]
  connect_bd_net -net button_in_1 [get_bd_ports button_in] [get_bd_pins HWreset_0/button_in]
  connect_bd_net -net buttons_1 [get_bd_ports buttons] [get_bd_pins ioblock_0/buttons]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins HWreset_0/clk] [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins ioblock_0/clk] [get_bd_pins memory_0/CLK] [get_bd_pins rgb_selector_0/clk] [get_bd_pins single_control_unit_0/clk] [get_bd_pins vga_sync_0/clk] [get_bd_pins wallet_0/clk]
  connect_bd_net -net firstscreen_0_rgb_out [get_bd_pins firstscreen_0/rgb_out] [get_bd_pins rgb_selector_0/rgb_title]
  connect_bd_net -net ioblock_0_iobus [get_bd_pins ioblock_0/iobus] [get_bd_pins single_control_unit_0/iobus]
  connect_bd_net -net ioblock_0_iochoice [get_bd_pins ioblock_0/iochoice] [get_bd_pins single_control_unit_0/iochoice]
  connect_bd_net -net ioblock_0_leds [get_bd_ports leds] [get_bd_pins ioblock_0/leds]
  connect_bd_net -net lastscreen_0_rgb_out [get_bd_pins lastscreen_0/rgb_out] [get_bd_pins rgb_selector_0/rgb_end]
  connect_bd_net -net mem_vga_0_bet [get_bd_pins mem_vga_0/bet] [get_bd_pins screen_0/p_bet]
  connect_bd_net -net mem_vga_0_budget [get_bd_pins mem_vga_0/budget] [get_bd_pins screen_0/p_budget]
  connect_bd_net -net mem_vga_0_d_cd1_col [get_bd_pins mem_vga_0/d_cd1_col] [get_bd_pins screen_0/d1_cd_col]
  connect_bd_net -net mem_vga_0_d_cd1_num [get_bd_pins mem_vga_0/d_cd1_num] [get_bd_pins screen_0/d1_cd_num]
  connect_bd_net -net mem_vga_0_d_cd2_col [get_bd_pins mem_vga_0/d_cd2_col] [get_bd_pins screen_0/d2_cd_col]
  connect_bd_net -net mem_vga_0_d_cd2_num [get_bd_pins mem_vga_0/d_cd2_num] [get_bd_pins screen_0/d2_cd_num]
  connect_bd_net -net mem_vga_0_d_cd3_col [get_bd_pins mem_vga_0/d_cd3_col] [get_bd_pins screen_0/d3_cd_col]
  connect_bd_net -net mem_vga_0_d_cd3_num [get_bd_pins mem_vga_0/d_cd3_num] [get_bd_pins screen_0/d3_cd_num]
  connect_bd_net -net mem_vga_0_d_cd4_col [get_bd_pins mem_vga_0/d_cd4_col] [get_bd_pins screen_0/d4_cd_col]
  connect_bd_net -net mem_vga_0_d_cd4_num [get_bd_pins mem_vga_0/d_cd4_num] [get_bd_pins screen_0/d4_cd_num]
  connect_bd_net -net mem_vga_0_d_score_ext [get_bd_pins mem_vga_0/d_score_ext] [get_bd_pins screen_0/d_score]
  connect_bd_net -net mem_vga_0_diff_ext [get_bd_pins mem_vga_0/diff_ext] [get_bd_pins screen_0/diff_ch]
  connect_bd_net -net mem_vga_0_p_cd1_col [get_bd_pins mem_vga_0/p_cd1_col] [get_bd_pins screen_0/p1_cd_col]
  connect_bd_net -net mem_vga_0_p_cd1_num [get_bd_pins mem_vga_0/p_cd1_num] [get_bd_pins screen_0/p1_cd_num]
  connect_bd_net -net mem_vga_0_p_cd2_col [get_bd_pins mem_vga_0/p_cd2_col] [get_bd_pins screen_0/p2_cd_col]
  connect_bd_net -net mem_vga_0_p_cd2_num [get_bd_pins mem_vga_0/p_cd2_num] [get_bd_pins screen_0/p2_cd_num]
  connect_bd_net -net mem_vga_0_p_cd3_col [get_bd_pins mem_vga_0/p_cd3_col] [get_bd_pins screen_0/p3_cd_col]
  connect_bd_net -net mem_vga_0_p_cd3_num [get_bd_pins mem_vga_0/p_cd3_num] [get_bd_pins screen_0/p3_cd_num]
  connect_bd_net -net mem_vga_0_p_cd4_col [get_bd_pins mem_vga_0/p_cd4_col] [get_bd_pins screen_0/p4_cd_col]
  connect_bd_net -net mem_vga_0_p_cd4_num [get_bd_pins mem_vga_0/p_cd4_num] [get_bd_pins screen_0/p4_cd_num]
  connect_bd_net -net mem_vga_0_p_score_ext [get_bd_pins mem_vga_0/p_score_ext] [get_bd_pins screen_0/p_score]
  connect_bd_net -net memory_0_card_colA [get_bd_pins mem_vga_0/p_cd_col] [get_bd_pins memory_0/card_colA]
  connect_bd_net -net memory_0_card_colB [get_bd_pins mem_vga_0/d_cd_col] [get_bd_pins memory_0/card_colB]
  connect_bd_net -net memory_0_card_outA [get_bd_pins mem_vga_0/p_cd_num] [get_bd_pins memory_0/card_outA] [get_bd_pins single_control_unit_0/p_card]
  connect_bd_net -net memory_0_card_outB [get_bd_pins mem_vga_0/d_cd_num] [get_bd_pins memory_0/card_outB] [get_bd_pins single_control_unit_0/d_card]
  connect_bd_net -net memory_0_card_rdyA [get_bd_pins memory_0/card_rdyA] [get_bd_pins single_control_unit_0/p_card_rdy]
  connect_bd_net -net memory_0_card_rdyB [get_bd_pins memory_0/card_rdyB] [get_bd_pins single_control_unit_0/d_card_rdy]
  connect_bd_net -net rgb_selector_0_rgb_out [get_bd_ports rgb_out] [get_bd_pins rgb_selector_0/rgb_out]
  connect_bd_net -net screen_0_rgb_out [get_bd_pins rgb_selector_0/rgb_screen] [get_bd_pins screen_0/rgb_out]
  connect_bd_net -net single_control_unit_0_bet_amount [get_bd_pins mem_vga_0/p_bet] [get_bd_pins single_control_unit_0/bet_amount] [get_bd_pins wallet_0/bet_amount]
  connect_bd_net -net single_control_unit_0_d_score_ext [get_bd_pins mem_vga_0/d_score] [get_bd_pins single_control_unit_0/d_score_ext]
  connect_bd_net -net single_control_unit_0_d_t1 [get_bd_pins mem_vga_0/d_t1] [get_bd_pins single_control_unit_0/d_t1]
  connect_bd_net -net single_control_unit_0_d_t2 [get_bd_pins mem_vga_0/d_t2] [get_bd_pins single_control_unit_0/d_t2]
  connect_bd_net -net single_control_unit_0_d_t3 [get_bd_pins mem_vga_0/d_t3] [get_bd_pins single_control_unit_0/d_t3]
  connect_bd_net -net single_control_unit_0_d_t4 [get_bd_pins mem_vga_0/d_t4] [get_bd_pins single_control_unit_0/d_t4]
  connect_bd_net -net single_control_unit_0_dealer_hit [get_bd_pins memory_0/getB] [get_bd_pins single_control_unit_0/dealer_hit]
  connect_bd_net -net single_control_unit_0_dealer_win [get_bd_pins lastscreen_0/lost] [get_bd_pins rgb_selector_0/lose] [get_bd_pins single_control_unit_0/dealer_win] [get_bd_pins wallet_0/dealer_win]
  connect_bd_net -net single_control_unit_0_easy_out [get_bd_pins mem_vga_0/easy] [get_bd_pins single_control_unit_0/easy_out]
  connect_bd_net -net single_control_unit_0_hard_out [get_bd_pins mem_vga_0/hard] [get_bd_pins single_control_unit_0/hard_out]
  connect_bd_net -net single_control_unit_0_medium_out [get_bd_pins mem_vga_0/medium_in] [get_bd_pins single_control_unit_0/medium_out]
  connect_bd_net -net single_control_unit_0_mem_clear [get_bd_pins mem_vga_0/clr] [get_bd_pins memory_0/mem_clr] [get_bd_pins single_control_unit_0/mem_clear] [get_bd_pins wallet_0/mem_clr]
  connect_bd_net -net single_control_unit_0_p_score_ext [get_bd_pins mem_vga_0/p_score] [get_bd_pins single_control_unit_0/p_score_ext]
  connect_bd_net -net single_control_unit_0_p_t1 [get_bd_pins mem_vga_0/p_t1] [get_bd_pins single_control_unit_0/p_t1]
  connect_bd_net -net single_control_unit_0_p_t2 [get_bd_pins mem_vga_0/p_t2] [get_bd_pins single_control_unit_0/p_t2]
  connect_bd_net -net single_control_unit_0_p_t3 [get_bd_pins mem_vga_0/p_t3] [get_bd_pins single_control_unit_0/p_t3]
  connect_bd_net -net single_control_unit_0_p_t4 [get_bd_pins mem_vga_0/p_t4] [get_bd_pins single_control_unit_0/p_t4]
  connect_bd_net -net single_control_unit_0_player_hit [get_bd_pins memory_0/getA] [get_bd_pins single_control_unit_0/player_hit]
  connect_bd_net -net single_control_unit_0_player_win [get_bd_pins lastscreen_0/win] [get_bd_pins rgb_selector_0/win] [get_bd_pins single_control_unit_0/player_win] [get_bd_pins wallet_0/player_win]
  connect_bd_net -net single_control_unit_0_state_exit [get_bd_pins ioblock_0/state_exit] [get_bd_pins single_control_unit_0/state_exit]
  connect_bd_net -net single_control_unit_0_tie [get_bd_pins lastscreen_0/tie] [get_bd_pins rgb_selector_0/draw] [get_bd_pins single_control_unit_0/tie]
  connect_bd_net -net sw_1 [get_bd_ports sw] [get_bd_pins ioblock_0/sw]
  connect_bd_net -net sys_clock_1 [get_bd_ports sys_clock] [get_bd_pins clk_wiz_0/clk_in1]
  connect_bd_net -net vga_sync_0_hsync [get_bd_ports hsync] [get_bd_pins vga_sync_0/hsync]
  connect_bd_net -net vga_sync_0_pixel_x [get_bd_pins firstscreen_0/pixel_x] [get_bd_pins lastscreen_0/pixel_x] [get_bd_pins screen_0/pixel_x] [get_bd_pins vga_sync_0/pixel_x]
  connect_bd_net -net vga_sync_0_pixel_y [get_bd_pins firstscreen_0/pixel_y] [get_bd_pins lastscreen_0/pixel_y] [get_bd_pins screen_0/pixel_y] [get_bd_pins vga_sync_0/pixel_y]
  connect_bd_net -net vga_sync_0_video_on [get_bd_pins firstscreen_0/video_on] [get_bd_pins lastscreen_0/video_on] [get_bd_pins screen_0/video_on] [get_bd_pins vga_sync_0/video_on]
  connect_bd_net -net vga_sync_0_vsync [get_bd_ports vsync] [get_bd_pins vga_sync_0/vsync]
  connect_bd_net -net wallet_0_budget [get_bd_pins mem_vga_0/p_budget] [get_bd_pins wallet_0/budget]
  connect_bd_net -net wallet_0_can_bet [get_bd_pins single_control_unit_0/can_bet] [get_bd_pins wallet_0/can_bet]
  connect_bd_net -net wallet_0_player_bankrupt [get_bd_pins lastscreen_0/broke] [get_bd_pins single_control_unit_0/bankrupt] [get_bd_pins wallet_0/player_bankrupt]

  # Create address segments

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.12  2016-01-29 bk=1.3547 VDI=39 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port vsync -pg 1 -y 630 -defaultsOSRD
preplace port button_in -pg 1 -y 840 -defaultsOSRD
preplace port hsync -pg 1 -y 610 -defaultsOSRD
preplace port sys_clock -pg 1 -y 790 -defaultsOSRD
preplace portBus buttons -pg 1 -y 920 -defaultsOSRD
preplace portBus sw -pg 1 -y 900 -defaultsOSRD
preplace portBus rgb_out -pg 1 -y 740 -defaultsOSRD
preplace portBus leds -pg 1 -y 860 -defaultsOSRD
preplace inst ioblock_0 -pg 1 -lvl 3 -y 1010 -defaultsOSRD
preplace inst memory_0 -pg 1 -lvl 3 -y 470 -defaultsOSRD
preplace inst wallet_0 -pg 1 -lvl 3 -y 700 -defaultsOSRD
preplace inst lastscreen_0 -pg 1 -lvl 6 -y 970 -defaultsOSRD
preplace inst HWreset_0 -pg 1 -lvl 2 -y 830 -defaultsOSRD
preplace inst screen_0 -pg 1 -lvl 6 -y 370 -defaultsOSRD
preplace inst rgb_selector_0 -pg 1 -lvl 7 -y 740 -defaultsOSRD
preplace inst single_control_unit_0 -pg 1 -lvl 4 -y 260 -defaultsOSRD
preplace inst mem_vga_0 -pg 1 -lvl 5 -y 390 -defaultsOSRD
preplace inst clk_wiz_0 -pg 1 -lvl 1 -y 790 -defaultsOSRD
preplace inst vga_sync_0 -pg 1 -lvl 5 -y 760 -defaultsOSRD
preplace inst firstscreen_0 -pg 1 -lvl 6 -y 780 -defaultsOSRD
preplace netloc vga_sync_0_pixel_x 1 5 1 1580
preplace netloc vga_sync_0_pixel_y 1 5 1 1590
preplace netloc sys_clock_1 1 0 1 NJ
preplace netloc buttons_1 1 0 3 NJ 920 NJ 920 NJ
preplace netloc wallet_0_budget 1 3 2 NJ 680 1240
preplace netloc single_control_unit_0_dealer_win 1 2 5 420 820 NJ 810 1230 880 1620 690 NJ
preplace netloc single_control_unit_0_hard_out 1 4 1 1180
preplace netloc single_control_unit_0_d_t1 1 4 1 N
preplace netloc memory_0_card_outA 1 3 2 800 530 1150
preplace netloc mem_vga_0_p_cd4_col 1 5 1 N
preplace netloc single_control_unit_0_d_t2 1 4 1 N
preplace netloc rgb_selector_0_rgb_out 1 7 1 NJ
preplace netloc mem_vga_0_p_cd4_num 1 5 1 N
preplace netloc memory_0_card_outB 1 3 2 810 540 1160
preplace netloc single_control_unit_0_d_t3 1 4 1 N
preplace netloc mem_vga_0_d_cd3_col 1 5 1 N
preplace netloc firstscreen_0_rgb_out 1 6 1 1920
preplace netloc single_control_unit_0_d_t4 1 4 1 N
preplace netloc vga_sync_0_video_on 1 5 1 1570
preplace netloc mem_vga_0_d_cd4_num 1 5 1 N
preplace netloc memory_0_card_rdyA 1 3 1 710
preplace netloc mem_vga_0_budget 1 5 1 1620
preplace netloc vga_sync_0_vsync 1 5 3 NJ 670 NJ 620 NJ
preplace netloc single_control_unit_0_easy_out 1 4 1 1110
preplace netloc memory_0_card_rdyB 1 3 1 730
preplace netloc lastscreen_0_rgb_out 1 6 1 1950
preplace netloc vga_sync_0_hsync 1 5 3 NJ 660 NJ 610 NJ
preplace netloc mem_vga_0_p_cd2_num 1 5 1 N
preplace netloc wallet_0_can_bet 1 3 1 700
preplace netloc single_control_unit_0_tie 1 4 3 1140 900 1600 700 NJ
preplace netloc sw_1 1 0 3 NJ 900 NJ 900 NJ
preplace netloc mem_vga_0_p_cd3_col 1 5 1 N
preplace netloc screen_0_rgb_out 1 6 1 1930
preplace netloc mem_vga_0_p_cd1_col 1 5 1 N
preplace netloc mem_vga_0_d_cd2_col 1 5 1 N
preplace netloc mem_vga_0_d_score_ext 1 5 1 N
preplace netloc memory_0_card_colA 1 3 2 NJ 550 1210
preplace netloc single_control_unit_0_p_t1 1 4 1 N
preplace netloc single_control_unit_0_player_hit 1 2 3 410 570 NJ 570 1130
preplace netloc single_control_unit_0_player_win 1 2 5 410 810 NJ 800 1220 870 1610 680 NJ
preplace netloc ioblock_0_iochoice 1 3 1 760
preplace netloc HWreset_0_b_lower 1 2 5 NJ 840 NJ 840 NJ 850 NJ 850 1940
preplace netloc memory_0_card_colB 1 3 2 NJ 560 1190
preplace netloc single_control_unit_0_p_t2 1 4 1 N
preplace netloc single_control_unit_0_p_t3 1 4 1 N
preplace netloc single_control_unit_0_p_t4 1 4 1 N
preplace netloc mem_vga_0_p_cd2_col 1 5 1 N
preplace netloc single_control_unit_0_mem_clear 1 2 3 400 590 NJ 590 1100
preplace netloc single_control_unit_0_dealer_hit 1 2 3 420 580 NJ 580 1120
preplace netloc mem_vga_0_d_cd4_col 1 5 1 N
preplace netloc mem_vga_0_diff_ext 1 5 1 1540
preplace netloc mem_vga_0_d_cd3_num 1 5 1 N
preplace netloc button_in_1 1 0 2 NJ 840 NJ
preplace netloc mem_vga_0_d_cd1_col 1 5 1 N
preplace netloc mem_vga_0_bet 1 5 1 N
preplace netloc single_control_unit_0_d_score_ext 1 4 1 1240
preplace netloc mem_vga_0_d_cd2_num 1 5 1 N
preplace netloc single_control_unit_0_p_score_ext 1 4 1 1250
preplace netloc clk_wiz_0_clk_out1 1 1 6 180 930 390 800 770 820 1250 670 NJ 710 NJ
preplace netloc single_control_unit_0_medium_out 1 4 1 1200
preplace netloc mem_vga_0_p_score_ext 1 5 1 N
preplace netloc single_control_unit_0_bet_amount 1 2 3 420 600 NJ 600 1170
preplace netloc ioblock_0_leds 1 3 5 NJ 860 NJ 860 NJ 860 NJ 860 NJ
preplace netloc wallet_0_player_bankrupt 1 3 3 790 870 NJ 890 NJ
preplace netloc mem_vga_0_d_cd1_num 1 5 1 N
preplace netloc HWreset_0_b_upper 1 2 3 380 830 740 830 NJ
preplace netloc mem_vga_0_p_cd1_num 1 5 1 N
preplace netloc single_control_unit_0_state_exit 1 2 3 420 850 NJ 880 1090
preplace netloc mem_vga_0_p_cd3_num 1 5 1 N
preplace netloc ioblock_0_iobus 1 3 1 720
levelinfo -pg 1 0 100 280 560 950 1400 1760 2090 2250 -top 0 -bot 1170
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


