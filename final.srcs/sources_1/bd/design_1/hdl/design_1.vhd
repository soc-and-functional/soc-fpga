--Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2016.1 (win64) Build 1538259 Fri Apr  8 15:45:27 MDT 2016
--Date        : Sat Jul 09 18:35:41 2016
--Host        : Rajput-PC running 64-bit major release  (build 9200)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    button_in : in STD_LOGIC;
    buttons : in STD_LOGIC_VECTOR ( 2 downto 0 );
    hsync : out STD_LOGIC;
    leds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    rgb_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    sw : in STD_LOGIC_VECTOR ( 3 downto 0 );
    sys_clock : in STD_LOGIC;
    vsync : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=12,numReposBlks=12,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=11,numPkgbdBlks=0,bdsource=USER,da_board_cnt=1,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_HWreset_0_0 is
  port (
    clk : in STD_LOGIC;
    button_in : in STD_LOGIC;
    b_upper : out STD_LOGIC;
    b_lower : out STD_LOGIC
  );
  end component design_1_HWreset_0_0;
  component design_1_ioblock_0_0 is
  port (
    sw : in STD_LOGIC_VECTOR ( 3 downto 0 );
    leds : out STD_LOGIC_VECTOR ( 3 downto 0 );
    buttons : in STD_LOGIC_VECTOR ( 2 downto 0 );
    clk : in STD_LOGIC;
    rdy_A : in STD_LOGIC;
    easy : in STD_LOGIC;
    medium_in : in STD_LOGIC;
    hard : in STD_LOGIC;
    rdy_B : in STD_LOGIC;
    p_hit : in STD_LOGIC;
    d_hit : in STD_LOGIC;
    state_exit : in STD_LOGIC_VECTOR ( 3 downto 0 );
    iobus : out STD_LOGIC_VECTOR ( 3 downto 0 );
    add_flag : in STD_LOGIC;
    iochoice : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component design_1_ioblock_0_0;
  component design_1_memory_0_0 is
  port (
    reset : in STD_LOGIC;
    CLK : in STD_LOGIC;
    getA : in STD_LOGIC;
    getB : in STD_LOGIC;
    mem_clr : in STD_LOGIC;
    card_outA : out STD_LOGIC_VECTOR ( 3 downto 0 );
    card_outB : out STD_LOGIC_VECTOR ( 3 downto 0 );
    card_colA : out STD_LOGIC_VECTOR ( 2 downto 0 );
    card_colB : out STD_LOGIC_VECTOR ( 2 downto 0 );
    card_rdyA : out STD_LOGIC;
    card_rdyB : out STD_LOGIC
  );
  end component design_1_memory_0_0;
  component design_1_wallet_0_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    bet_amount : in STD_LOGIC_VECTOR ( 3 downto 0 );
    budget : out STD_LOGIC_VECTOR ( 8 downto 0 );
    can_bet : out STD_LOGIC;
    mem_clr : in STD_LOGIC;
    player_win : in STD_LOGIC;
    dealer_win : in STD_LOGIC;
    player_bankrupt : out STD_LOGIC
  );
  end component design_1_wallet_0_0;
  component design_1_single_control_unit_0_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    iobus : in STD_LOGIC_VECTOR ( 3 downto 0 );
    iochoice : in STD_LOGIC_VECTOR ( 1 downto 0 );
    can_bet : in STD_LOGIC;
    bankrupt : in STD_LOGIC;
    p_card_rdy : in STD_LOGIC;
    d_card_rdy : in STD_LOGIC;
    bet_amount : out STD_LOGIC_VECTOR ( 3 downto 0 );
    player_hit : out STD_LOGIC;
    dealer_hit : out STD_LOGIC;
    dealer_win : out STD_LOGIC;
    player_win : out STD_LOGIC;
    p_score_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
    d_score_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
    state_exit : out STD_LOGIC_VECTOR ( 3 downto 0 );
    add_flag_ext : out STD_LOGIC;
    p_t1 : out STD_LOGIC;
    p_t2 : out STD_LOGIC;
    p_t3 : out STD_LOGIC;
    p_t4 : out STD_LOGIC;
    d_t1 : out STD_LOGIC;
    d_t2 : out STD_LOGIC;
    d_t3 : out STD_LOGIC;
    d_t4 : out STD_LOGIC;
    p_card : in STD_LOGIC_VECTOR ( 3 downto 0 );
    d_card : in STD_LOGIC_VECTOR ( 3 downto 0 );
    mem_clear : out STD_LOGIC;
    easy_out : out STD_LOGIC;
    medium_out : out STD_LOGIC;
    hard_out : out STD_LOGIC;
    tie : out STD_LOGIC;
    vga_debug : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );
  end component design_1_single_control_unit_0_0;
  component design_1_clk_wiz_0_0 is
  port (
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC
  );
  end component design_1_clk_wiz_0_0;
  component design_1_mem_vga_0_0 is
  port (
    clr : in STD_LOGIC;
    p_t1 : in STD_LOGIC;
    p_t2 : in STD_LOGIC;
    p_t3 : in STD_LOGIC;
    p_t4 : in STD_LOGIC;
    d_t1 : in STD_LOGIC;
    d_t2 : in STD_LOGIC;
    d_t3 : in STD_LOGIC;
    d_t4 : in STD_LOGIC;
    p_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    d_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    d_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    p_score : in STD_LOGIC_VECTOR ( 4 downto 0 );
    d_score : in STD_LOGIC_VECTOR ( 4 downto 0 );
    p_bet : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_budget : in STD_LOGIC_VECTOR ( 8 downto 0 );
    easy : in STD_LOGIC;
    medium_in : in STD_LOGIC;
    hard : in STD_LOGIC;
    d_cd1_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    d_cd2_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    d_cd3_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    d_cd4_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    p_cd1_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    p_cd2_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    p_cd3_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    p_cd4_col : out STD_LOGIC_VECTOR ( 2 downto 0 );
    d_cd1_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    d_cd2_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    d_cd3_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    d_cd4_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_cd1_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_cd2_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_cd3_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_cd4_num : out STD_LOGIC_VECTOR ( 3 downto 0 );
    p_score_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
    d_score_ext : out STD_LOGIC_VECTOR ( 4 downto 0 );
    bet : out STD_LOGIC_VECTOR ( 3 downto 0 );
    budget : out STD_LOGIC_VECTOR ( 8 downto 0 );
    diff_ext : out STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component design_1_mem_vga_0_0;
  component design_1_screen_0_0 is
  port (
    video_on : in STD_LOGIC;
    pixel_x : in STD_LOGIC_VECTOR ( 9 downto 0 );
    pixel_y : in STD_LOGIC_VECTOR ( 9 downto 0 );
    d1_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    d2_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    d3_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    d4_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    p1_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    p2_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    p3_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    p4_cd_col : in STD_LOGIC_VECTOR ( 2 downto 0 );
    d1_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    d2_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    d3_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    d4_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p1_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p2_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p3_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p4_cd_num : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_score : in STD_LOGIC_VECTOR ( 4 downto 0 );
    d_score : in STD_LOGIC_VECTOR ( 4 downto 0 );
    p_bet : in STD_LOGIC_VECTOR ( 3 downto 0 );
    iobus : in STD_LOGIC_VECTOR ( 3 downto 0 );
    p_budget : in STD_LOGIC_VECTOR ( 8 downto 0 );
    rgb_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    diff_ch : in STD_LOGIC_VECTOR ( 1 downto 0 )
  );
  end component design_1_screen_0_0;
  component design_1_rgb_selector_0_0 is
  port (
    win : in STD_LOGIC;
    lose : in STD_LOGIC;
    draw : in STD_LOGIC;
    clk : in STD_LOGIC;
    selector : in STD_LOGIC;
    rgb_screen : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rgb_title : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rgb_end : in STD_LOGIC_VECTOR ( 15 downto 0 );
    rgb_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_1_rgb_selector_0_0;
  component design_1_vga_sync_0_0 is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    hsync : out STD_LOGIC;
    vsync : out STD_LOGIC;
    video_on : out STD_LOGIC;
    pixel_x : out STD_LOGIC_VECTOR ( 9 downto 0 );
    pixel_y : out STD_LOGIC_VECTOR ( 9 downto 0 )
  );
  end component design_1_vga_sync_0_0;
  component design_1_firstscreen_0_0 is
  port (
    video_on : in STD_LOGIC;
    pixel_x : in STD_LOGIC_VECTOR ( 9 downto 0 );
    pixel_y : in STD_LOGIC_VECTOR ( 9 downto 0 );
    rgb_out : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_1_firstscreen_0_0;
  component design_1_lastscreen_0_0 is
  port (
    video_on : in STD_LOGIC;
    pixel_x : in STD_LOGIC_VECTOR ( 9 downto 0 );
    pixel_y : in STD_LOGIC_VECTOR ( 9 downto 0 );
    rgb_out : out STD_LOGIC_VECTOR ( 15 downto 0 );
    win : in STD_LOGIC;
    lost : in STD_LOGIC;
    broke : in STD_LOGIC;
    tie : in STD_LOGIC
  );
  end component design_1_lastscreen_0_0;
  signal HWreset_0_b_lower : STD_LOGIC;
  signal HWreset_0_b_upper : STD_LOGIC;
  signal button_in_1 : STD_LOGIC;
  signal buttons_1 : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal firstscreen_0_rgb_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal ioblock_0_iobus : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal ioblock_0_iochoice : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ioblock_0_leds : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal lastscreen_0_rgb_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal mem_vga_0_bet : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_budget : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal mem_vga_0_d_cd1_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_d_cd1_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_d_cd2_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_d_cd2_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_d_cd3_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_d_cd3_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_d_cd4_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_d_cd4_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_d_score_ext : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal mem_vga_0_diff_ext : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal mem_vga_0_p_cd1_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_p_cd1_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_p_cd2_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_p_cd2_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_p_cd3_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_p_cd3_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_p_cd4_col : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal mem_vga_0_p_cd4_num : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mem_vga_0_p_score_ext : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal memory_0_card_colA : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal memory_0_card_colB : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal memory_0_card_outA : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal memory_0_card_outB : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal memory_0_card_rdyA : STD_LOGIC;
  signal memory_0_card_rdyB : STD_LOGIC;
  signal rgb_selector_0_rgb_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal screen_0_rgb_out : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal single_control_unit_0_bet_amount : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal single_control_unit_0_d_score_ext : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal single_control_unit_0_d_t1 : STD_LOGIC;
  signal single_control_unit_0_d_t2 : STD_LOGIC;
  signal single_control_unit_0_d_t3 : STD_LOGIC;
  signal single_control_unit_0_d_t4 : STD_LOGIC;
  signal single_control_unit_0_dealer_hit : STD_LOGIC;
  signal single_control_unit_0_dealer_win : STD_LOGIC;
  signal single_control_unit_0_easy_out : STD_LOGIC;
  signal single_control_unit_0_hard_out : STD_LOGIC;
  signal single_control_unit_0_medium_out : STD_LOGIC;
  signal single_control_unit_0_mem_clear : STD_LOGIC;
  signal single_control_unit_0_p_score_ext : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal single_control_unit_0_p_t1 : STD_LOGIC;
  signal single_control_unit_0_p_t2 : STD_LOGIC;
  signal single_control_unit_0_p_t3 : STD_LOGIC;
  signal single_control_unit_0_p_t4 : STD_LOGIC;
  signal single_control_unit_0_player_hit : STD_LOGIC;
  signal single_control_unit_0_player_win : STD_LOGIC;
  signal single_control_unit_0_state_exit : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal single_control_unit_0_tie : STD_LOGIC;
  signal sw_1 : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal sys_clock_1 : STD_LOGIC;
  signal vga_sync_0_hsync : STD_LOGIC;
  signal vga_sync_0_pixel_x : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal vga_sync_0_pixel_y : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal vga_sync_0_video_on : STD_LOGIC;
  signal vga_sync_0_vsync : STD_LOGIC;
  signal wallet_0_budget : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal wallet_0_can_bet : STD_LOGIC;
  signal wallet_0_player_bankrupt : STD_LOGIC;
  signal NLW_single_control_unit_0_add_flag_ext_UNCONNECTED : STD_LOGIC;
  signal NLW_single_control_unit_0_vga_debug_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
begin
  button_in_1 <= button_in;
  buttons_1(2 downto 0) <= buttons(2 downto 0);
  hsync <= vga_sync_0_hsync;
  leds(3 downto 0) <= ioblock_0_leds(3 downto 0);
  rgb_out(15 downto 0) <= rgb_selector_0_rgb_out(15 downto 0);
  sw_1(3 downto 0) <= sw(3 downto 0);
  sys_clock_1 <= sys_clock;
  vsync <= vga_sync_0_vsync;
HWreset_0: component design_1_HWreset_0_0
     port map (
      b_lower => HWreset_0_b_lower,
      b_upper => HWreset_0_b_upper,
      button_in => button_in_1,
      clk => clk_wiz_0_clk_out1
    );
clk_wiz_0: component design_1_clk_wiz_0_0
     port map (
      clk_in1 => sys_clock_1,
      clk_out1 => clk_wiz_0_clk_out1
    );
firstscreen_0: component design_1_firstscreen_0_0
     port map (
      pixel_x(9 downto 0) => vga_sync_0_pixel_x(9 downto 0),
      pixel_y(9 downto 0) => vga_sync_0_pixel_y(9 downto 0),
      rgb_out(15 downto 0) => firstscreen_0_rgb_out(15 downto 0),
      video_on => vga_sync_0_video_on
    );
ioblock_0: component design_1_ioblock_0_0
     port map (
      add_flag => '0',
      buttons(2 downto 0) => buttons_1(2 downto 0),
      clk => clk_wiz_0_clk_out1,
      d_hit => '0',
      easy => '0',
      hard => '0',
      iobus(3 downto 0) => ioblock_0_iobus(3 downto 0),
      iochoice(1 downto 0) => ioblock_0_iochoice(1 downto 0),
      leds(3 downto 0) => ioblock_0_leds(3 downto 0),
      medium_in => '0',
      p_hit => '0',
      rdy_A => '0',
      rdy_B => '0',
      state_exit(3 downto 0) => single_control_unit_0_state_exit(3 downto 0),
      sw(3 downto 0) => sw_1(3 downto 0)
    );
lastscreen_0: component design_1_lastscreen_0_0
     port map (
      broke => wallet_0_player_bankrupt,
      lost => single_control_unit_0_dealer_win,
      pixel_x(9 downto 0) => vga_sync_0_pixel_x(9 downto 0),
      pixel_y(9 downto 0) => vga_sync_0_pixel_y(9 downto 0),
      rgb_out(15 downto 0) => lastscreen_0_rgb_out(15 downto 0),
      tie => single_control_unit_0_tie,
      video_on => vga_sync_0_video_on,
      win => single_control_unit_0_player_win
    );
mem_vga_0: component design_1_mem_vga_0_0
     port map (
      bet(3 downto 0) => mem_vga_0_bet(3 downto 0),
      budget(8 downto 0) => mem_vga_0_budget(8 downto 0),
      clr => single_control_unit_0_mem_clear,
      d_cd1_col(2 downto 0) => mem_vga_0_d_cd1_col(2 downto 0),
      d_cd1_num(3 downto 0) => mem_vga_0_d_cd1_num(3 downto 0),
      d_cd2_col(2 downto 0) => mem_vga_0_d_cd2_col(2 downto 0),
      d_cd2_num(3 downto 0) => mem_vga_0_d_cd2_num(3 downto 0),
      d_cd3_col(2 downto 0) => mem_vga_0_d_cd3_col(2 downto 0),
      d_cd3_num(3 downto 0) => mem_vga_0_d_cd3_num(3 downto 0),
      d_cd4_col(2 downto 0) => mem_vga_0_d_cd4_col(2 downto 0),
      d_cd4_num(3 downto 0) => mem_vga_0_d_cd4_num(3 downto 0),
      d_cd_col(2 downto 0) => memory_0_card_colB(2 downto 0),
      d_cd_num(3 downto 0) => memory_0_card_outB(3 downto 0),
      d_score(4 downto 0) => single_control_unit_0_d_score_ext(4 downto 0),
      d_score_ext(4 downto 0) => mem_vga_0_d_score_ext(4 downto 0),
      d_t1 => single_control_unit_0_d_t1,
      d_t2 => single_control_unit_0_d_t2,
      d_t3 => single_control_unit_0_d_t3,
      d_t4 => single_control_unit_0_d_t4,
      diff_ext(1 downto 0) => mem_vga_0_diff_ext(1 downto 0),
      easy => single_control_unit_0_easy_out,
      hard => single_control_unit_0_hard_out,
      medium_in => single_control_unit_0_medium_out,
      p_bet(3 downto 0) => single_control_unit_0_bet_amount(3 downto 0),
      p_budget(8 downto 0) => wallet_0_budget(8 downto 0),
      p_cd1_col(2 downto 0) => mem_vga_0_p_cd1_col(2 downto 0),
      p_cd1_num(3 downto 0) => mem_vga_0_p_cd1_num(3 downto 0),
      p_cd2_col(2 downto 0) => mem_vga_0_p_cd2_col(2 downto 0),
      p_cd2_num(3 downto 0) => mem_vga_0_p_cd2_num(3 downto 0),
      p_cd3_col(2 downto 0) => mem_vga_0_p_cd3_col(2 downto 0),
      p_cd3_num(3 downto 0) => mem_vga_0_p_cd3_num(3 downto 0),
      p_cd4_col(2 downto 0) => mem_vga_0_p_cd4_col(2 downto 0),
      p_cd4_num(3 downto 0) => mem_vga_0_p_cd4_num(3 downto 0),
      p_cd_col(2 downto 0) => memory_0_card_colA(2 downto 0),
      p_cd_num(3 downto 0) => memory_0_card_outA(3 downto 0),
      p_score(4 downto 0) => single_control_unit_0_p_score_ext(4 downto 0),
      p_score_ext(4 downto 0) => mem_vga_0_p_score_ext(4 downto 0),
      p_t1 => single_control_unit_0_p_t1,
      p_t2 => single_control_unit_0_p_t2,
      p_t3 => single_control_unit_0_p_t3,
      p_t4 => single_control_unit_0_p_t4
    );
memory_0: component design_1_memory_0_0
     port map (
      CLK => clk_wiz_0_clk_out1,
      card_colA(2 downto 0) => memory_0_card_colA(2 downto 0),
      card_colB(2 downto 0) => memory_0_card_colB(2 downto 0),
      card_outA(3 downto 0) => memory_0_card_outA(3 downto 0),
      card_outB(3 downto 0) => memory_0_card_outB(3 downto 0),
      card_rdyA => memory_0_card_rdyA,
      card_rdyB => memory_0_card_rdyB,
      getA => single_control_unit_0_player_hit,
      getB => single_control_unit_0_dealer_hit,
      mem_clr => single_control_unit_0_mem_clear,
      reset => HWreset_0_b_upper
    );
rgb_selector_0: component design_1_rgb_selector_0_0
     port map (
      clk => clk_wiz_0_clk_out1,
      draw => single_control_unit_0_tie,
      lose => single_control_unit_0_dealer_win,
      rgb_end(15 downto 0) => lastscreen_0_rgb_out(15 downto 0),
      rgb_out(15 downto 0) => rgb_selector_0_rgb_out(15 downto 0),
      rgb_screen(15 downto 0) => screen_0_rgb_out(15 downto 0),
      rgb_title(15 downto 0) => firstscreen_0_rgb_out(15 downto 0),
      selector => HWreset_0_b_lower,
      win => single_control_unit_0_player_win
    );
screen_0: component design_1_screen_0_0
     port map (
      d1_cd_col(2 downto 0) => mem_vga_0_d_cd1_col(2 downto 0),
      d1_cd_num(3 downto 0) => mem_vga_0_d_cd1_num(3 downto 0),
      d2_cd_col(2 downto 0) => mem_vga_0_d_cd2_col(2 downto 0),
      d2_cd_num(3 downto 0) => mem_vga_0_d_cd2_num(3 downto 0),
      d3_cd_col(2 downto 0) => mem_vga_0_d_cd3_col(2 downto 0),
      d3_cd_num(3 downto 0) => mem_vga_0_d_cd3_num(3 downto 0),
      d4_cd_col(2 downto 0) => mem_vga_0_d_cd4_col(2 downto 0),
      d4_cd_num(3 downto 0) => mem_vga_0_d_cd4_num(3 downto 0),
      d_score(4 downto 0) => mem_vga_0_d_score_ext(4 downto 0),
      diff_ch(1 downto 0) => mem_vga_0_diff_ext(1 downto 0),
      iobus(3 downto 0) => B"0000",
      p1_cd_col(2 downto 0) => mem_vga_0_p_cd1_col(2 downto 0),
      p1_cd_num(3 downto 0) => mem_vga_0_p_cd1_num(3 downto 0),
      p2_cd_col(2 downto 0) => mem_vga_0_p_cd2_col(2 downto 0),
      p2_cd_num(3 downto 0) => mem_vga_0_p_cd2_num(3 downto 0),
      p3_cd_col(2 downto 0) => mem_vga_0_p_cd3_col(2 downto 0),
      p3_cd_num(3 downto 0) => mem_vga_0_p_cd3_num(3 downto 0),
      p4_cd_col(2 downto 0) => mem_vga_0_p_cd4_col(2 downto 0),
      p4_cd_num(3 downto 0) => mem_vga_0_p_cd4_num(3 downto 0),
      p_bet(3 downto 0) => mem_vga_0_bet(3 downto 0),
      p_budget(8 downto 0) => mem_vga_0_budget(8 downto 0),
      p_score(4 downto 0) => mem_vga_0_p_score_ext(4 downto 0),
      pixel_x(9 downto 0) => vga_sync_0_pixel_x(9 downto 0),
      pixel_y(9 downto 0) => vga_sync_0_pixel_y(9 downto 0),
      rgb_out(15 downto 0) => screen_0_rgb_out(15 downto 0),
      video_on => vga_sync_0_video_on
    );
single_control_unit_0: component design_1_single_control_unit_0_0
     port map (
      add_flag_ext => NLW_single_control_unit_0_add_flag_ext_UNCONNECTED,
      bankrupt => wallet_0_player_bankrupt,
      bet_amount(3 downto 0) => single_control_unit_0_bet_amount(3 downto 0),
      can_bet => wallet_0_can_bet,
      clk => clk_wiz_0_clk_out1,
      d_card(3 downto 0) => memory_0_card_outB(3 downto 0),
      d_card_rdy => memory_0_card_rdyB,
      d_score_ext(4 downto 0) => single_control_unit_0_d_score_ext(4 downto 0),
      d_t1 => single_control_unit_0_d_t1,
      d_t2 => single_control_unit_0_d_t2,
      d_t3 => single_control_unit_0_d_t3,
      d_t4 => single_control_unit_0_d_t4,
      dealer_hit => single_control_unit_0_dealer_hit,
      dealer_win => single_control_unit_0_dealer_win,
      easy_out => single_control_unit_0_easy_out,
      hard_out => single_control_unit_0_hard_out,
      iobus(3 downto 0) => ioblock_0_iobus(3 downto 0),
      iochoice(1 downto 0) => ioblock_0_iochoice(1 downto 0),
      medium_out => single_control_unit_0_medium_out,
      mem_clear => single_control_unit_0_mem_clear,
      p_card(3 downto 0) => memory_0_card_outA(3 downto 0),
      p_card_rdy => memory_0_card_rdyA,
      p_score_ext(4 downto 0) => single_control_unit_0_p_score_ext(4 downto 0),
      p_t1 => single_control_unit_0_p_t1,
      p_t2 => single_control_unit_0_p_t2,
      p_t3 => single_control_unit_0_p_t3,
      p_t4 => single_control_unit_0_p_t4,
      player_hit => single_control_unit_0_player_hit,
      player_win => single_control_unit_0_player_win,
      reset => HWreset_0_b_upper,
      state_exit(3 downto 0) => single_control_unit_0_state_exit(3 downto 0),
      tie => single_control_unit_0_tie,
      vga_debug(3 downto 0) => NLW_single_control_unit_0_vga_debug_UNCONNECTED(3 downto 0)
    );
vga_sync_0: component design_1_vga_sync_0_0
     port map (
      clk => clk_wiz_0_clk_out1,
      hsync => vga_sync_0_hsync,
      pixel_x(9 downto 0) => vga_sync_0_pixel_x(9 downto 0),
      pixel_y(9 downto 0) => vga_sync_0_pixel_y(9 downto 0),
      reset => HWreset_0_b_upper,
      video_on => vga_sync_0_video_on,
      vsync => vga_sync_0_vsync
    );
wallet_0: component design_1_wallet_0_0
     port map (
      bet_amount(3 downto 0) => single_control_unit_0_bet_amount(3 downto 0),
      budget(8 downto 0) => wallet_0_budget(8 downto 0),
      can_bet => wallet_0_can_bet,
      clk => clk_wiz_0_clk_out1,
      dealer_win => single_control_unit_0_dealer_win,
      mem_clr => single_control_unit_0_mem_clear,
      player_bankrupt => wallet_0_player_bankrupt,
      player_win => single_control_unit_0_player_win,
      reset => HWreset_0_b_upper
    );
end STRUCTURE;
